err = require('err'),fs = require('fs');
JSCLASS_PATH = './lib/vendors/Js.Class/';
require(JSCLASS_PATH+'core');
require(JSCLASS_PATH+'loader');
require(JSCLASS_PATH+'enumerable');
require(JSCLASS_PATH+'hash');
require(JSCLASS_PATH+'comparable');
require(JSCLASS_PATH+'observable');
require(JSCLASS_PATH+'set');
require(JSCLASS_PATH+'state');
require(JSCLASS_PATH+'command');
_ = require('underscore'); 
_Str = require('underscore.string');
var check = require('validator').check,
    sanitize = require('validator').sanitize;
    
var Validator = require('validator').Validator;
Validator.prototype.error = function (msg) {
    this._errors.push(msg);
}

Validator.prototype.getErrors = function () {
    return this._errors;
}

Validator.prototype.hasErrors = function () {
    return (this._errors.length == 0)?false:true;
}
var validator = new Validator();

var PMP = null; 
var ApplicationObject  = {
	Config: {},
	Connection: null,
	PMP: null,
	Events: require('events').EventEmitter,
	configPath: "config/",
	files: [
	//	{NS:"TokenSystem",File:"tokenSystem.json"},
		{NS:"Errors",File:"errors.json"},
		{NS:"Settings",File:"settings.json"}
	//	{NS:"Sockets",File:"socketConfig.json"}
	],
	Init: function () {
		var based =this;
		var objects = {};
		_.each(this.files,function (item) { 
			var path = require('path');
    		var p =path.join(__dirname, based.configPath,item.File);
			console.log("Config File: "+p+" Been Loaded \n");
			var fileContents = fs.readFileSync(p,'utf8'); 
			console.dir(JSON.parse(fileContents.toString()))
			objects[item.NS]=JSON.parse(fileContents.toString())
		});
		based.Configs = objects;
		based._InitApplication();
	},
	LoadSessionDb: function ()  {
		var based = this;
		var config = based._loadDbConfig();
		var mysql = require('mysql');
		var client = mysql.createClient({
			  host: config.host,
			  port: config.port,
			  user: config.user,
			  password: config.password,
			  database: config.dbname
		});
		return client;
	},
	_loadDb: function ()  {
		var based = this;
		var config = based._loadDbConfig();
		var mysql = require('mysql');
		var client = mysql.createClient({
			  host: config.host,
			  port: config.port,
			  user: config.user,
			  password: config.password,
			  database: config.dbname
		});
		var queues = require('mysql-queues');
		queues(client, false);
		
		based.Connection = client;
	},
	ReloadDb: function ()  {
		var based = this;
		var config = based._loadDbConfig();
		var mysql = require('mysql');
		var client = mysql.createClient({
			  host: config.host,
			  port: config.port,
			  user: config.user,
			  password: config.password,
			  database: config.dbname
		});
		var queues = require('mysql-queues');
		queues(client, false); 
		based.Connection = client;
	},
	OnBeginRequest: function (req,res,next) {
		//mark response accept cross domain urls issues
		res.header("X-Powered-By","Pass My Pass Web Systems (build by sivan wolberg)");
	    res.header("Access-Control-Allow-Origin", "*");
	    res.header("Access-Control-Allow-Headers", "X-Requested-With"); 
        next();
	},
	OnEndRequest: function (req,res) {
		//enterd into the after fillter areas
		//NOTE this global for all calls
 
	},
	_InitApplication: function () {
		//on start application
		var based = this; 
		//load db connection
		based._loadDb(); 
		//load the libs
		based._loadLibs();
	},
	_loadLibs: function() {
		var based = this;
		console.log("load libs");
		//try { path= applicationPath; } catch(err) {}
		based.PMP = require(__dirname+"/lib/PMP/Core").Init();
	},
	_loadDbConfig: function() {
		var confFile = app.root + '/config/database.json';
		var fs = require('fs');
		var path = require('path');

		var config;
		if (path.existsSync(confFile)) {
		    try {
		        config = JSON.parse(fs.readFileSync(confFile, 'utf-8'));
		    } catch (e) {
		        console.log('Could not parse config/database.json');
		        throw e; 
		    }
		} else {
		    config = {};
		} 
		config = config[app.settings.env];
		return config;
	}
};
exports.LoadDb = function () {
	return ApplicationObject.LoadSessionDb();
}
exports.LoadApplciation = function(){
	ApplicationObject.Init();
	return ApplicationObject;
}
