@echo off
start "" "cmd" "/k cd %NODE_INSPECTOR%\bin&&node inspector &"

if exist "%NODE_INSPECTOR%\environmentvars.bat" ( 
    start "" "cmd" "/k %NODE_INSPECTOR%\environmentvars.bat&&node --debug-brk server" 
) else ( 
    start "" "cmd" "/k node --debug-brk server" 
)

start "" %NODE_INSPECTOR_BROWSER% "http://localhost:8080"
echo start "" %NODE_INSPECTOR_BROWSER% "%1%"