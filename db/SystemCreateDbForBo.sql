-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 01, 2012 at 12:45 PM
-- Server version: 5.5.16
-- PHP Version: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `passmypass`
--

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

DROP TABLE IF EXISTS `country`;
CREATE TABLE IF NOT EXISTS `country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `short_name` varchar(80) NOT NULL DEFAULT '',
  `long_name` varchar(80) NOT NULL DEFAULT '',
  `iso2` char(2) DEFAULT NULL,
  `iso3` char(3) DEFAULT NULL,
  `numcode` varchar(6) DEFAULT NULL,
  `un_member` varchar(12) DEFAULT NULL,
  `calling_code` varchar(8) DEFAULT NULL,
  `cctld` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=251 ;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`id`, `short_name`, `long_name`, `iso2`, `iso3`, `numcode`, `un_member`, `calling_code`, `cctld`) VALUES
(1, 'Afghanistan', 'Islamic Republic of Afghanistan', 'AF', 'AFG', '004', 'yes', '93', '.af'),
(2, 'Aland Islands', '&Aring;land Islands', 'AX', 'ALA', '248', 'no', '358', '.ax'),
(3, 'Albania', 'Republic of Albania', 'AL', 'ALB', '008', 'yes', '355', '.al'),
(4, 'Algeria', 'People''s Democratic Republic of Algeria', 'DZ', 'DZA', '012', 'yes', '213', '.dz'),
(5, 'American Samoa', 'American Samoa', 'AS', 'ASM', '016', 'no', '1+684', '.as'),
(6, 'Andorra', 'Principality of Andorra', 'AD', 'AND', '020', 'yes', '376', '.ad'),
(7, 'Angola', 'Republic of Angola', 'AO', 'AGO', '024', 'yes', '244', '.ao'),
(8, 'Anguilla', 'Anguilla', 'AI', 'AIA', '660', 'no', '1+264', '.ai'),
(9, 'Antarctica', 'Antarctica', 'AQ', 'ATA', '010', 'no', '672', '.aq'),
(10, 'Antigua and Barbuda', 'Antigua and Barbuda', 'AG', 'ATG', '028', 'yes', '1+268', '.ag'),
(11, 'Argentina', 'Argentine Republic', 'AR', 'ARG', '032', 'yes', '54', '.ar'),
(12, 'Armenia', 'Republic of Armenia', 'AM', 'ARM', '051', 'yes', '374', '.am'),
(13, 'Aruba', 'Aruba', 'AW', 'ABW', '533', 'no', '297', '.aw'),
(14, 'Australia', 'Commonwealth of Australia', 'AU', 'AUS', '036', 'yes', '61', '.au'),
(15, 'Austria', 'Republic of Austria', 'AT', 'AUT', '040', 'yes', '43', '.at'),
(16, 'Azerbaijan', 'Republic of Azerbaijan', 'AZ', 'AZE', '031', 'yes', '994', '.az'),
(17, 'Bahamas', 'Commonwealth of The Bahamas', 'BS', 'BHS', '044', 'yes', '1+242', '.bs'),
(18, 'Bahrain', 'Kingdom of Bahrain', 'BH', 'BHR', '048', 'yes', '973', '.bh'),
(19, 'Bangladesh', 'People''s Republic of Bangladesh', 'BD', 'BGD', '050', 'yes', '880', '.bd'),
(20, 'Barbados', 'Barbados', 'BB', 'BRB', '052', 'yes', '1+246', '.bb'),
(21, 'Belarus', 'Republic of Belarus', 'BY', 'BLR', '112', 'yes', '375', '.by'),
(22, 'Belgium', 'Kingdom of Belgium', 'BE', 'BEL', '056', 'yes', '32', '.be'),
(23, 'Belize', 'Belize', 'BZ', 'BLZ', '084', 'yes', '501', '.bz'),
(24, 'Benin', 'Republic of Benin', 'BJ', 'BEN', '204', 'yes', '229', '.bj'),
(25, 'Bermuda', 'Bermuda Islands', 'BM', 'BMU', '060', 'no', '1+441', '.bm'),
(26, 'Bhutan', 'Kingdom of Bhutan', 'BT', 'BTN', '064', 'yes', '975', '.bt'),
(27, 'Bolivia', 'Plurinational State of Bolivia', 'BO', 'BOL', '068', 'yes', '591', '.bo'),
(28, 'Bonaire, Sint Eustatius and Saba', 'Bonaire, Sint Eustatius and Saba', 'BQ', 'BES', '535', 'no', '599', '.bq'),
(29, 'Bosnia and Herzegovina', 'Bosnia and Herzegovina', 'BA', 'BIH', '070', 'yes', '387', '.ba'),
(30, 'Botswana', 'Republic of Botswana', 'BW', 'BWA', '072', 'yes', '267', '.bw'),
(31, 'Bouvet Island', 'Bouvet Island', 'BV', 'BVT', '074', 'no', 'NONE', '.bv'),
(32, 'Brazil', 'Federative Republic of Brazil', 'BR', 'BRA', '076', 'yes', '55', '.br'),
(33, 'British Indian Ocean Territory', 'British Indian Ocean Territory', 'IO', 'IOT', '086', 'no', '246', '.io'),
(34, 'Brunei', 'Brunei Darussalam', 'BN', 'BRN', '096', 'yes', '673', '.bn'),
(35, 'Bulgaria', 'Republic of Bulgaria', 'BG', 'BGR', '100', 'yes', '359', '.bg'),
(36, 'Burkina Faso', 'Burkina Faso', 'BF', 'BFA', '854', 'yes', '226', '.bf'),
(37, 'Burundi', 'Republic of Burundi', 'BI', 'BDI', '108', 'yes', '257', '.bi'),
(38, 'Cambodia', 'Kingdom of Cambodia', 'KH', 'KHM', '116', 'yes', '855', '.kh'),
(39, 'Cameroon', 'Republic of Cameroon', 'CM', 'CMR', '120', 'yes', '237', '.cm'),
(40, 'Canada', 'Canada', 'CA', 'CAN', '124', 'yes', '1', '.ca'),
(41, 'Cape Verde', 'Republic of Cape Verde', 'CV', 'CPV', '132', 'yes', '238', '.cv'),
(42, 'Cayman Islands', 'The Cayman Islands', 'KY', 'CYM', '136', 'no', '1+345', '.ky'),
(43, 'Central African Republic', 'Central African Republic', 'CF', 'CAF', '140', 'yes', '236', '.cf'),
(44, 'Chad', 'Republic of Chad', 'TD', 'TCD', '148', 'yes', '235', '.td'),
(45, 'Chile', 'Republic of Chile', 'CL', 'CHL', '152', 'yes', '56', '.cl'),
(46, 'China', 'People''s Republic of China', 'CN', 'CHN', '156', 'yes', '86', '.cn'),
(47, 'Christmas Island', 'Christmas Island', 'CX', 'CXR', '162', 'no', '61', '.cx'),
(48, 'Cocos (Keeling) Islands', 'Cocos (Keeling) Islands', 'CC', 'CCK', '166', 'no', '61', '.cc'),
(49, 'Colombia', 'Republic of Colombia', 'CO', 'COL', '170', 'yes', '57', '.co'),
(50, 'Comoros', 'Union of the Comoros', 'KM', 'COM', '174', 'yes', '269', '.km'),
(51, 'Congo', 'Republic of the Congo', 'CG', 'COG', '178', 'yes', '242', '.cg'),
(52, 'Cook Islands', 'Cook Islands', 'CK', 'COK', '184', 'some', '682', '.ck'),
(53, 'Costa Rica', 'Republic of Costa Rica', 'CR', 'CRI', '188', 'yes', '506', '.cr'),
(54, 'Cote d''ivoire (Ivory Coast)', 'Republic of C&ocirc;te D''Ivoire (Ivory Coast)', 'CI', 'CIV', '384', 'yes', '225', '.ci'),
(55, 'Croatia', 'Republic of Croatia', 'HR', 'HRV', '191', 'yes', '385', '.hr'),
(56, 'Cuba', 'Republic of Cuba', 'CU', 'CUB', '192', 'yes', '53', '.cu'),
(57, 'Curacao', 'Cura&ccedil;ao', 'CW', 'CUW', '531', 'no', '599', '.cw'),
(58, 'Cyprus', 'Republic of Cyprus', 'CY', 'CYP', '196', 'yes', '357', '.cy'),
(59, 'Czech Republic', 'Czech Republic', 'CZ', 'CZE', '203', 'yes', '420', '.cz'),
(60, 'Democratic Republic of the Congo', 'Democratic Republic of the Congo', 'CD', 'COD', '180', 'yes', '243', '.cd'),
(61, 'Denmark', 'Kingdom of Denmark', 'DK', 'DNK', '208', 'yes', '45', '.dk'),
(62, 'Djibouti', 'Republic of Djibouti', 'DJ', 'DJI', '262', 'yes', '253', '.dj'),
(63, 'Dominica', 'Commonwealth of Dominica', 'DM', 'DMA', '212', 'yes', '1+767', '.dm'),
(64, 'Dominican Republic', 'Dominican Republic', 'DO', 'DOM', '214', 'yes', '1+809, 8', '.do'),
(65, 'Ecuador', 'Republic of Ecuador', 'EC', 'ECU', '218', 'yes', '593', '.ec'),
(66, 'Egypt', 'Arab Republic of Egypt', 'EG', 'EGY', '818', 'yes', '20', '.eg'),
(67, 'El Salvador', 'Republic of El Salvador', 'SV', 'SLV', '222', 'yes', '503', '.sv'),
(68, 'Equatorial Guinea', 'Republic of Equatorial Guinea', 'GQ', 'GNQ', '226', 'yes', '240', '.gq'),
(69, 'Eritrea', 'State of Eritrea', 'ER', 'ERI', '232', 'yes', '291', '.er'),
(70, 'Estonia', 'Republic of Estonia', 'EE', 'EST', '233', 'yes', '372', '.ee'),
(71, 'Ethiopia', 'Federal Democratic Republic of Ethiopia', 'ET', 'ETH', '231', 'yes', '251', '.et'),
(72, 'Falkland Islands (Malvinas)', 'The Falkland Islands (Malvinas)', 'FK', 'FLK', '238', 'no', '500', '.fk'),
(73, 'Faroe Islands', 'The Faroe Islands', 'FO', 'FRO', '234', 'no', '298', '.fo'),
(74, 'Fiji', 'Republic of Fiji', 'FJ', 'FJI', '242', 'yes', '679', '.fj'),
(75, 'Finland', 'Republic of Finland', 'FI', 'FIN', '246', 'yes', '358', '.fi'),
(76, 'France', 'French Republic', 'FR', 'FRA', '250', 'yes', '33', '.fr'),
(77, 'French Guiana', 'French Guiana', 'GF', 'GUF', '254', 'no', '594', '.gf'),
(78, 'French Polynesia', 'French Polynesia', 'PF', 'PYF', '258', 'no', '689', '.pf'),
(79, 'French Southern Territories', 'French Southern Territories', 'TF', 'ATF', '260', 'no', NULL, '.tf'),
(80, 'Gabon', 'Gabonese Republic', 'GA', 'GAB', '266', 'yes', '241', '.ga'),
(81, 'Gambia', 'Republic of The Gambia', 'GM', 'GMB', '270', 'yes', '220', '.gm'),
(82, 'Georgia', 'Georgia', 'GE', 'GEO', '268', 'yes', '995', '.ge'),
(83, 'Germany', 'Federal Republic of Germany', 'DE', 'DEU', '276', 'yes', '49', '.de'),
(84, 'Ghana', 'Republic of Ghana', 'GH', 'GHA', '288', 'yes', '233', '.gh'),
(85, 'Gibraltar', 'Gibraltar', 'GI', 'GIB', '292', 'no', '350', '.gi'),
(86, 'Greece', 'Hellenic Republic', 'GR', 'GRC', '300', 'yes', '30', '.gr'),
(87, 'Greenland', 'Greenland', 'GL', 'GRL', '304', 'no', '299', '.gl'),
(88, 'Grenada', 'Grenada', 'GD', 'GRD', '308', 'yes', '1+473', '.gd'),
(89, 'Guadaloupe', 'Guadeloupe', 'GP', 'GLP', '312', 'no', '590', '.gp'),
(90, 'Guam', 'Guam', 'GU', 'GUM', '316', 'no', '1+671', '.gu'),
(91, 'Guatemala', 'Republic of Guatemala', 'GT', 'GTM', '320', 'yes', '502', '.gt'),
(92, 'Guernsey', 'Guernsey', 'GG', 'GGY', '831', 'no', '44', '.gg'),
(93, 'Guinea', 'Republic of Guinea', 'GN', 'GIN', '324', 'yes', '224', '.gn'),
(94, 'Guinea-Bissau', 'Republic of Guinea-Bissau', 'GW', 'GNB', '624', 'yes', '245', '.gw'),
(95, 'Guyana', 'Co-operative Republic of Guyana', 'GY', 'GUY', '328', 'yes', '592', '.gy'),
(96, 'Haiti', 'Republic of Haiti', 'HT', 'HTI', '332', 'yes', '509', '.ht'),
(97, 'Heard Island and McDonald Islands', 'Heard Island and McDonald Islands', 'HM', 'HMD', '334', 'no', 'NONE', '.hm'),
(98, 'Honduras', 'Republic of Honduras', 'HN', 'HND', '340', 'yes', '504', '.hn'),
(99, 'Hong Kong', 'Hong Kong', 'HK', 'HKG', '344', 'no', '852', '.hk'),
(100, 'Hungary', 'Hungary', 'HU', 'HUN', '348', 'yes', '36', '.hu'),
(101, 'Iceland', 'Republic of Iceland', 'IS', 'ISL', '352', 'yes', '354', '.is'),
(102, 'India', 'Republic of India', 'IN', 'IND', '356', 'yes', '91', '.in'),
(103, 'Indonesia', 'Republic of Indonesia', 'ID', 'IDN', '360', 'yes', '62', '.id'),
(104, 'Iran', 'Islamic Republic of Iran', 'IR', 'IRN', '364', 'yes', '98', '.ir'),
(105, 'Iraq', 'Republic of Iraq', 'IQ', 'IRQ', '368', 'yes', '964', '.iq'),
(106, 'Ireland', 'Ireland', 'IE', 'IRL', '372', 'yes', '353', '.ie'),
(107, 'Isle of Man', 'Isle of Man', 'IM', 'IMN', '833', 'no', '44', '.im'),
(108, 'Israel', 'State of Israel', 'IL', 'ISR', '376', 'yes', '972', '.il'),
(109, 'Italy', 'Italian Republic', 'IT', 'ITA', '380', 'yes', '39', '.jm'),
(110, 'Jamaica', 'Jamaica', 'JM', 'JAM', '388', 'yes', '1+876', '.jm'),
(111, 'Japan', 'Japan', 'JP', 'JPN', '392', 'yes', '81', '.jp'),
(112, 'Jersey', 'The Bailiwick of Jersey', 'JE', 'JEY', '832', 'no', '44', '.je'),
(113, 'Jordan', 'Hashemite Kingdom of Jordan', 'JO', 'JOR', '400', 'yes', '962', '.jo'),
(114, 'Kazakhstan', 'Republic of Kazakhstan', 'KZ', 'KAZ', '398', 'yes', '7', '.kz'),
(115, 'Kenya', 'Republic of Kenya', 'KE', 'KEN', '404', 'yes', '254', '.ke'),
(116, 'Kiribati', 'Republic of Kiribati', 'KI', 'KIR', '296', 'yes', '686', '.ki'),
(117, 'Kosovo', 'Republic of Kosovo', 'XK', '---', '---', 'some', '381', ''),
(118, 'Kuwait', 'State of Kuwait', 'KW', 'KWT', '414', 'yes', '965', '.kw'),
(119, 'Kyrgyzstan', 'Kyrgyz Republic', 'KG', 'KGZ', '417', 'yes', '996', '.kg'),
(120, 'Laos', 'Lao People''s Democratic Republic', 'LA', 'LAO', '418', 'yes', '856', '.la'),
(121, 'Latvia', 'Republic of Latvia', 'LV', 'LVA', '428', 'yes', '371', '.lv'),
(122, 'Lebanon', 'Republic of Lebanon', 'LB', 'LBN', '422', 'yes', '961', '.lb'),
(123, 'Lesotho', 'Kingdom of Lesotho', 'LS', 'LSO', '426', 'yes', '266', '.ls'),
(124, 'Liberia', 'Republic of Liberia', 'LR', 'LBR', '430', 'yes', '231', '.lr'),
(125, 'Libya', 'Libya', 'LY', 'LBY', '434', 'yes', '218', '.ly'),
(126, 'Liechtenstein', 'Principality of Liechtenstein', 'LI', 'LIE', '438', 'yes', '423', '.li'),
(127, 'Lithuania', 'Republic of Lithuania', 'LT', 'LTU', '440', 'yes', '370', '.lt'),
(128, 'Luxembourg', 'Grand Duchy of Luxembourg', 'LU', 'LUX', '442', 'yes', '352', '.lu'),
(129, 'Macao', 'The Macao Special Administrative Region', 'MO', 'MAC', '446', 'no', '853', '.mo'),
(130, 'Macedonia', 'The Former Yugoslav Republic of Macedonia', 'MK', 'MKD', '807', 'yes', '389', '.mk'),
(131, 'Madagascar', 'Republic of Madagascar', 'MG', 'MDG', '450', 'yes', '261', '.mg'),
(132, 'Malawi', 'Republic of Malawi', 'MW', 'MWI', '454', 'yes', '265', '.mw'),
(133, 'Malaysia', 'Malaysia', 'MY', 'MYS', '458', 'yes', '60', '.my'),
(134, 'Maldives', 'Republic of Maldives', 'MV', 'MDV', '462', 'yes', '960', '.mv'),
(135, 'Mali', 'Republic of Mali', 'ML', 'MLI', '466', 'yes', '223', '.ml'),
(136, 'Malta', 'Republic of Malta', 'MT', 'MLT', '470', 'yes', '356', '.mt'),
(137, 'Marshall Islands', 'Republic of the Marshall Islands', 'MH', 'MHL', '584', 'yes', '692', '.mh'),
(138, 'Martinique', 'Martinique', 'MQ', 'MTQ', '474', 'no', '596', '.mq'),
(139, 'Mauritania', 'Islamic Republic of Mauritania', 'MR', 'MRT', '478', 'yes', '222', '.mr'),
(140, 'Mauritius', 'Republic of Mauritius', 'MU', 'MUS', '480', 'yes', '230', '.mu'),
(141, 'Mayotte', 'Mayotte', 'YT', 'MYT', '175', 'no', '262', '.yt'),
(142, 'Mexico', 'United Mexican States', 'MX', 'MEX', '484', 'yes', '52', '.mx'),
(143, 'Micronesia', 'Federated States of Micronesia', 'FM', 'FSM', '583', 'yes', '691', '.fm'),
(144, 'Moldava', 'Republic of Moldova', 'MD', 'MDA', '498', 'yes', '373', '.md'),
(145, 'Monaco', 'Principality of Monaco', 'MC', 'MCO', '492', 'yes', '377', '.mc'),
(146, 'Mongolia', 'Mongolia', 'MN', 'MNG', '496', 'yes', '976', '.mn'),
(147, 'Montenegro', 'Montenegro', 'ME', 'MNE', '499', 'yes', '382', '.me'),
(148, 'Montserrat', 'Montserrat', 'MS', 'MSR', '500', 'no', '1+664', '.ms'),
(149, 'Morocco', 'Kingdom of Morocco', 'MA', 'MAR', '504', 'yes', '212', '.ma'),
(150, 'Mozambique', 'Republic of Mozambique', 'MZ', 'MOZ', '508', 'yes', '258', '.mz'),
(151, 'Myanmar (Burma)', 'Republic of the Union of Myanmar', 'MM', 'MMR', '104', 'yes', '95', '.mm'),
(152, 'Namibia', 'Republic of Namibia', 'NA', 'NAM', '516', 'yes', '264', '.na'),
(153, 'Nauru', 'Republic of Nauru', 'NR', 'NRU', '520', 'yes', '674', '.nr'),
(154, 'Nepal', 'Federal Democratic Republic of Nepal', 'NP', 'NPL', '524', 'yes', '977', '.np'),
(155, 'Netherlands', 'Kingdom of the Netherlands', 'NL', 'NLD', '528', 'yes', '31', '.nl'),
(156, 'New Caledonia', 'New Caledonia', 'NC', 'NCL', '540', 'no', '687', '.nc'),
(157, 'New Zealand', 'New Zealand', 'NZ', 'NZL', '554', 'yes', '64', '.nz'),
(158, 'Nicaragua', 'Republic of Nicaragua', 'NI', 'NIC', '558', 'yes', '505', '.ni'),
(159, 'Niger', 'Republic of Niger', 'NE', 'NER', '562', 'yes', '227', '.ne'),
(160, 'Nigeria', 'Federal Republic of Nigeria', 'NG', 'NGA', '566', 'yes', '234', '.ng'),
(161, 'Niue', 'Niue', 'NU', 'NIU', '570', 'some', '683', '.nu'),
(162, 'Norfolk Island', 'Norfolk Island', 'NF', 'NFK', '574', 'no', '672', '.nf'),
(163, 'North Korea', 'Democratic People''s Republic of Korea', 'KP', 'PRK', '408', 'yes', '850', '.kp'),
(164, 'Northern Mariana Islands', 'Northern Mariana Islands', 'MP', 'MNP', '580', 'no', '1+670', '.mp'),
(165, 'Norway', 'Kingdom of Norway', 'NO', 'NOR', '578', 'yes', '47', '.no'),
(166, 'Oman', 'Sultanate of Oman', 'OM', 'OMN', '512', 'yes', '968', '.om'),
(167, 'Pakistan', 'Islamic Republic of Pakistan', 'PK', 'PAK', '586', 'yes', '92', '.pk'),
(168, 'Palau', 'Republic of Palau', 'PW', 'PLW', '585', 'yes', '680', '.pw'),
(169, 'Palestine', 'State of Palestine (or Occupied Palestinian Territory)', 'PS', 'PSE', '275', 'some', '970', '.ps'),
(170, 'Panama', 'Republic of Panama', 'PA', 'PAN', '591', 'yes', '507', '.pa'),
(171, 'Papua New Guinea', 'Independent State of Papua New Guinea', 'PG', 'PNG', '598', 'yes', '675', '.pg'),
(172, 'Paraguay', 'Republic of Paraguay', 'PY', 'PRY', '600', 'yes', '595', '.py'),
(173, 'Peru', 'Republic of Peru', 'PE', 'PER', '604', 'yes', '51', '.pe'),
(174, 'Phillipines', 'Republic of the Philippines', 'PH', 'PHL', '608', 'yes', '63', '.ph'),
(175, 'Pitcairn', 'Pitcairn', 'PN', 'PCN', '612', 'no', 'NONE', '.pn'),
(176, 'Poland', 'Republic of Poland', 'PL', 'POL', '616', 'yes', '48', '.pl'),
(177, 'Portugal', 'Portuguese Republic', 'PT', 'PRT', '620', 'yes', '351', '.pt'),
(178, 'Puerto Rico', 'Commonwealth of Puerto Rico', 'PR', 'PRI', '630', 'no', '1+939', '.pr'),
(179, 'Qatar', 'State of Qatar', 'QA', 'QAT', '634', 'yes', '974', '.qa'),
(180, 'Reunion', 'R&eacute;union', 'RE', 'REU', '638', 'no', '262', '.re'),
(181, 'Romania', 'Romania', 'RO', 'ROU', '642', 'yes', '40', '.ro'),
(182, 'Russia', 'Russian Federation', 'RU', 'RUS', '643', 'yes', '7', '.ru'),
(183, 'Rwanda', 'Republic of Rwanda', 'RW', 'RWA', '646', 'yes', '250', '.rw'),
(184, 'Saint Barthelemy', 'Saint Barth&eacute;lemy', 'BL', 'BLM', '652', 'no', '590', '.bl'),
(185, 'Saint Helena', 'Saint Helena, Ascension and Tristan da Cunha', 'SH', 'SHN', '654', 'no', '290', '.sh'),
(186, 'Saint Kitts and Nevis', 'Federation of Saint Christopher and Nevis', 'KN', 'KNA', '659', 'yes', '1+869', '.kn'),
(187, 'Saint Lucia', 'Saint Lucia', 'LC', 'LCA', '662', 'yes', '1+758', '.lc'),
(188, 'Saint Martin', 'Saint Martin', 'MF', 'MAF', '663', 'no', '590', '.mf'),
(189, 'Saint Pierre and Miquelon', 'Saint Pierre and Miquelon', 'PM', 'SPM', '666', 'no', '508', '.pm'),
(190, 'Saint Vincent and the Grenadines', 'Saint Vincent and the Grenadines', 'VC', 'VCT', '670', 'yes', '1+784', '.vc'),
(191, 'Samoa', 'Independent State of Samoa', 'WS', 'WSM', '882', 'yes', '685', '.ws'),
(192, 'San Marino', 'Republic of San Marino', 'SM', 'SMR', '674', 'yes', '378', '.sm'),
(193, 'Sao Tome and Principe', 'Democratic Republic of S&atilde;o Tom&eacute; and Pr&iacute;ncipe', 'ST', 'STP', '678', 'yes', '239', '.st'),
(194, 'Saudi Arabia', 'Kingdom of Saudi Arabia', 'SA', 'SAU', '682', 'yes', '966', '.sa'),
(195, 'Senegal', 'Republic of Senegal', 'SN', 'SEN', '686', 'yes', '221', '.sn'),
(196, 'Serbia', 'Republic of Serbia', 'RS', 'SRB', '688', 'yes', '381', '.rs'),
(197, 'Seychelles', 'Republic of Seychelles', 'SC', 'SYC', '690', 'yes', '248', '.sc'),
(198, 'Sierra Leone', 'Republic of Sierra Leone', 'SL', 'SLE', '694', 'yes', '232', '.sl'),
(199, 'Singapore', 'Republic of Singapore', 'SG', 'SGP', '702', 'yes', '65', '.sg'),
(200, 'Sint Maarten', 'Sint Maarten', 'SX', 'SXM', '534', 'no', '1+721', '.sx'),
(201, 'Slovakia', 'Slovak Republic', 'SK', 'SVK', '703', 'yes', '421', '.sk'),
(202, 'Slovenia', 'Republic of Slovenia', 'SI', 'SVN', '705', 'yes', '386', '.si'),
(203, 'Solomon Islands', 'Solomon Islands', 'SB', 'SLB', '090', 'yes', '677', '.sb'),
(204, 'Somalia', 'Somali Republic', 'SO', 'SOM', '706', 'yes', '252', '.so'),
(205, 'South Africa', 'Republic of South Africa', 'ZA', 'ZAF', '710', 'yes', '27', '.za'),
(206, 'South Georgia and the South Sandwich Islands', 'South Georgia and the South Sandwich Islands', 'GS', 'SGS', '239', 'no', '500', '.gs'),
(207, 'South Korea', 'Republic of Korea', 'KR', 'KOR', '410', 'yes', '82', '.kr'),
(208, 'South Sudan', 'Republic of South Sudan', 'SS', 'SSD', '728', 'yes', '211', '.ss'),
(209, 'Spain', 'Kingdom of Spain', 'ES', 'ESP', '724', 'yes', '34', '.es'),
(210, 'Sri Lanka', 'Democratic Socialist Republic of Sri Lanka', 'LK', 'LKA', '144', 'yes', '94', '.lk'),
(211, 'Sudan', 'Republic of the Sudan', 'SD', 'SDN', '729', 'yes', '249', '.sd'),
(212, 'Suriname', 'Republic of Suriname', 'SR', 'SUR', '740', 'yes', '597', '.sr'),
(213, 'Svalbard and Jan Mayen', 'Svalbard and Jan Mayen', 'SJ', 'SJM', '744', 'no', '47', '.sj'),
(214, 'Swaziland', 'Kingdom of Swaziland', 'SZ', 'SWZ', '748', 'yes', '268', '.sz'),
(215, 'Sweden', 'Kingdom of Sweden', 'SE', 'SWE', '752', 'yes', '46', '.se'),
(216, 'Switzerland', 'Swiss Confederation', 'CH', 'CHE', '756', 'yes', '41', '.ch'),
(217, 'Syria', 'Syrian Arab Republic', 'SY', 'SYR', '760', 'yes', '963', '.sy'),
(218, 'Taiwan', 'Republic of China (Taiwan)', 'TW', 'TWN', '158', 'former', '886', '.tw'),
(219, 'Tajikistan', 'Republic of Tajikistan', 'TJ', 'TJK', '762', 'yes', '992', '.tj'),
(220, 'Tanzania', 'United Republic of Tanzania', 'TZ', 'TZA', '834', 'yes', '255', '.tz'),
(221, 'Thailand', 'Kingdom of Thailand', 'TH', 'THA', '764', 'yes', '66', '.th'),
(222, 'Timor-Leste (East Timor)', 'Democratic Republic of Timor-Leste', 'TL', 'TLS', '626', 'yes', '670', '.tl'),
(223, 'Togo', 'Togolese Republic', 'TG', 'TGO', '768', 'yes', '228', '.tg'),
(224, 'Tokelau', 'Tokelau', 'TK', 'TKL', '772', 'no', '690', '.tk'),
(225, 'Tonga', 'Kingdom of Tonga', 'TO', 'TON', '776', 'yes', '676', '.to'),
(226, 'Trinidad and Tobago', 'Republic of Trinidad and Tobago', 'TT', 'TTO', '780', 'yes', '1+868', '.tt'),
(227, 'Tunisia', 'Republic of Tunisia', 'TN', 'TUN', '788', 'yes', '216', '.tn'),
(228, 'Turkey', 'Republic of Turkey', 'TR', 'TUR', '792', 'yes', '90', '.tr'),
(229, 'Turkmenistan', 'Turkmenistan', 'TM', 'TKM', '795', 'yes', '993', '.tm'),
(230, 'Turks and Caicos Islands', 'Turks and Caicos Islands', 'TC', 'TCA', '796', 'no', '1+649', '.tc'),
(231, 'Tuvalu', 'Tuvalu', 'TV', 'TUV', '798', 'yes', '688', '.tv'),
(232, 'Uganda', 'Republic of Uganda', 'UG', 'UGA', '800', 'yes', '256', '.ug'),
(233, 'Ukraine', 'Ukraine', 'UA', 'UKR', '804', 'yes', '380', '.ua'),
(234, 'United Arab Emirates', 'United Arab Emirates', 'AE', 'ARE', '784', 'yes', '971', '.ae'),
(235, 'United Kingdom', 'United Kingdom of Great Britain and Nothern Ireland', 'GB', 'GBR', '826', 'yes', '44', '.uk'),
(236, 'United States', 'United States of America', 'US', 'USA', '840', 'yes', '1', '.us'),
(237, 'United States Minor Outlying Islands', 'United States Minor Outlying Islands', 'UM', 'UMI', '581', 'no', 'NONE', 'NONE'),
(238, 'Uruguay', 'Eastern Republic of Uruguay', 'UY', 'URY', '858', 'yes', '598', '.uy'),
(239, 'Uzbekistan', 'Republic of Uzbekistan', 'UZ', 'UZB', '860', 'yes', '998', '.uz'),
(240, 'Vanuatu', 'Republic of Vanuatu', 'VU', 'VUT', '548', 'yes', '678', '.vu'),
(241, 'Vatican City', 'State of the Vatican City', 'VA', 'VAT', '336', 'no', '39', '.va'),
(242, 'Venezuela', 'Bolivarian Republic of Venezuela', 'VE', 'VEN', '862', 'yes', '58', '.ve'),
(243, 'Vietnam', 'Socialist Republic of Vietnam', 'VN', 'VNM', '704', 'yes', '84', '.vn'),
(244, 'Virgin Islands, British', 'British Virgin Islands', 'VG', 'VGB', '092', 'no', '1+284', '.vg'),
(245, 'Virgin Islands, US', 'Virgin Islands of the United States', 'VI', 'VIR', '850', 'no', '1+340', '.vi'),
(246, 'Wallis and Futuna', 'Wallis and Futuna', 'WF', 'WLF', '876', 'no', '681', '.wf'),
(247, 'Western Sahara', 'Western Sahara', 'EH', 'ESH', '732', 'no', '212', '.eh'),
(248, 'Yemen', 'Republic of Yemen', 'YE', 'YEM', '887', 'yes', '967', '.ye'),
(249, 'Zambia', 'Republic of Zambia', 'ZM', 'ZMB', '894', 'yes', '260', '.zm'),
(250, 'Zimbabwe', 'Republic of Zimbabwe', 'ZW', 'ZWE', '716', 'yes', '263', '.zw');

-- --------------------------------------------------------

--
-- Table structure for table `feeds`
--

DROP TABLE IF EXISTS `feeds`;
CREATE TABLE IF NOT EXISTS `feeds` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '-1',
  `group_id` int(11) NOT NULL,
  `site_id` int(11) NOT NULL DEFAULT '-1',
  `title` varchar(50) NOT NULL,
  `alt` varchar(100) NOT NULL,
  `body` varchar(150) NOT NULL,
  `register_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `isAchive` bit(1) NOT NULL,
  `isSticky` bit(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`group_id`,`site_id`),
  KEY `title` (`title`),
  KEY `group_id` (`group_id`,`site_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `feeds_messages`
--

DROP TABLE IF EXISTS `feeds_messages`;
CREATE TABLE IF NOT EXISTS `feeds_messages` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `feed_id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `alt` varchar(200) NOT NULL,
  `body` text NOT NULL,
  `isSticky` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `feed_id` (`feed_id`,`title`),
  KEY `parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

DROP TABLE IF EXISTS `groups`;
CREATE TABLE IF NOT EXISTS `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_profile_id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `description` varchar(45) DEFAULT NULL,
  `register_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime NOT NULL,
  `isActive` varchar(45) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`,`group_profile_id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`),
  KEY `group_profile_id` (`group_profile_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `group_profile_id`, `name`, `description`, `register_date`, `update_date`, `isActive`) VALUES
(1, 1, 'TelAvivUni', 'Tel aviv Uni', '2012-04-11 17:48:45', '2012-04-11 20:49:18', '1');

-- --------------------------------------------------------

--
-- Table structure for table `group_profile`
--

DROP TABLE IF EXISTS `group_profile`;
CREATE TABLE IF NOT EXISTS `group_profile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `logo` varchar(500) DEFAULT NULL,
  `email` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `group_profile`
--

INSERT INTO `group_profile` (`id`, `logo`, `email`) VALUES
(1, NULL, 'fsatwings@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `premmisionmap2user`
--

DROP TABLE IF EXISTS `premmisionmap2user`;
CREATE TABLE IF NOT EXISTS `premmisionmap2user` (
  `groupid` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `roleid` int(11) NOT NULL,
  `isActive` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`groupid`,`userid`,`roleid`),
  KEY `roleid` (`roleid`),
  KEY `userid` (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `desc` varchar(45) DEFAULT NULL,
  `isActive` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`id`,`name`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `desc`, `isActive`) VALUES
(2, 'Registers', 'For all Register Users', '1');

-- --------------------------------------------------------

--
-- Table structure for table `roles2groups`
--

DROP TABLE IF EXISTS `roles2groups`;
CREATE TABLE IF NOT EXISTS `roles2groups` (
  `role_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`role_id`,`group_id`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `security_levels`
--

DROP TABLE IF EXISTS `security_levels`;
CREATE TABLE IF NOT EXISTS `security_levels` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` text,
  `isFullFromReq` bit(1) NOT NULL DEFAULT b'0',
  `isPasswordReq` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`name`),
  KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `security_levels`
--

INSERT INTO `security_levels` (`id`, `name`, `description`, `isFullFromReq`, `isPasswordReq`) VALUES
(1, 'No Securty Level', NULL, '0', '0');

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

DROP TABLE IF EXISTS `sessions`;
CREATE TABLE IF NOT EXISTS `sessions` (
  `sid` varchar(255) NOT NULL,
  `session` text NOT NULL,
  `expires` int(11) DEFAULT NULL,
  PRIMARY KEY (`sid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
CREATE TABLE IF NOT EXISTS `settings` (
  `key` varchar(50) NOT NULL,
  `value` text,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`key`, `value`) VALUES
('expried_scan_key', '2'),
('expried_session_key', '2');

-- --------------------------------------------------------

--
-- Table structure for table `sites`
--

DROP TABLE IF EXISTS `sites`;
CREATE TABLE IF NOT EXISTS `sites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `security_level_id` smallint(6) NOT NULL,
  `uuid` varchar(50) NOT NULL,
  `name` varchar(45) NOT NULL,
  `description` text,
  `isSupported` bit(1) NOT NULL DEFAULT b'1',
  `isBlocked` bit(1) NOT NULL DEFAULT b'0',
  `isArcived` bit(1) NOT NULL DEFAULT b'0',
  `register_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime DEFAULT NULL,
  `last_scan_date` datetime NOT NULL,
  PRIMARY KEY (`id`,`uuid`,`name`),
  UNIQUE KEY `site_uuid_UNIQUE` (`uuid`),
  UNIQUE KEY `name_UNIQUE` (`name`),
  KEY `security_level_id` (`security_level_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `sites`
--

INSERT INTO `sites` (`id`, `security_level_id`, `uuid`, `name`, `description`, `isSupported`, `isBlocked`, `isArcived`, `register_date`, `update_date`, `last_scan_date`) VALUES
(2, 1, 'd626e6db-8612-11e1-b58a-00248c94c2e0', 'yrr', NULL, '1', '0', '0', '2012-04-14 09:18:50', NULL, '2012-04-14 12:18:50');

-- --------------------------------------------------------

--
-- Table structure for table `sites_domains`
--

DROP TABLE IF EXISTS `sites_domains`;
CREATE TABLE IF NOT EXISTS `sites_domains` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `site_id` int(11) NOT NULL,
  `domain` varchar(500) NOT NULL,
  `isSecure` bit(1) NOT NULL DEFAULT b'0',
  `register_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`,`site_id`),
  UNIQUE KEY `site_id_UNIQUE` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `sites_inputfields`
--

DROP TABLE IF EXISTS `sites_inputfields`;
CREATE TABLE IF NOT EXISTS `sites_inputfields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `site_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL DEFAULT '',
  `label` varchar(100) NOT NULL DEFAULT '',
  `markup` varchar(500) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`,`site_id`),
  KEY `site_id` (`site_id`),
  KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `sites_inputfields`
--

INSERT INTO `sites_inputfields` (`id`, `site_id`, `name`, `label`, `markup`) VALUES
(1, 2, 'test', 'test', 'test'),
(2, 2, 'test1', 'test1', 'testq1');

-- --------------------------------------------------------

--
-- Table structure for table `sites_ios`
--

DROP TABLE IF EXISTS `sites_ios`;
CREATE TABLE IF NOT EXISTS `sites_ios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `site_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `icon_path` varchar(255) NOT NULL,
  `title` varchar(35) NOT NULL,
  `alt` varchar(100) DEFAULT NULL,
  `securty_level_id` smallint(6) NOT NULL,
  `isArcived` bit(1) NOT NULL DEFAULT b'0',
  `isReadonly` bit(1) NOT NULL,
  `last_access` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `site_id` (`site_id`),
  KEY `securty_level_id` (`securty_level_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `sites_ios2folders`
--

DROP TABLE IF EXISTS `sites_ios2folders`;
CREATE TABLE IF NOT EXISTS `sites_ios2folders` (
  `ios_id` int(11) NOT NULL,
  `folder_id` int(11) NOT NULL,
  PRIMARY KEY (`ios_id`,`folder_id`),
  KEY `folder_id` (`folder_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sites_ios_folder`
--

DROP TABLE IF EXISTS `sites_ios_folder`;
CREATE TABLE IF NOT EXISTS `sites_ios_folder` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `title` varchar(35) NOT NULL,
  `alt` varchar(150) NOT NULL,
  `securty_level_id` smallint(6) NOT NULL,
  `isReadonly` bit(1) NOT NULL,
  `isArcived` bit(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `securty_level_id` (`securty_level_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `userlogin`
--

DROP TABLE IF EXISTS `userlogin`;
CREATE TABLE IF NOT EXISTS `userlogin` (
  `user_id` int(11) NOT NULL,
  `uuid` varchar(50) NOT NULL,
  `expire_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `uuid` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `userlogin`
--

INSERT INTO `userlogin` (`user_id`, `uuid`, `expire_date`) VALUES
(1, '2f8dc771-957a-903c-8958-d331090bdeeb', '2012-04-12 12:57:05');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_profile_id` int(11) NOT NULL,
  `email` varchar(500) NOT NULL,
  `password` varchar(200) NOT NULL,
  `register_date` datetime NOT NULL,
  `update_date` datetime DEFAULT NULL,
  `last_login_date` datetime DEFAULT NULL,
  `last_ip` varchar(45) DEFAULT NULL,
  `isActive` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`id`),
  KEY `user_profile_id` (`user_profile_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user_profile_id`, `email`, `password`, `register_date`, `update_date`, `last_login_date`, `last_ip`, `isActive`) VALUES
(1, 1, 'test@test.com', '123456', '2012-04-11 20:58:24', '2012-04-11 20:58:24', '2012-04-12 13:33:54', '127.0.0.1', '1');

-- --------------------------------------------------------

--
-- Table structure for table `usersites`
--

DROP TABLE IF EXISTS `usersites`;
CREATE TABLE IF NOT EXISTS `usersites` (
  `site_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `securty_level_id` smallint(6) NOT NULL DEFAULT '0',
  `InputKey` varchar(50) NOT NULL,
  PRIMARY KEY (`site_id`,`user_id`,`InputKey`),
  KEY `securty_level_id` (`securty_level_id`),
  KEY `user_id` (`user_id`),
  KEY `InputKey` (`InputKey`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `usersites`
--

INSERT INTO `usersites` (`site_id`, `user_id`, `securty_level_id`, `InputKey`) VALUES
(2, 1, 1, 'd626e6db-8612-11e1-b58a-00248c94c2e0');

-- --------------------------------------------------------

--
-- Table structure for table `usersites_input`
--

DROP TABLE IF EXISTS `usersites_input`;
CREATE TABLE IF NOT EXISTS `usersites_input` (
  `input_key` varchar(50) NOT NULL,
  `input_id` int(11) NOT NULL,
  `value` varchar(500) NOT NULL,
  PRIMARY KEY (`input_key`,`input_id`),
  KEY `input_id` (`input_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users_profile`
--

DROP TABLE IF EXISTS `users_profile`;
CREATE TABLE IF NOT EXISTS `users_profile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) NOT NULL,
  `fname` varchar(45) NOT NULL,
  `lname` varchar(45) NOT NULL,
  `gender` enum('m','f') NOT NULL,
  `address` varchar(200) DEFAULT NULL,
  `city` varchar(45) DEFAULT NULL,
  `birth_date` datetime DEFAULT NULL,
  `title` enum('mis','mr','doc') DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `country_id` (`country_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `users_profile`
--

INSERT INTO `users_profile` (`id`, `country_id`, `fname`, `lname`, `gender`, `address`, `city`, `birth_date`, `title`) VALUES
(1, 181, 'erez', 'sp', 'm', 'hyhgf', NULL, '2012-04-11 20:53:51', 'mr');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `feeds`
--
ALTER TABLE `feeds`
  ADD CONSTRAINT `feeds_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `feeds_ibfk_2` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `feeds_ibfk_3` FOREIGN KEY (`site_id`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `feeds_messages`
--
ALTER TABLE `feeds_messages`
  ADD CONSTRAINT `feeds_messages_ibfk_1` FOREIGN KEY (`parent_id`) REFERENCES `feeds_messages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `feeds_messages_ibfk_2` FOREIGN KEY (`feed_id`) REFERENCES `feeds` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `groups`
--
ALTER TABLE `groups`
  ADD CONSTRAINT `groups_ibfk_1` FOREIGN KEY (`group_profile_id`) REFERENCES `group_profile` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `premmisionmap2user`
--
ALTER TABLE `premmisionmap2user`
  ADD CONSTRAINT `premmisionmap2user_ibfk_3` FOREIGN KEY (`roleid`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `premmisionmap2user_ibfk_5` FOREIGN KEY (`groupid`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `premmisionmap2user_ibfk_6` FOREIGN KEY (`userid`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `roles2groups`
--
ALTER TABLE `roles2groups`
  ADD CONSTRAINT `roles2groups_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `roles2groups_ibfk_2` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sites`
--
ALTER TABLE `sites`
  ADD CONSTRAINT `sites_ibfk_1` FOREIGN KEY (`security_level_id`) REFERENCES `security_levels` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sites_domains`
--
ALTER TABLE `sites_domains`
  ADD CONSTRAINT `sites_domains_ibfk_1` FOREIGN KEY (`site_id`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sites_inputfields`
--
ALTER TABLE `sites_inputfields`
  ADD CONSTRAINT `sites_inputfields_ibfk_1` FOREIGN KEY (`site_id`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sites_ios`
--
ALTER TABLE `sites_ios`
  ADD CONSTRAINT `sites_ios_ibfk_1` FOREIGN KEY (`site_id`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sites_ios_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sites_ios_ibfk_3` FOREIGN KEY (`securty_level_id`) REFERENCES `security_levels` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sites_ios2folders`
--
ALTER TABLE `sites_ios2folders`
  ADD CONSTRAINT `sites_ios2folders_ibfk_1` FOREIGN KEY (`ios_id`) REFERENCES `sites_ios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sites_ios2folders_ibfk_2` FOREIGN KEY (`folder_id`) REFERENCES `sites_ios_folder` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sites_ios_folder`
--
ALTER TABLE `sites_ios_folder`
  ADD CONSTRAINT `sites_ios_folder_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sites_ios_folder_ibfk_2` FOREIGN KEY (`securty_level_id`) REFERENCES `security_levels` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `userlogin`
--
ALTER TABLE `userlogin`
  ADD CONSTRAINT `userlogin_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`user_profile_id`) REFERENCES `users_profile` (`id`);

--
-- Constraints for table `usersites`
--
ALTER TABLE `usersites`
  ADD CONSTRAINT `usersites_ibfk_1` FOREIGN KEY (`securty_level_id`) REFERENCES `security_levels` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `usersites_ibfk_2` FOREIGN KEY (`site_id`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `usersites_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `usersites_ibfk_4` FOREIGN KEY (`InputKey`) REFERENCES `sites` (`uuid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `usersites_input`
--
ALTER TABLE `usersites_input`
  ADD CONSTRAINT `usersites_input_ibfk_1` FOREIGN KEY (`input_key`) REFERENCES `usersites` (`InputKey`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `usersites_input_ibfk_2` FOREIGN KEY (`input_id`) REFERENCES `sites_inputfields` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

DELIMITER $$
--
-- Events
--
DROP EVENT `sess_cleanup`$$
CREATE DEFINER=`root`@`localhost` EVENT `sess_cleanup` ON SCHEDULE EVERY 15 MINUTE STARTS '2012-04-11 19:47:24' ON COMPLETION NOT PRESERVE ENABLE DO DELETE FROM `sessions` WHERE `expires` < UNIX_TIMESTAMP()$$

DELIMITER ;
