#!/usr/bin/env node
app = module.exports = require('railway').createServer();
//var io = require('socket.io').listen(app);
if (!module.parent) {
	//bundle.register('sockets');
    var port = process.env.PORT || 8082
    console.log("Railway server Start listening on port %d within %s environment", port, app.settings.env);
    app.listen(port);
}