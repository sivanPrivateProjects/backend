//require with ever controller 
/**Controller Logic **/
load('application');
var misc_control = function () {
	var based = this;
  based.UUID = "";
  based.userContext = null;
  try {
    PMP = app.ApplicationSharedObjects.PMP;
    before(function(){ based._onload(); });
    action('getCountrys', function () { based._doGetCountrys(); });
    action('IsCountryExist',function () { based._doIsCountryExist(); });
    action('getFrontEndSettings', function () { based._doGetFrontEndSettings(); });
	}
  catch (err) {
    railway.logger.write(err);
  }
  based._onload = function(){
    based.Context = new PMP.Models.Misc();
    based.SettingsContext = new PMP.Models.Settings();
    next();
  };

  based._doGetCountrys = function () {
    based.Context.GetCountrys(function(r) { send(r);});
  }

  based._doIsCountryExist = function () {
    var cid = req.query.cid;
    based.Context.IsCountryExist(cid,function(r) { send(r);});
  }

  based._doGetFrontEndSettings = function () {
    based.SettingsContext.GetSettingsFrontEnd(function(r) { 
      send(r);
    });
  }
} 
/**Dont remove**/
new misc_control();