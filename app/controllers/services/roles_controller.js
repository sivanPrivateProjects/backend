
/**Controller Logic **/
load('application');
var role_control = function () {
	var based = this;
	based.Base58 = require('base58');
	based.UUID = "";
	based.userContext = null;
	try {
		PMP = app.ApplicationSharedObjects.PMP;
		before(function(){ based._verfiedRole(); }, {except: 'GetRoles'});
		before(function(){ based._initGetRoles(); }, {only: 'GetRoles'});
		before(function(){ based._loadGroupController(); }, {only: ['BindRoleToGroup','UnBindRoleToGroup']});
		action('AddRole', function () { based._doAddRole(); });
		action('DeleteRole', function () { based._doDeleteGroup(); });
		action('GetRoles', function () { based._doGetRoles(); });
		action('UnBindRoleToGroup', function () { based._doUnBindRoleToGroup(); });
		action('BindRoleToGroup', function () { based._doBindRoleToGroup(); });
		action('UnBindRoleToUser', function () { based._doUnBindRoleToUser(); });
		action('BindRoleToUser', function () { based._doBindRoleToUser(); });
		action('ToggleRole', function () { based._doToggelStatusRole(); });
		action('ScanCachedRoles', function () { based._doScanCachedRoles(); });
	}
	catch (err) {
		railway.logger.write(err);
	}
	based._verfiedRole = function () {
		var uriUUID = req.url.split("/")[3];
		var responseObj = {
			success: false,
			errorCode: 404,
			error: "Incorrect User Seesion"
		}
		based.UUID = uriUUID;
		new PMP.Models.Users(true,uriUUID,function (responseObject) { 
			if (responseObject != null) {
				based.userContext = responseObject; 
				new PMP.Models.Roles(uriUUID,responseObject,function (responseBased){
					responseObj.error = "Incorrect Role";
					if (responseBased != null) {
						try {
							based.context = responseBased;
							based.UserId = based.Base58.decode(req.session.user_id);
							next();
						}
						catch (err) {
							railway.logger.write(err);
							send(JSON.stringify(responseObj));
						}
					}
					else {
						send(JSON.stringify(responseObj));
					}
				});
			}
			else {
				send(JSON.stringify(responseObj));
			}
		},function (){ 
			send(JSON.stringify(responseObj));
		});
	}


	based._doAddRole = function () {
		//Post data: name,description,email
		based.context.AddRole(req.body.name,req.body.description,function (responseObject){
			send(JSON.stringify(responseObject));
		});
	}

	based._doDeleteRole = function () {
		//Post data: name,description,email
		var roleid = based.Based58.decode(req.query.roleid);
		based.context.DeleteRole(roleid,function (responseObject){
			send(JSON.stringify(responseObject));
		});
	}

	based._doToggelStatusRole = function () {
		//Post data: name,description,email
		var roleid = based.Based58.decode(req.query.roleid);
		based.context.ToggleRole(roleid,function (responseObject){
			send(JSON.stringify(responseObject));
		});
	}

	based._initGetRoles = function () {
		based.context = new PMP.Models.Roles(-1,based.userContext,function (responseGroupObj){
				next();
		},error,true); 
		var error = function (){ 
			var responseObj = {
				success: false,
				errorCode: 101,
				error: "Incorrect Server Failer!!"
			}
			send(JSON.stringify(responseObj));
		};
	}
	
	based._doGetRoles = function () {
		var returnObject = {
			success: false,
			Groups: [],
			errorCode: 0,
			error: ""
		};
		var localPrivateKeyRef = req.query.hash;
		if (localPrivateKeyRef != undefined &&localPrivateKeyRef != null &&localPrivateKeyRef != "") {
			var Hashes = require("jshashes");
			var SHA512 = new Hashes.SHA512;
			var MD5 = new Hashes.MD5;
			var encryptLocalkeyRef = MD5.hex(SHA512.b64_hmac(SHA512.hex_hmac(SHA512.hex(PMP.Settings.frontend_addr_localPrivateKey),PMP.Settings.frontend_addr_localPrivateKey)),PMP.Settings.frontend_addr_localPrivateKey) 
			if (localPrivateKeyRef == encryptLocalkeyRef) {
				based.context.GetRoles(function (responseObject){
					send(JSON.stringify(responseObject));
				})
			}
			else {
				returnObject.error = "Hasking, Recored been loaded";
				returnObject.errorCode = -1;
				railway.logger.write(returnObject);
				send(JSON.stringify(responseObject));
			}
		}
		else {
			returnObject.error = "Hasking, Recored been loaded";
			returnObject.errorCode = -1;
			railway.logger.write(returnObject);
			send(JSON.stringify(responseObject));
		}
	}
	based._loadGroupController = function () {
		load('groups');
	}
	based._doUnBindRoleToGroup = function () {
		var groupId = based.Based58.decode(req.query.groupid);
		var roleId = based.Based58.decode(req.query.roleid);
		based.context.UnBindGroupToRole(groupId,roleId,function (responseObject){
			send(JSON.stringify(responseObject));
		});
	}
	based._doBindRoleToGroup = function () {
		var groupId = based.Based58.decode(req.query.groupid);
		var roleId = based.Based58.decode(req.query.roleid);
		based.context.BindGroupToRole(groupId,roleId,function (responseObject){
			send(JSON.stringify(responseObject));
		});
	}
	based._doUnBindRoleToUser = function () {
		var roleId = based.Based58.decode(req.query.roleid);
		var uidId = based.Based58.decode(req.query.uid);
		based.context.UnBindUserToRole(uidId,groupId,function (responseObject){
			send(JSON.stringify(responseObject));
		});
	}
	based._doBindRoleToUser = function () {
		var roleId = based.Based58.decode(req.query.roleid);
		var uidId = based.Based58.decode(req.query.uid);
		based.context.BindUserToRole(uidId,groupId,function (responseObject){
			send(JSON.stringify(responseObject));
		});
	}
	based._doScanCachedRoles = function () {
		var returnObject = {
			success: false,
			errorCode: 0,
			error: "",
			checksum: {
				Sum: 0,
				Errors:[]
			}
		};
		PMP.Utills.SendRestRequest(
			PMP.Utills.Sprintf(SettingsConfigObject.FEURIS.Caching.FetchRoles.URI,req.session.RealUUID),
			SettingsConfigObject.FEURIS.Caching.FetchRoles.Type,{},function (err,response) {
			if (response.success) {
				_.each(response.Roles, function(item) {
					based.context.AddRole(item.Name,item.Description,function (responseObj){
						if (responseObj.success) {
							returnObject.checksum.Sum++;
						}
						else {
							returnObject.checksum.Errors.push(item.Name);
						}
					});
				});
				send(JSON.stringify(returnObject));
			}
			else {
				railway.logger.write("Roles Caching System Went Wrong, Response give Error: <pre>"+JSON.stringify(response)+"</pre>");
				returnObject.Error = "Something Want Wrong Internal Error";
				returnObject.ErrorCode =500;
				send(JSON.stringify(returnObject));
			}
		});
	}
} 
/**Dont remove**/
new role_control();