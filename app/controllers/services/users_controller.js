//require with ever controller 
/**Controller Logic **/
load('application');
var users_control = function () {
	var based = this;
  based.Base58 = require('base58');
  based.UUID = "";
  based.userContext = null;
  try {
	PMP = app.ApplicationSharedObjects.PMP;
	before(function(){ based._verfiedUser(); },{only:['logout',
													  'HasPremmision',
													  'getProfile' , 
													  'userGroups' , 
													  'getUser']});
	before(function(){ based._onloadNoneUser(); },{only:['login']});
	before(function(){ based._onloadRegister(); },{only:['register']});
	action('logout', function () { based._doLogout(); });
	action('hasPremmision', function () { based._doHasPremmision(); });
	action('login', function () { based._doLogin(); });
	action('getProfile', function () { based._getProfile(); });
	action('userGroups', function () { based._doGetUserGroup(); });
	action('userList', function () { based._doUserList(); });
	action('getUser', function () { based._doGetUser(); });
	action('register', function () { based._doRegister(); });
	}
  catch (err) {
	railway.logger.write(err);
  }
  based._onloadRegister = function () {

	var ok = function (responseObject) { 
	  based.userContext = responseObject; 
	  based.groupContext = new PMP.Models.Groups(-1,based.userContext,function (responseGroupObj){
		based.roleContext = new  PMP.Models.Roles(-1,based.userContext,function (responseRoleObj){
		  next();
		},error,true);
	  },error,true);
	  
	};
	var error = function (){ 
	  var responseObj = {
		success: false,
		errorCode: 101,
		error: "Incorrect Server Failer!!"
	  }
	  send(JSON.stringify(responseObj));
	};
	based.userContext = new PMP.Models.Users(false,null,ok,error);
  }
  based._onloadNoneUser = function(){
	var ok = function (responseObject) { 
	  based.userContext = responseObject; 
	  next();
	};
	var error = function (){ 
		var responseObj = {
			success: false,
			errorCode: 0,
			error: "Incorrect User Seesion"
		}
		send(JSON.stringify(responseObj));
	};
	based.userContext = new PMP.Models.Users(false,null,ok,error);
  };
  based._verfiedUser = function () {
	var uriUUID = req.url.split("/")[3];
	based.UUID = uriUUID;
	var ok = function (responseObject) { 
	  based.userContext = responseObject; 
	  next();
	};
	var error = function (){ 
		var responseObj = {
			success: false,
			errorCode: 0,
			error: "Incorrect User Seesion"
		}
		send(JSON.stringify(responseObj));
	};
	new PMP.Models.Users(true,uriUUID,ok,error);
  }
  /**
   * private action method on register controller
   * @return JSON
   */
  based._doRegister = function () {

	var responseObj = {
		success: false,
		Groups: [],
		errorCode: 0,
		error: ""
	};
	var localPrivateKeyRef = req.query.hash;
	if (localPrivateKeyRef != undefined &&localPrivateKeyRef != null &&localPrivateKeyRef != "") {
		var Hashes = require("jshashes");
		var SHA512 = new Hashes.SHA512;
		var MD5 = new Hashes.MD5;
		var encryptLocalkeyRef = MD5.hex(SHA512.b64_hmac(SHA512.hex_hmac(SHA512.hex(PMP.Settings.frontend_addr_localPrivateKey),PMP.Settings.frontend_addr_localPrivateKey)),PMP.Settings.frontend_addr_localPrivateKey) 
		if (localPrivateKeyRef == encryptLocalkeyRef) {
			responseObj = {
				success: false,
				errorCode: 404,
				error: "arguments not matched"
			};
			var user = req.body.email;
			var pass = req.body.pass;
			var fname = req.body.fname;
			var lname = req.body.lname;
			var address = req.body.address;
			var city = req.body.city;
			var localIp = req.body.localIp; // get the user ip
			var country = req.body.country;
			var gid = req.body.gid;
			var rid = req.body.rid;
			var toc = req.body.toc;
			var isAutoLogin = req.body.auto;
			try { 
				toc = parseInt(toc,10); 
				isAutoLogin = (parseInt(isAutoLogin,10)==1)?true:false; 
				if (toc != 1) {
					send(JSON.stringify(returnObject));
				}
				else {
					var fields = {
						email: user,
						pass: pass,
						fname: fname,
						lname: lname,
						address: address,
						city: city,
						country: country,
						gid: gid,
						rid: rid
					}
					based.userContext.Register(fields,localIp,PMP.Settings.localPrivateKey,based.groupContext,based.roleContext,function (response) {
						if (response.status) {
							if (isAutoLogin) {
								based.userContext.RegisterSession(user,pass,req.ip,PMP.Settings.localPrivateKey,function (responseObject) {
									req.session.user_id  = based.userContext.Id;
									send(JSON.stringify(responseObject));
								});
							}
							else {
								send(JSON.stringify(response));
							}
						}
						else {
							send(JSON.stringify(responseObj));
						}
					});
				}
			}
			catch (err) {
				railway.logger.write(err);
				railway.logger.write(returnObject);
				send(JSON.stringify(returnObject));
			}
		}
		else {
			returnObject.error = "Hasking, Recored been loaded";
			returnObject.errorCode = -1;
			railway.logger.write(returnObject);
			send(JSON.stringify(responseObject));
		}
	}
	else {
		returnObject.error = "Hasking, Recored been loaded";
		returnObject.errorCode = -1;
		railway.logger.write(returnObject);
		send(JSON.stringify(responseObject));
	}
  }

  /**
   * private action method on HasPremmision controller
   * @return JSON
   */
  based._doHasPremmision = function () {
	var groupId = based.Based58.decode(req.query.groupid);
	var roleid = based.Based58.decode(req.query.roleid);
	based.userContext.HasUserHavePremmision(groupId,roleid,function (responseObject) {
		send(responseObject);
	});
  }

  /**
   * private action method on Login controller
   * @return JSON
   */
  based._doLogin = function() {
	var returnObject = {
		success: false,
		errorCode: 0,
		error: ""
	};
	var localPrivateKeyRef = req.query.hash;
	if (localPrivateKeyRef != undefined &&localPrivateKeyRef != null &&localPrivateKeyRef != "") {
		based.userContext.RegisterSession(req.body.user,req.body.password,req.body.localIp,PMP.Settings.localPrivateKey,function (responseObject) {
			req.session.user_id  = based.userContext.Id;
			send(JSON.stringify(responseObject));
		});
	}
	else {
		returnObject.error = "Hasking, Recored been loaded";
		returnObject.errorCode = -1;
		railway.logger.write(returnObject);
		send(JSON.stringify(responseObject));
	}
  }

  /**
   * private action method on Logout controller
   * @return JSON
   */
  based._doLogout = function () {
	based.userContext.IsLogin(based.UUID,function (responseObject) {
		var responseObj = {
			success: true,
			errorCode: 0,
			error: ""
		}
		send(JSON.stringify(responseObj));;
		if (responseObject.status) {
			delete req.session.user_id;
			based.userContext.Logout(based.UUID,function (responseObject) {});
		}
	});
  }

  /**
   * private action method on GetProfile controller
   * @return JSON
   */
  based._doGetProfile = function() {
	var userId = based.Base58.decode(req.session.user_id);
	based.userContext.GetProfile(userId,function (responseObject) {
		send(JSON.stringify(responseObject));
	});
  }

  /**
   * private action method on GetUserGroup controller
   * @return JSON
   */
  based._doGetUserGroup = function() {
	var userId = based.Base58.decode(req.session.user_id);
	based.userContext.GetUserGroups(userId,function (responseObject) {
		send(JSON.stringify(responseObject));
	});
  }

  /**
   * private action method on GetUser controller
   * @return JSON
   */
  based._doGetUser = function() {
	var userId = based.Base58.decode(req.session.user_id);
	based.userContext.GetUser(userId,function (responseObject) {
		send(JSON.stringify(responseObject));
	});
  }
  based._doUserList = function () {
	var returnObj  = {
		users:[],
		success: false,
		errorCode: 0,
		error: ""
	};
  	var groupId = -1;
  	var limit = 30;
  	var page = 0
  	try {
	  	if (parseInt(req.query.groupid,10) != -1) 
	  		groupId = based.Based58.decode(req.query.groupid);
	  	if (parseInt(req.query.limit,10) != -1);
	  		limit = parseInt(req.query.limit,10)
	  	if (parseInt(req.query.page,10) != -1) 
	  		page = parseInt(req.query.page,10);
	}
	catch (err) {
        railway.logger.write(err);
        returnObject.error  = "Arguments error";
        returnObject.errorCode = 403;
		send(JSON.stringify(returnObj));
		return;
	}
	based.userContext.GetUserList(groupId,limit,page,function (responseObject) {
		send(JSON.stringify(responseObject));
	});
  }
}
/**Dont remove**/
new users_control(); 