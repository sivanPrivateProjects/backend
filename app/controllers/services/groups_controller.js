//require with ever controller 
/**Controller Logic **/
load('application');
var groups_control = function () {
	var based = this;
	based.Base58 = require('base58');
	based.UUID = "";
	based.userContext = null;
	try {
		PMP = app.ApplicationSharedObjects.PMP;
    	SettingsConfigObject = app.ApplicationSharedObjects.Configs.Settings;
		before(function(){ based._verfiedGroup(); }, {except: 'GetGroups'});
		before(function(){ based._initGetGroups(); }, {only: 'GetGroups'});
		action('AddGroup', function () { based._doAddGroup(); });
		action('DeleteGroup', function () { based._doDeleteGroup(); });
		action('GetGroups', function () { based._doGetGroups(); });
		action('GetGroupProfile', function () { based._doGetGroupProfile(); });
		action('BindUserToGroup', function () { based._doBindUserToGroup(); });
		action('UnBindUserToGroup', function () { based._doUnBindUserToGroup(); });
		action('ToggelStatusGroup', function () { based._doToggelStatusGroup(); });
		action('MoveGroupUsers', function () { based._doHasPremmision(); });
		action('IsGroupPartOfRole', function () { based._doIsGroupPartOfRole(); });
		action('ScanCachedGroups', function () { based._doScanCachedGroups(); });
	}
	catch (err) {
		railway.logger.write(err);
	}
	based._verfiedGroup = function () {
		var uriUUID = req.url.split("/")[3];
		var responseObj = {
			success: false,
			errorCode: 404,
			error: "Incorrect User Seesion"
		}
		based.UUID = uriUUID;
		new PMP.Models.Users(true,uriUUID,function (responseObject) { 
			if (responseObject != null) {
				based.userContext = responseObject; 
				new PMP.Models.Groups(uriUUID,responseObject,function (responseBased){
				responseObj.error = "Incorrect Group";
				if (responseBased != null) {
					try {
						based.context = responseBased;
						based.UserId = based.Base58.decode(req.session.user_id);
						next();
					}
					catch (err) {
						railway.logger.write(err);
						send(JSON.stringify(responseObj));
					}
				}
				else {
					send(JSON.stringify(responseObj));
				}},false);
			}
			else {
				send(JSON.stringify(responseObj));
			}},function (){ 
				send(JSON.stringify(responseObj));
		});
	}
	based._initGetGroups = function () {
		based.context = new PMP.Models.Groups(-1,based.userContext,function (responseGroupObj){
			next();
		},error,true); 
		var error = function (){ 
			var responseObj = {
				success: false,
				errorCode: 101,
				error: "Incorrect Server Failer!!"
			}
			send(JSON.stringify(responseObj));
		};
	}
	based._doGetGroups = function () {
		var returnObject = {
			success: false,
			Groups: [],
			errorCode: 0,
			error: ""
		};
		var localPrivateKeyRef = req.query.hash;
		if (localPrivateKeyRef != undefined &&localPrivateKeyRef != null &&localPrivateKeyRef != "") {
			var Hashes = require("jshashes");
			var SHA512 = new Hashes.SHA512;
			var MD5 = new Hashes.MD5;
			var encryptLocalkeyRef = MD5.hex(SHA512.b64_hmac(SHA512.hex_hmac(SHA512.hex(PMP.Settings.frontend_addr_localPrivateKey),PMP.Settings.frontend_addr_localPrivateKey)),PMP.Settings.frontend_addr_localPrivateKey) 
			if (localPrivateKeyRef == encryptLocalkeyRef) {
				based.context.GetGroups(function (responseObject){
					send(JSON.stringify(responseObject));
				})
			}
			else {
				returnObject.error = "Hasking, Recored been loaded";
				returnObject.errorCode = -1;
				railway.logger.write(returnObject);
				send(JSON.stringify(responseObject));
			}
		}
		else {
			returnObject.error = "Hasking, Recored been loaded";
			returnObject.errorCode = -1;
			railway.logger.write(returnObject);
			send(JSON.stringify(responseObject));
		}
	}

	based._doIsGroupPartOfRole = function () {
		var groupId = based.Based58.decode(req.query.groupid);
		var roleid = based.Based58.decode(req.query.roleid);
		based.context.IsGroupPartOfRole(groupId,roleid,function (responseObject){
			send(JSON.stringify(responseObject))
		});
	}

	based._doAddGroup = function () {
		//Post data: name,description,email
		based.context.AddGroup(req.body.name,req.body.description,req.body.email,function (responseObject){
			send(JSON.stringify(responseObject))
		});
	}

	based._doMoveGroupUsers = function () {
		//au is number encode with base58
		//all the request on is like json -> data={groupid:aU,moveGroupId:AuA , users: [au,au,au] }
		var groupId = based.Based58.decode(req.query.groupid);
		var moveGroupId = based.Based58.decode(respones.moveGroupId);
		var userarr = respones.users;
		based.context.MoveGroupUsers(groupId,moveGroupId,userarr,function (responseObject){
			send(JSON.stringify(responseObject));
		});
	}

	based._doToggelStatusGroup = function () {
		var groupId = based.Based58.decode(req.query.groupid);
		based.context.ToggelStatusGroup(groupId,function (responseObject){
			send(JSON.stringify(responseObject));
		});
	}

	based._doGetGroupProfile = function () {
		var groupId = based.Based58.decode(req.query.groupid);
		based.context.GetGroupProfile(groupId,function (responseObject){
			send(JSON.stringify(responseObject))
		});
	}

	based._doDeleteGroup = function () {
		var groupId = based.Based58.decode(req.query.groupid);
		based.context.DeleteGroup(groupId,function (responseObject){
			send(JSON.stringify(responseObject))
		});
	}
	based._doBindUserToGroup =  function () {
		var groupId = based.Based58.decode(req.query.groupid);
		var uidId = based.Based58.decode(req.query.uid);
		based.context.BindUserToGroup(groupId,uidId,function (responseObject){
			send(JSON.stringify(responseObject));
		});

	}
	based._doBindUserToGroup =  function () {
		var groupId = based.Based58.decode(req.query.groupid);
		var uidId = based.Based58.decode(req.query.uid);
		based.context.UnBindUserToGroup(groupId,uidId,function (responseObject){
			send(JSON.stringify(responseObject));
		});

	}
	based._doScanCachedGroups = function () {
		var returnObject = {
			success: false,
			errorCode: 0,
			error: "",
			checksum: {
				Sum: 0,
				Errors:[]
			}
		};
		PMP.Utills.SendRestRequest(
			PMP.Utills.Sprintf(SettingsConfigObject.FEURIS.Caching.FetchGroups.URI,req.session.RealUUID),
			SettingsConfigObject.FEURIS.Caching.FetchGroups.Type,{},function (err,response) {
		if (response.success) {
				_.each(response.Groups, function(item) {
					based.context.AddGroup(item.Name,item.Description,item.Email,function (responseObj){
						if (responseObj.success) {
							returnObject.checksum.Sum++;
						}
						else {
							returnObject.checksum.Errors.push(item.Name);
						}
					});
				});
				send(JSON.stringify(returnObject));
			}
			else {
				railway.logger.write("Group Caching System Went Wrong, Response give Error: <pre>"+JSON.stringify(response)+"</pre>");
				returnObject.Error = "Something Want Wrong Internal Error";
				returnObject.ErrorCode =500;
				send(JSON.stringify(returnObject));
			}
		});
	}
} 
/**Dont remove**/
new groups_control();