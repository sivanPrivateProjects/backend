//require with ever controller 
/**Controller Logic **/
load('application');
var site_control = function () {
  var based = this;
  based.Base58 = require('base58');
  based.UUID = "";
  based.userContext = null;
  try {
    PMP = app.ApplicationSharedObjects.PMP;
    before(function(){ based._verfiedSite(); },{only: [
                                                        'AddSite',
                                                        'EditSite',
                                                        'DeleteSite',
                                                        'AddSiteInputs',
                                                        'DeleteSiteInputs',
                                                        'GetSiteInputs',
                                                        'SiteAddDomain',
                                                        'SiteRemoveDomain',
                                                        'GetSiteByDomain',
                                                        'IsExistSiteDomain',
                                                        'SiteDomainList',
                                                        'BindSiteToUser',
                                                        'IsBindSiteToUser',
                                                        'UnBindSiteToUser',
                                                        'IsArciveSite',
                                                        'AddSiteInputValue',
                                                        'EditSiteInputValue',
                                                        'GetSiteInputValue',
                                                        'ClearArciveSite',
                                                        'ScanSites',
                                                        'MarkScanSite',
                                                      ]});
    before(function(){ based._verfiedSiteIOs(); },{only: [
                                                        'AddSiteIcon',
                                                        'DeleteSiteIcon',
                                                        'ArciveSiteIcon',
                                                        'IsArciveSiteIcon',
                                                        'IsReadonlySiteIcon',
                                                        'IsExistedSiteIcon',
                                                        'AddSiteIcon2Folder',
                                                        'DeleteSiteIconFromFolder',
                                                        'AddSiteFolder',
                                                        'DeleteSiteFolder',
                                                        'ArciveSiteFolder',
                                                        'IsArciveSiteFolder',
                                                        'IsReadonlySiteFolder',
                                                        'IsExistedSiteFolder'
                                                      ]});
    /*** Site Controller ***/
    action('AddSite', function () { based._doAddSite(); });
    action('EditSite', function () { based._doEditSite(); });
    action('DeleteSite', function () { based._doDeleteSite(); });
    action('AddSiteInputs', function () { based._doAddSiteInput(); });
    action('DeleteSiteInputs', function () { based._doDeleteSiteInputs(); });
    action('GetSiteInputs', function () { based._doGetSiteInputs(); });
    action('SiteAddDomain', function () { based._doSiteAddDomain(); });
    action('SiteRemoveDomain', function () { based._doSiteRemoveDomain(); });
    action('GetSiteByDomain', function () { based._doGetSiteByDomain(); });
    action('IsExistSiteDomain', function () { based._doIsExistSiteDomain(); });
    action('SiteDomainList', function () { based._doSiteDomainList(); });
    action('BindSiteToUser', function () { based._doBindSiteToUser(); });
    action('IsBindSiteToUser', function () { based._doIsBindSiteToUser(); });
    action('UnBindSiteToUser', function () { based._doUnBindSiteToUser(); });
    action('IsArciveSite', function () { based._doIsArciveSite(); });
    action('SiteList', function () { based._doSiteList(); });
    action('AddSiteInputValue', function () { based._doAddSiteInputValue(); });
    action('EditSiteInputValue', function () { based._doEditSiteInputValue(); });
    action('GetSiteInputValue', function () { based._doGetSiteInputValue(); });
    action('ClearArciveSite', function () { based._doClearArciveSite(); });
    action('ScanSites', function () { based._doScanSites(); });
    action('MarkScanSite', function () { based._doMarkScanSite(); });
    /*** SiteIO Controller ***/
    action('AddSiteIcon', function () { based._doAddSiteIcon(); });
    action('DeleteSiteIcon', function () { based._doDeleteSiteIcon(); });
    action('ArciveSiteIcon', function () { based._doArciveSiteIcon(); });
    action('IsArciveSiteIcon', function () { based._doIsArciveSiteIcon(); });
    action('IsReadonlySiteIcon', function () { based._doIsReadonlySiteIcon(); });
    action('IsExistedSiteIcon', function () { based._doIsExistedSiteIcon(); });
    action('AddSiteIcon2Folder', function () { based._doAddSiteIcon2Folder(); });
    action('DeleteSiteIconFromFolder', function () { based._doDeleteSiteIconFromFolder(); });
    action('AddSiteFolder', function () { based._doAddSiteFolder(); });
    action('DeleteSiteFolder', function () { based._doDeleteSiteFolder(); });
    action('ArciveSiteFolder', function () { based._doArciveSiteFolder(); });
    action('IsArciveSiteFolder', function () { based._doIsArciveSiteFolder(); });
    action('IsReadonlySiteFolder', function () { based._doIsReadonlySiteFolder(); });
    action('IsExistedSiteFolder', function () { based._doIsExistedSiteFolder(); });
  }
  catch (err) {
    railway.logger.write(err);
  }
  based._verfiedSite = function () {
    var uriUUID = req.url.split("/")[3];
    var siteUriUUID = req.url.split("/")[4];
    var responseObj = {
      success: false,
      errorCode: 404,
      error: "Incorrect User Seesion"
    }
    based.UUID = uriUUID;
    new PMP.Models.Users(true,uriUUID,function (responseObject) { 
      if (responseObject != null) {
        based.userContext = responseObject; 
        new PMP.Models.Site(siteUriUUID,uriUUID,responseObject,function (responseBased){
          responseObj.error = "Incorrect Site";
          if (responseBased != null) {
            try {
              based.Context = responseBased;
              based.UserId = based.Base58.decode(req.session.user_id);
              next();
            }
            catch (err) {
              railway.logger.write(err);
              send(JSON.stringify(responseObj));;
            }
          }
          else {
            send(JSON.stringify(responseObj));;
          }
        });
      }
      else {
        send(JSON.stringify(responseObj));;
      }
    },function (){ 
      send(JSON.stringify(responseObj));; 
    });
  }


  based._verfiedSiteIOs = function () {
    var uriUUID = req.url.split("/")[3];
    var siteUriUUID = req.url.split("/")[4];
    var responseObj = {
      success: false,
      errorCode: 404,
      error: "Incorrect User Seesion"
    }
    based.UUID = uriUUID;
    new PMP.Models.Users(true,uriUUID,function (responseObject) { 
      if (responseObject != null) {
        based.userContext = responseObject; 
        new PMP.Models.Site(siteUriUUID,uriUUID,responseObject,function (responseBased){
          responseObj.error = "Incorrect Site";
          if (responseBased != null) {
            try {
              new PMP.Models.SiteIOs(siteUriUUID,uriUUID,responseObject,function (r){
                based.Context = responseBased;
                based.ContextIO = r;
                based.UserId = based.Base58.decode(req.session.user_id);
                next();
              });
            }
            catch (err) {
              railway.logger.write(err);
              send(JSON.stringify(responseObj));;
            }
          }
          else {
            send(JSON.stringify(responseObj));;
          }
        });
      }
      else {
        send(JSON.stringify(responseObj));;
      }
    },function (){ 
      send(JSON.stringify(responseObj));; 
    });
  }
  
  based._doAddSite = function() {
    var responseObj = {
        success: false,
        errorCode: 404,
        error: "Incorrect User Seesion"
    }
    var secId = based.Base58.decode(req.body.sec_id);
    var name = req.body.name;
    var desc = req.body.desc;
    var isSupported = req.body.isSupported;
    var isBlocked = req.body.isBlocked;
    new PMP.Models.SecurtyLevels(based.UUID,based.userContext,function (responseBased){
      responseObj.error = "Incorrect Securty";
      if (responseBased != null) {
        try {
          responseBased.IsExistSecurtyLevel(secId,function (response) {
              if (response.status) {
                based.Context.AddSite(secId,name,desc,isSupported,isBlocked,function (responseObjec) {
                    send(responseObjec);
                });
              }
          });
        }
        catch (err) {
          railway.logger.write(err);
          send(JSON.stringify(responseObj));;
        }
      }
      else {
        send(JSON.stringify(responseObj));;
      }
    });
  }
  
  based._doEditSite = function() {
    var responseObj = {
        success: false,
        errorCode: 404,
        error: "Incorrect User Seesion"
    }
    var secId = based.Base58.decode(req.body.sec_id);
    var name = req.body.name;
    var desc = req.body.desc;
    var isSupported = req.body.isSupported;
    var isBlocked = req.body.isBlocked;
    new PMP.Models.SecurtyLevels(based.UUID,based.userContext,function (responseBased){
      responseObj.error = "Incorrect Securty";
      if (responseBased != null) {
        try {
          responseBased.IsExistSecurtyLevel(secId,function (response) {
              if (response.status) {
                based.Context.EditSite(secId,name,desc,isSupported,isBlocked,function (responseObjec) {
                    send(responseObjec);
                });
              }
          });
        }
        catch (err) {
          railway.logger.write(err);
          send(JSON.stringify(responseObj));;
        }
      }
      else {
        send(JSON.stringify(responseObj));;
      }
    });
  }
  based._doDeleteSite = function() {
    based.Context.DeleteSite(function (response) {
        send(response);
    });
  }
  
  based._doAddSiteInput = function() {
    var responseObj = {
        success: false,
        errorCode: 404,
        error: "Incorrect Arguments"
    }
    var data  = req.body.data;
    try {
        data = JSON.parse(data);
        based.Context.AddSiteInputs(data,function (response) {
            send(response);
        });
    }
    catch(err) {
        send(JSON.stringify(responseObj));;
    }
  }
  
  based._doDeleteSiteInputs = function() {
    based.Context.DeleteSiteInputs(function (response) {
        send(response);
    });
  }
  
  based._doGetSiteInputs = function () {
    based.Context.GetSiteInputs(function (response) {
        send(response);
    });
  }
  
  based._doSiteAddDomain = function () {
        var responseObj = {
            success: false,
            errorCode: 403,
            error: "Domain Existed under this site"
        }
      var domain  = req.query.domain;
      var secure  = req.query.secure;
      
      based.Context.IsExistSiteDomain(domain,secure,function (response) { 
          if (response.status) {
            based.Context.SiteAddDomain(domain,secure,function (r) { send(r); });
          }
          else {
              send(JSON.stringify(responseObj));;
          }
      });
  }
  
  based._doSiteRemoveDomain = function () {
        var responseObj = {
            success: false,
            errorCode: 403,
            error: "Domain not Existed under this site"
        }
      var domain  = req.query.domain;
      var secure  = req.query.secure;
      
      based.Context.IsExistSiteDomain(domain,secure,function (response) { 
          if (response.status) {
            based.Context.SiteRemoveDomain(domain,secure,function (r) { send(r); });
          }
          else {
              send(JSON.stringify(responseObj));;
          }
      });
  }
  
  based._doGetSiteByDomain = function () {
  
        var responseObj = {
            success: false,
            errorCode: 403,
            error: "Domain not Existed under this site"
        }
      var domain  = req.query.domain;
      var secure  = req.query.secure;
      
      based.Context.IsExistSiteDomain(domain,secure,function (response) { 
          if (response.status) {
            based.Context.GetSiteByDomain(domain,secure,function (r) { send(r); });
          }
          else {
              send(JSON.stringify(responseObj));;
          }
      });
  }
  
  based._doIsExistSiteDomain = function () {
      var domain  = req.query.domain;
      var secure  = req.query.secure;
      
      based.Context.IsExistSiteDomain(domain,secure,function (response) {      
        send(response);
      });
  }
  
  based._doSiteDomainList = function () {
    based.Context.SiteDomainList(function (r) { send(r); });
  }
  
  based._doBindSiteToUser = function() {
    var responseObj = {
        success: false,
        errorCode: 404,
        error: "Incorrect User Seesion"
    }
    var secId = based.Base58.decode(req.body.sec_id);
    new PMP.Models.SecurtyLevels(based.UUID,based.userContext,function (responseBased){
      responseObj.error = "Incorrect Securty";
      if (responseBased != null) {
        try {
          responseBased.IsExistSecurtyLevel(secId,function (response) {
              if (response.status) {
                based.Context.BindSiteToUser(secId,function (responseObjec) {
                    send(responseObjec);
                });
              }
          });
        }
        catch (err) {
          railway.logger.write(err);
          send(JSON.stringify(responseObj));;
        }
      }
      else {
        send(JSON.stringify(responseObj));;
      }
    });
  }
  
  based._doUnBindSiteToUser = function () {
    based.Context.UnBindSiteToUser(function (r) { send(r); });
  }
  
  based._doIsBindSiteToUser = function () {
    based.Context.IsBindSiteToUser(function (r) { send(r); });
  }
  
  based._doIsArciveSite = function () {
    based.Context.IsArciveSite(function (r) { send(r); });
  }
  
  based._doSiteList = function () {
      //start,limit,isShowSupported,isShowBlocked,isShowArcived     
      var options = {
        start: 0,
        limit: parseInt(PMP.Settings.limitResulte,10),
        isShowSupported: false,
        isShowBlocked:false,
        isShowArcived:false
      }
      var start = req.query.start;
      var limit = req.query.limit;
      var isShowSupported = req.query.isShowSupported;
      var isShowBlocked = req.query.isShowBlocked;
      var isShowArcived = req.query.isShowArcived;
      
      based.Validator = validator;
      based.Validator.check(start).notEmpty().isInt();
      
      if (based.Validator.hasErrors()) {
          based.Validator = validator;
          options.start = 0;
      } else { options.start = start; }
      based.Validator.check(limit).notEmpty().isInt();
      
      if (based.Validator.hasErrors()) {
          based.Validator = validator;
          options.limit = 0;
      } else { options.limit = limit; }
      options.isShowSupported = sanitize(isShowSupported).toBoolean();
      options.isShowBlocked = sanitize(isShowBlocked).toBoolean();
      options.isShowArcived = sanitize(isShowArcived).toBoolean();
      based.Context.SiteList(options,function (r) { send(r); });
  }
  
  based._doAddSiteInputValue =  function () {
      var responseObj = {
        success: false,
        errorCode: 404,
        error: "Incorrect Arguments"
    }
    var data  = req.body.data;
    try {
        data = JSON.parse(data);
        based.Context.AddSiteInputValue(data,function (response) {
            send(response);
        });
    }
    catch(err) {
        send(JSON.stringify(responseObj));;
    }
  }
  
  based._doEditSiteInputValue =  function () {
      var responseObj = {
        success: false,
        errorCode: 404,
        error: "Incorrect Arguments"
    }
    var data  = req.body.data;
    try {
        data = JSON.parse(data);
        based.Context.EditSiteInputValue(data,function (response) {
            send(response);
        });
    }
    catch(err) {
        send(JSON.stringify(responseObj));;
    }
  }
  based._doGetSiteInputValue = function() {
    based.Context.GetSiteInputValues(function (r) { send(r); });
  }
          
  based._doClearArciveSite = function() {
    based.Context.ClearArciveSite(function (r) { send(r); });
  }
  
  based._doScanSites = function() {
    based.Context.ClearArciveSite(function (r) { send(r); });
  }
  
  based._doScanSites = function() {
    based.Context.ScanSites(function (r) { send(r); });
  }
  
  based._doMarkScanSite = function() {
    based.Context.MarkScanSite(function (r) { send(r); });
  } 


  /*** Site Icons & Folders ***/

  based._doAddSiteIcon = function() {
    var responseObj = {
        success: false,
        errorCode: 404,
        error: "Incorrect User Seesion"
    }
    var secId = based.Base58.decode(req.body.sec_id);
    var siteId = based.Context.SiteID;
    var userId = based.UserId;
    var title = req.body.title;
    var icon_path = req.body.icon_path;
    var alt = req.body.alt;
    var isReadonly = req.body.isReadonly;
    new PMP.Models.SecurtyLevels(based.UUID,based.userContext,function (responseBased){
      responseObj.error = "Incorrect Securty";
      if (responseBased != null) {
        try {
          responseBased.IsExistSecurtyLevel(secId,function (response) {
              if (response.status) {
                based.ContextIO.AddSiteIOs(siteId,userId,title,icon_path,alt,isReadonly,secId,function (responseObjec) {
                    send(responseObjec);
                });
              }
          });
        }
        catch (err) {
          railway.logger.write(err);
          send(JSON.stringify(responseObj));;
        }
      }
      else {
        send(JSON.stringify(responseObj));;
      }
    });
  }

  based._doDeleteSiteIcon = function() {
    var userId = based.UserId;
    var siteId = based.Context.SiteID;
    var sic_id  = based.Base58.decode(req.query.sic_id);//sic = site icon id
    based.ContextIO.DeleteSiteIOs(siteId,userId,sic_id,function (r) { send(r); });
  } 

  based._doDeleteSiteFolder = function() {
    var userId = based.UserId;
    var fol_id  = based.Base58.decode(req.query.fod_id);//fol_id = folder id
    based.ContextIO.DeleteSiteIOsFolder(userId,fol_id,function (r) { send(r); });
  } 

  based._doAddSiteFolder = function() {
    var responseObj = {
        success: false,
        errorCode: 404,
        error: "Incorrect User Seesion"
    }
    var secId = based.Base58.decode(req.body.sec_id);
    var userId = based.UserId;
    var title = req.body.title;
    var alt = req.body.alt;
    var isReadonly = req.body.isReadonly;
    new PMP.Models.SecurtyLevels(based.UUID,based.userContext,function (responseBased){
      responseObj.error = "Incorrect Securty";
      if (responseBased != null) {
        try {
          responseBased.IsExistSecurtyLevel(secId,function (response) {
              if (response.status) {
                based.ContextIO.AddSiteIOsFolder(userId,title,alt,isReadonly,secId,function (responseObjec) {
                    send(responseObjec);
                });
              }
          });
        }
        catch (err) {
          railway.logger.write(err);
          send(JSON.stringify(responseObj));;
        }
      }
      else {
        send(JSON.stringify(responseObj));;
      }
    });
  }
  based._doIsArciveSiteIcon = function() {
    var userId = based.UserId;
    var siteId = based.Context.SiteID;
    var sic_id  = based.Base58.decode(req.query.sic_id);//sic = site icon id
    based.ContextIO.IsArciveSiteIOs(siteId,userId,sic_id,function (r) { send(r); });
  }
  
  based._doIsReadonlySiteIcon = function() {
    var userId = based.UserId;
    var siteId = based.Context.SiteID;
    var sic_id  = based.Base58.decode(req.query.sic_id);//sic = site icon id
    based.ContextIO.IsReadonlySiteIOs(siteId,userId,sic_id,function (r) { send(r); });
  }
  
  based._doIsExistedSiteIcon = function() {
    var userId = based.UserId;
    var siteId = based.Context.SiteID;
    var sic_id  = based.Base58.decode(req.query.sic_id);//sic = site icon id
    based.ContextIO.IsExistSiteIOs(siteId,userId,sic_id,function (r) { send(r); });
  }
  
  based._doIsExistedSiteFolder = function() {
    var userId = based.UserId;
    var fol_id  = based.Base58.decode(req.query.fod_id);//fol_id = folder id
    based.ContextIO.IsExistSiteIOsFolder(userId,fol_id,function (r) { send(r); });
  }
  
  based._doIsReadonlySiteFolder = function() {
    var userId = based.UserId;
    var fol_id  = based.Base58.decode(req.query.fod_id);//fol_id = folder id
    based.ContextIO.IsReadonlySiteIOsFolder(userId,fol_id,function (r) { send(r); });
  }

  based._doIsArciveSiteFolder = function() {
    var userId = based.UserId;
    var fol_id  = based.Base58.decode(req.query.fod_id);//fol_id = folder id
    based.ContextIO.IsArciveSiteIOsFolder(userId,fol_id,function (r) { send(r); });
  } 

  based._doArciveSiteIcon = function() {
    var userId = based.UserId;
    var siteId = based.Context.SiteID;
    var sic_id  = based.Base58.decode(req.query.sic_id);//sic = site icon id
    based.ContextIO.ArciveSiteIOs(siteId,userId,sic_id,function (r) { send(r); });
  }
  
  based._doArciveSiteFolder = function() {
    var userId = based.UserId;
    var fol_id  = based.Base58.decode(req.query.fod_id);//fol_id = folder id
    based.ContextIO.ArciveSiteIOsFolder(userId,fol_id,function (r) { send(r); });
  }

  based._doAddSiteIcon2Folder = function() {
    var userId = based.UserId;
    var siteId = based.Context.SiteID;
    var sic_id  = based.Base58.decode(req.query.sic_id);//sic = site icon id
    var fol_id  = based.Base58.decode(req.query.fod_id);//fol_id = folder id
    based.ContextIO.BindSiteIOs2SiteIOsFolder(siteId,userId,sic_id,fol_id,function (r) { send(r); });
  }
  
  based._doDeleteSiteIconFromFolder = function() {
    var userId = based.UserId;
    var siteId = based.Context.SiteID;
    var sic_id  = based.Base58.decode(req.query.sic_id);//sic = site icon id
    var fol_id  = based.Base58.decode(req.query.fod_id);//fol_id = folder id
    based.ContextIO.UnBindSiteIOs2SiteIOsFolder(siteId,userId,sic_id,fol_id,function (r) { send(r); });
  }
  
} 
/**Dont remove**/
new site_control();