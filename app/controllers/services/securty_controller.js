load('application');
var securty_control = function () {
  var based = this;
  based.Base58 = require('base58');
  based.UUID = "";
  based.userContext = null;
  try {
    PMP = app.ApplicationSharedObjects.PMP
    before(function(){ based._verfiedGroup(); });
    action('AddSecurty', function () { based._doAddSecurty(); });
    action('EditSecurty', function () { based._doEditSecurty(); });
    action('DeleteSecurty', function () { based._doDeleteSecurty(); });
    action('GetSecurtyList', function () { based._doGetSecurtyList(); });
    action('GetSecurty', function () { based._doGetSecurty(); });
    action('IsExistSecurty', function () { based._doIsExistSecurty(); });
  }
  catch (err) {
    railway.logger.write(err);
  }
  based._verfiedGroup = function () {
    var uriUUID = req.url.split("/")[3];
    var responseObj = {
      success: false,
      errorCode: 404,
      error: "Incorrect User Seesion"
    }
    based.UUID = uriUUID;
    new PMP.Models.Users(true,uriUUID,function (responseObject) { 
      if (responseObject != null) {
        based.userContext = responseObject; 
        new PMP.Models.SecurtyLevels(uriUUID,responseObject,function (responseBased){
          responseObj.error = "Incorrect Securty";
          if (responseBased != null) {
            try {
              based.Context = responseBased;
              based.UserId = based.Base58.decode(req.session.user_id);
              next();
            }
            catch (err) {
              railway.logger.write(err);
              send(JSON.stringify(responseObj));;
            }
          }
          else {
            send(JSON.stringify(responseObj));;
          }
        });
      }
      else {
        send(JSON.stringify(responseObj));;
      }
    },function (){ 
      send(JSON.stringify(responseObj));;
    });
  }
  
  based._doAddSecurty = function() { 
      var name = req.body.name;
      var desc = req.body.desc;
      var isFullFromReq = req.body.isFullFromReq;
      var isPasswordReq = req.body.isPasswordReq;
      based.Context.AddSecurtyLevel(name,desc,isFullFromReq,isPasswordReq,function (response) {
          send(response);
      });
  }
  
  based._doEditSecurty = function() { 
      var secId = based.Base58.decode(req.body.sec_id);
      var name = req.body.name;
      var desc = req.body.desc;
      var isFullFromReq = req.body.isFullFromReq;
      var isPasswordReq = req.body.isPasswordReq;
      based.Context.EditSecurtyLevel(secId,name,desc,isFullFromReq,isPasswordReq,function (response) {
          send(response);
      });
  }
  
  based._doDeleteSecurty = function () {
      var secId = based.Base58.decode(req.query.sec_id);
      based.Context.DeleteSecurtyLevel(secId,function (response) {
          send(response);
      });
  }
  
  based._doGetSecurtyList = function () {
      based.Context.GetSecurtyLevelList(function (response) {
          send(response);
      });
  }
  
  based._doGetSecurty = function () {
      var secId = based.Base58.decode(req.query.sec_id);
      based.Context.GetSecurtyLevel(secId,function (response) {
          send(response);
      });
  }
  
  based._doIsExistSecurty = function () {
      var secId = based.Base58.decode(req.query.sec_id);
      based.Context.IsExistSecurtyLevel(secId,function (response) {
          send(response);
      });
  }
} 
/**Dont remove**/
new securty_control();