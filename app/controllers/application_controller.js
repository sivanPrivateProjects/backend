before(function () {
    PMP = app.ApplicationSharedObjects.PMP;
    var settings = new PMP.Models.Settings();
    settings.LoadSettings(function (responseObject) {
      if (responseObject.isBlocker) res.end("Application Not Loaded");
      else {
        PMP.Settings = responseObject.Settings;
      }
      next();
    });
});
before('protect from forgery', function () {
    protectFromForgery('4caa949ce0d2726a154aeccb2e6e18a82076f1ed');
});