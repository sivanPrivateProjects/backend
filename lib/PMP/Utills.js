PMP.Utills = new JS.Singleton({
    GetRealIp: function  () {
        var ip_address = null;
        try {
                ip_address = req.headers['x-forwarded-for'];
        }
        catch ( error ) {
                ip_address = req.connection.remoteAddress;
        }
        return ip_address;
    },
    isEmptyCookie: function (cookie) {
        if (!_.isUndefined(site)||!_.isNull(site)) {
            return true;
        }
        return false;
    },
    /**
     * Will send Call for any rest service and return data
     * its use of any services type not ours
     * @param {string}   url      uri
     * @param {string}   type     method of the request
     * @param {object}   data     object to send thu rest
     * @param {Function} callback call back
     */
    SendRestRequestAnyService: function (url,type,data,callback) {
        var HTTP_REQUEST_TIMEOUT = 3000;
        var dataStr = JSON.stringify(data);
        var request1 = require('request');
        var options = {
            body: dataStr,
            method: type,
            uri:url,
            timeout: HTTP_REQUEST_TIMEOUT
        }
        if (type == "DELETE" || type == "GET" ) {
            delete options.body;
        }
        else {
            delete options.qs;
        }
        request(options,callback);
        function request(options, cb) {
            if(options.encoding == 'iso-8859-8') {
                options.encoding = 'binary';
                return request1(options, function(error, request, data) {
                    try {
                        var data2 = iconv_iso8859_8i_to_utf8.convert(iconv_utf8_to_latin.convert(data)).toString('utf8') //conver buffer to string
                    } catch(e) {
                        data2 = null;
                        error = e;
                    }
                    cb(error, data2);
                });
            } else {
                return request1(options, function(error, request, data) {
                    cb(error,  data);
                });
            }
        }
    },
    /**
     * Will send Call for any rest service and return data
     * this used for our services type
     * @param {string}   url      uri
     * @param {string}   type     method of the request
     * @param {object}   data     object to send thu rest
     * @param {Function} callback call back
     */
    SendRestRequest: function (url,type,data,callback) {
        var HTTP_REQUEST_TIMEOUT = 3000;
        var dataStr = JSON.stringify(data);
        var request1 = require('request');

        /* covert for encoding
        var Iconv = require('iconv').Iconv;
        var iconv_utf8_to_latin = new Iconv('utf-8', 'iso-8859-1');
        var iconv_iso8859_8i_to_utf8 = new Iconv('iso-8859-8', 'utf-8');
        var iconv_utf8_to_iso8859_8i = new Iconv('utf-8', 'iso-8859-8');
        exports.iconv_iso8859_8i_to_utf8 = iconv_iso8859_8i_to_utf8;
        exports.iconv_utf8_to_iso8859_8i = iconv_utf8_to_iso8859_8i;*/
        var options = {
            body: dataStr,
            method: type,
            uri:url,
            timeout: HTTP_REQUEST_TIMEOUT
        }
        if (type == "DELETE" || type == "GET" ) {
            delete options.body;
        }
        else {
            delete options.qs;
        }
        request(options,callback);
        function request(options, cb) {
            if(options.encoding == 'iso-8859-8') {
                options.encoding = 'binary';
                return request1(options, function(error, request, data) {
                    try {
                        var data2 = iconv_iso8859_8i_to_utf8.convert(iconv_utf8_to_latin.convert(data)).toString('utf8') //conver buffer to string
                    } catch(e) {
                        data2 = null;
                        error = e;
                    }
                    cb(error, data2);
                });
            } else {
                return request1(options, function(error, request, data) {
                    var responseObject = JSON.parse(data);
                    if (req.session.RealUUID == responseObject.UUID) {
                        cb(error,  responseObject);
                    }
                    else {
                        PMP.ReloadUserSession(req.session.localRealUserID,req.session.RealUUID,function (responseObj) {
                            if (!responseObj.success) {
                                cb(error,  responseObj); //send the logout if it failed
                            }
                            else {
                                responseObject.UUID = req.session.localUUID;
                                cb(error,  responseObject);
                            }
                        })
                    }
                });
            }
        }
    },
    PropSql: function (sql,params) {
        var based = this;
        if (!params || params.length < 1 ) {
            return;
        }
        var tParams = params;
        _.each(tParams,function (val,key) {
            tParams[key] = based._escapeString(val);
        });
        tParams = _.flatten([sql,tParams]);
        console.log(tParams);
        return based["Sprintf"].apply(based,tParams);
    },
    _escapeString: function(str) {
      str = str.replace(/\0/g, "\\0")
      str = str.replace(/\n/g, "\\n")
      str = str.replace(/\r/g, "\\r")
      str = str.replace(/\032/g, "\\Z")
      str = str.replace(/([\'\"]+)/g, "\\$1")

      return str
    },
    Sprintf: function () {
        if (!arguments || arguments.length < 1 || !RegExp) {
            return;
        }
        var str = arguments[0];
        var re = /([^%]*)%('.|0|\x20)?(-)?(\d+)?(\.\d+)?(%|b|c|d|u|f|o|s|x|X)(.*)/;
        var a = b = [], numSubstitutions = 0, numMatches = 0;
        while (a = re.exec(str)) {
            var leftpart = a[1], pPad = a[2], pJustify = a[3], pMinLength = a[4];
            var pPrecision = a[5], pType = a[6], rightPart = a[7];
            //alert(a + '\n' + [a[0], leftpart, pPad, pJustify, pMinLength, pPrecision);
            numMatches++;
            if (pType == '%') {
                subst = '%';
            }
            else {
                numSubstitutions++;
                if (numSubstitutions >= arguments.length) {
                    console.log('Error! Not enough function arguments (' + (arguments.length - 1) + ', excluding the string)\nfor the number of substitution parameters in string (' + numSubstitutions + ' so far).');
                }
                var param = arguments[numSubstitutions];
                var pad = '';
                if (pPad && pPad.substr(0, 1) == "'") pad = leftpart.substr(1, 1);
                else if (pPad) pad = pPad;
                var justifyRight = true;
                if (pJustify && pJustify === "-") justifyRight = false;
                var minLength = -1;
                if (pMinLength) minLength = parseInt(pMinLength);
                var precision = -1;
                if (pPrecision && pType == 'f') precision = parseInt(pPrecision.substring(1));
                var subst = param;
                if (pType == 'b') subst = parseInt(param).toString(2);
                else if (pType == 'c') subst = String.fromCharCode(parseInt(param));
                else if (pType == 'd') subst = parseInt(param) ? parseInt(param) : 0;
                else if (pType == 'u') subst = Math.abs(param);
                else if (pType == 'f') subst = (precision > -1) ? Math.round(parseFloat(param) * Math.pow(10, precision)) / Math.pow(10, precision) : parseFloat(param);
                else if (pType == 'o') subst = parseInt(param).toString(8);
                else if (pType == 's') subst = param;
                else if (pType == 'x') subst = ('' + parseInt(param).toString(16)).toLowerCase();
                else if (pType == 'X') subst = ('' + parseInt(param).toString(16)).toUpperCase();
            }
            str = leftpart + subst + rightPart;
        }
        return str;
    }
});