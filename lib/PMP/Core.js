fs = require('fs');
var PMP_Abstract = new JS.Class({
	Utills: null,
	Settings: {},
	Models: {}
});
PMP = new JS.Singleton(PMP_Abstract,{
	//on Application Start
    Init: function() {
		require('./Utills');
		this._loadModels();
    },
    _loadModels: function () {
		var _path = __dirname+'/Models/';
		var _files = fs.readdirSync(_path);
		_files.forEach(function(file){
			if (file != ".DS_Store") {
	    		require(_path+'/'+file);
	    	}
		});
    }
});

/*** Loader ***/
module.exports.Init = function () {
	PMP.Init();
	return PMP;
}