PMP.Models.Site = function (uuid,userUUID,userCtx,callback,failercallback) {
  var based = this;
  based.GUID = require('guid');	
	based.dbLink = app.ApplicationSharedObjects.Connection;
  if (based.GUID.isGuid(userUUID)) {
    if (based.GUID.isGuid(uuid)) { 
      if (based.userCtx.IsLoginsuccess) {
        based.IsExistSite(uuid,function (response) {
          if (response.success) {
            based.Context = response.Site;
            based.userCtx = userCtx;
            based.UUID = based.userCtx.UUID;
            based.siteUUID = uuid;
            based.isExisted = true;
            callback(based);
          }
          else {
            failercallback();
          }
        });
      }
      else {
        failercallback();
      }
    }
    else {
      if (based.userCtx.IsLoginsuccess) {
        based.userCtx = userCtx;
        based.UUID = based.userCtx.UUID;
        based.isExisted = false;
        callback();
      }
    }
	}
  else {
    failercallback();
  }
}

PMP.Models.Site.prototype.AddSite = function (securtyLevelId, name,desciprtion, isSupported, isBlocked ,callback) {
  var based= this;
  var uuidHash = based.GUID.create(1);
  var returnObject  = {
    UUID: based.userCtx.UUID,
    success: false,
    errorCode: 0,
    error: ""
  }
  var sqlIsExistedByName = "select id,isBlocked from sites where name = ? limit 1";
  based.dbLink.query(sqlIsExistedByName,[siteUUID],function (err,resultes, fields) {    
    if (err) {
      railway.logger.write(err);
      callback(returnObject);
    }
    if(resultes.length == 0) {
      _add(false);
    }
    else {
      var isBlocked = Boolean(resultes[0].isBlocked);
      if (isBlocked) _add(true,resultes[0].id);
    }
  });
  function _add(isExisted,siteId) {
    var sqlinsert = "insert into sites (security_level_id,name,uuid,description,isSupported,isBlocked,isArcived,last_scan_date) values (?,?,?,?,?,?,0,NOW())";
    var sqlupdateSite = "update sites set isArcived = 0 , update_date = NOW() where id = ?;";
    var sqlupdateSiteIos = "update sites_ios set isArcived = 0 where site_id = ? and user_id = ?;";
    var sqllocateIdSite = "select id from sites where uuid = ?";
    if (isExisted) {
      var trans = based.dbLink.startTransaction();
      trans.query(sqllocateIdSite,[siteId],function (err,resultes, fields) {    
        if (err) {
          railway.logger.write(err);
          trans.rollback();
          callback(returnObject);
        }
        var encodeSite_id =based.Base58.encode(resultes.insertId);
        var site_id =resultes.insertId;
        trans.query(sqlupdateSite,[site_id],function (err,resultes, fields) {    
          if (err) {
            railway.logger.write(err);
            trans.rollback();
            callback(returnObject);
          }
          trans.query(sqlupdateSiteIos,[site_id,based.userCtx.ID],function (err,resultes, fields) {    
            if (err) {
              railway.logger.write(err);
              trans.rollback();
              callback(returnObject);
            }
        	returnObject.SiteId = encodeSite_id;
            returnObject.success = true;
            trans.commit();
            callback(returnObject);
          });
        });
      });
      trans.execute();
    }
    else {
      var trans = based.dbLink.startTransaction();
      trans.query(sqlinsert,[securtyLevelId, name,uuidHash,desciprtion, isSupported, isBlocked],function (err,resultes, fields) {    
        if (err) {
          railway.logger.write(err);
          trans.rollback();
          callback(returnObject);
        }
        returnObject.success =true;
        returnObject.SiteUUID =uuidHash;
        trans.commit();
        callback(returnObject);
      });
      trans.execute();
    }
  }
}

PMP.Models.Site.prototype.EditSite = function (securtyLevelId,name,description, isSupported, isBlocked,callback) {
  var based = this;
  var returnObject  = {
    UUID: based.userCtx.UUID,
    success: false,
    errorCode: 0,
    error: ""
  }
  if (based.isExisted != undefined && based.isExisted) {
    var sqlupdateSite = "update sites set security_level_id= ?,name = ?, `desc` = ? , isSupported = ? , isBlocked = ? , update_date = NOW() where UUID = ?;";
    var trans = based.dbLink.startTransaction();
    trans.query(sqlupdateSite,[securtyLevelId, name,description, isSupported, isBlocked,based.siteUUID],function (err,resultes, fields) {    
      if (err) {
        railway.logger.write(err);
        trans.rollback();
        callback(returnObject);
      }
      returnObject.success =true;
      returnObject.SiteUUID =uuidHash;
      trans.commit();
      callback(returnObject);
    });
    trans.execute();
  }  
  else {
    returnObject.errorCode = 501;
    returnObject.error = "site not existed";
    callback(returnObject);
  }
}

PMP.Models.Site.prototype.DeleteSite = function (callback) {
  var based = this;
  var returnObject  = {
    UUID: based.userCtx.UUID,
    success: false,
    errorCode: 0,
    error: ""
  }
  if (based.isExisted != undefined && based.isExisted) {
    var sqlupdateSite = "update sites set isArcived = 1 , update_date = NOW() where id = ?;";
    var sqlupdateSiteIOs = "update sites_ios set isArcived = 1 where site_id = ? and user_id = ?;";
    var trans = based.dbLink.startTransaction();
    trans.query(sqlupdateSite,[based.SiteID],function (err,resultes, fields) {  
      if (err) {
        railway.logger.write(err);
        trans.rollback();
        callback(returnObject);
      }  
      trans.query(sqlupdateSiteIOs,[based.SiteID,based.userCtx.ID],function (err,resultes, fields) {    
        if (err) {
          railway.logger.write(err);
          trans.rollback();
          callback(returnObject);
        }
        returnObject.success =true;
        trans.commit();
        callback(returnObject);
      });
    });
    trans.execute();
  }  
  else {
    returnObject.errorCode = 501;
    returnObject.error = "site not existed";
    callback(returnObject);
  }
}

PMP.Models.Site.prototype.AddSiteInputs = function (arrInputs,callback) {
  var based = this;
  var returnObject  = {
    UUID: based.userCtx.UUID,
    success: false,
    errorCode: 0,
    error: ""
  }
  //post data need be like this data=[{name:"", lable:"" , markup:""}]
  if (based.isExisted != undefined && based.isExisted) {
    if (arrInputs.length!= 0) {
      var sqlSite = "insert into sites (site_id , name, lable, markup) values (?,?,?,?);";
      var trans = based.dbLink.startTransaction();
      _.each(arrInputs,function (item,key) {
        if (_.isObject(item)&&_matchObject(item))  {
          trans.query(sqlSite,[based.SiteID,item.name,item.lable,item.markup],function (err,resultes, fields) {  
            if (err) {
              railway.logger.write(err);
              trans.rollback();
              callback(returnObject);
            }  
            if (arrInputs.length <= key){
              returnObject.success =true;
              trans.commit();
              callback(returnObject);
            }
          });
        }
      });
      trans.execute();
    }
    else {
      returnObject.errorCode = 501;
      returnObject.error = "site not existed";
      callback(returnObject);
    }
  }  
  else {
    returnObject.errorCode = 501;
    returnObject.error = "site not existed";
    callback(returnObject);
  }
  function _matchObject(obj) {
    if (_.has(obj, "name") &&
        _.has(obj, "lable") &&
        _.has(obj, "markup")) 
      return true;
    return false;
  }
}


PMP.Models.Site.prototype.GetSiteInputs = function (callback) {
  var based = this;
  var returnObject  = {
    UUID: based.userCtx.UUID,
    Inputs: [],
    success: false,
    errorCode: 0,
    error: ""
  }
  if (based.isExisted != undefined && based.isExisted) {
      var sqlSite = "select name, lable, markup from sites_inputfields where site_id = ?";
      based.dbLink.query(sqlSite,[based.SiteID],function (err,resultes, fields) {    
        if (err) {
          railway.logger.write(err);
          callback(returnObject);
        }
        if(resultes.length != 0) {
          _.each(resultes,function (item){
            returnObject.Inputs.push(item);
          });
          returnObject.success = true;
          callback(returnObject);
        }
        else {
          returnObject.errorCode = 501;
          returnObject.error = "Corrept Site Data";
          callback(returnObject);
        }
      });
  }  
  else {
    returnObject.errorCode = 501;
    returnObject.error = "site not existed";
    callback(returnObject);
  }
  function _matchObject(obj) {
    if (_.has(obj, "name") &&
        _.has(obj, "lable") &&
        _.has(obj, "markup")) 
      return true;
    return false;
  }
}



PMP.Models.Site.prototype.DeleteSiteInputs = function (callback) {
  var based = this;
  var returnObject  = {
    UUID: based.userCtx.UUID,
    success: false,
    errorCode: 0,
    error: ""
  }

  if (based.isExisted != undefined && based.isExisted) {
    var sqlSite = "delete from sites_inputfields where site_id = ?";
    var trans = based.dbLink.startTransaction();
    trans.query(sqlSite,[based.SiteID],function (err,resultes, fields) {  
      if (err) {
        railway.logger.write(err);
        trans.rollback();
        callback(returnObject);
      }  
      returnObject.success =true;
      trans.commit();
      callback(returnObject);
    });
  }
  else {
    returnObject.errorCode = 501;
    returnObject.error = "site not existed";
    callback(returnObject);
  }
}

PMP.Models.Site.prototype.IsBindSiteToUser = function (callback) {
  var based = this;
  var returnObject  = {
    UUID: based.userCtx.UUID,
    success: false,
    errorCode: 0,
    error: ""
  }
  if (!based.Validator.hasErrors()) {
    if (based.isExisted != undefined && !based.isExisted) {
      var sql = "select s.site_id  \
                  from  usersites us \
                  left join sites s on s.id = us.site_id \
                  where us.site_id = ? and us.user_id = ? and s.isSupported = 1 and s.isBlocked = 0 and s.isArcived = 0 limit 1";
      returnObject.IsBind = false;
      based.dbLink.query(sql,[based.SiteID,based.userCtx.ID],function (err,resultes, fields) { 
        if (err) {
          railway.logger.write(err);
          callback(returnObject);
        }
        if (resultes.length != 0) {
          returnObject.IsBind = true;
          callback(returnObject);
        }
      });
    }
    else {
      returnObject.errorCode = 500;
      returnObject.error = "Its Uses when uuid or site id is unkowns";
      callback(returnObject);
    }
  }
  else {
    returnObject.errorCode = 500;
    returnObject.error = "Incorrect Params";
    callback(returnObject);
  }
}

PMP.Models.Site.prototype.BindSiteToUser = function (securtyId,callback) {
  var based = this;
  var returnObject  = {
    UUID: based.userCtx.UUID,
    success: false,
    errorCode: 0,
    error: ""
  };
  if (based.isExisted != undefined && based.isExisted) {
    var sqlSite = "insert into usersites (site_id, user_id, inputKey, securty_level_id) values (? , ? , ? , ?)";
    var trans = based.dbLink.startTransaction();
    trans.query(sqlSite,[based.SiteID,based.userCtx.ID,based.siteUUID,securtyId],function (err,resultes, fields) {  
      if (err) {
        railway.logger.write(err);
        trans.rollback();
        callback(returnObject);
      }  
      returnObject.success =true;
      trans.commit();
      callback(returnObject);
    });
  }
  else {
    returnObject.errorCode = 501;
    returnObject.error = "site not existed";
    callback(returnObject);
  }
}

PMP.Models.Site.prototype.UnBindSiteToUser = function (callback) {
  var based = this;
  var returnObject  = {
    UUID: based.userCtx.UUID,
    success: false,
    errorCode: 0,
    error: ""
  }
  if (based.isExisted != undefined && based.isExisted) {
    var sqlSite = "delete from usersites where site_id = ? and user_id = ?";
    var trans = based.dbLink.startTransaction();
    trans.query(sqlSite,[based.SiteID,based.userCtx.ID],function (err,resultes, fields) {  
      if (err) {
        railway.logger.write(err);
        trans.rollback();
        callback(returnObject);
      }  
      returnObject.success =true;
      trans.commit();
      callback(returnObject);
    });
  }
  else {
    returnObject.errorCode = 501;
    returnObject.error = "site not existed";
    callback(returnObject);
  }

}

PMP.Models.Site.prototype.AddSiteInputValue = function (arrInputValues,callback) {
  var based = this;
  var returnObject  = {
    UUID: based.userCtx.UUID,
    success: false,
    errorCode: 0,
    error: ""
  };
  //post data need be like this data=[{name:"", lable:"" , value:""}]
  if (based.isExisted != undefined && based.isExisted) {
    if (arrInputValues.length!= 0) {
      var sqlSite = "INSERT INTO usersites_input( input_id, input_key, value ) \
                      VALUES ( \
                        ( \
                          SELECT id \
                          FROM sites_inputfields \
                          WHERE site_id =  ? AND name = ? AND lable = ? \
                          LIMIT 1\
                        ),  ?, ?)";
      var trans = based.dbLink.startTransaction();
      _.each(arrInputValues,function (item,key) {
        if (_.isObject(item)&&_matchObject(item))  {
          trans.query(sqlSite,[based.SiteID,item.name,item.lable,item.value],function (err,resultes, fields) {  
            if (err) {
              railway.logger.write(err);
              trans.rollback();
              callback(returnObject);
            }  
            if (arrInputValues.length <= key){
              returnObject.success =true;
              trans.commit();
              callback(returnObject);
            }
          });
        }
      });
      trans.execute();
    }
    else {
      returnObject.errorCode = 501;
      returnObject.error = "site not existed";
      callback(returnObject);
    }
  }  
  else {
    returnObject.errorCode = 501;
    returnObject.error = "site not existed";
    callback(returnObject);
  }
  function _matchObject(obj) {
    if (_.has(obj, "name") &&
        _.has(obj, "lable") &&
        _.has(obj, "value")) 
      return true;
    return false;
  }
}
PMP.Models.Site.prototype.EditSiteInputValue = function (arrInputValues,callback) {
  var based = this;
  var returnObject  = {
    UUID: based.userCtx.UUID,
    success: false,
    errorCode: 0,
    error: ""
  }
  //post data need be like this data=[{name:"", lable:"" , value:""}]
  if (based.isExisted != undefined && based.isExisted) {
    if (arrInputs.length!= 0) {
      var sqlSite = "Delete from usersites_input \
                      where input_id in ( \
                          SELECT id \
                          FROM sites_inputfields \
                          WHERE site_id =  ? \
                        ) and input_key = ?";
      var trans = based.dbLink.startTransaction();
      trans.query(sqlSite,[based.SiteID,based.siteUUID],function (err,resultes, fields) {  
        if (err) {
          railway.logger.write(err);
          trans.rollback();
          callback(returnObject);
        }  
        based.AddSiteInputValue(arrInputValues,function (rasponse) {
          if (rasponse.success) {
            returnObject.success =true;
            trans.commit();
            callback(returnObject);
          }
          else {
            returnObject.errorCode = 502;
            returnObject.error = "Failer in removal of the old data";
            callback(returnObject);
          }
        });
      });
      trans.execute();
    }
    else {
      returnObject.errorCode = 501;
      returnObject.error = "site not existed";
      callback(returnObject);
    }
  }  
  else {
    returnObject.errorCode = 501;
    returnObject.error = "site not existed";
    callback(returnObject);
  }
  function _matchObject(obj) {
    if (_.has(obj, "name") &&
        _.has(obj, "lable") &&
        _.has(obj, "value")) 
      return true;
    return false;
  }
}

PMP.Models.Site.prototype.GetSiteInputValues = function (callback) {
  var based = this;
  var returnObject  = {
    UUID: based.userCtx.UUID,
    success: false,
    Values: {
      Data: [],
      Found: false
    },
    errorCode: 0,
    error: ""
  }
  if (based.isExisted != undefined && !based.isExisted) {
    var sql = "select sif.lable, sif.name , sif.markup ,  usv.value from usersites_input usv \
                      left join sites_inputfields sif usv.input_id = sif.id \
                    where  sif.site_id =  ? and usv.input_key = ?";
    based.dbLink.query(sql,[based.SiteID,based.siteUUID],function (err,resultes, fields) { 
      if (err) {
        railway.logger.write(err);
        callback(returnObject);
      }
      
      returnObject.success = true;
      if (resultes.length != 0) {
          _.each(resultes,function(item) {
              var obj = {
                  lable: item.lable,
                  name: item.name ,
                  markup: item.markup,
                  value: item.value
              };
              returnObject.Values.Data.push(obj);
          });
          returnObject.Values.Found = true;
          callback(returnObject);
      }
      else {
          returnObject.Values.Found = false;
          returnObject.errorCode = 504;
          returnObject.error = "Site Values Not Found";
          callback(returnObject);
      }
    });
  }
  else {
    returnObject.errorCode = 500;
    returnObject.error = "Its Uses when uuid or site id is unkowns";
    callback(returnObject);
  }
}

PMP.Models.Site.prototype.GetSiteByDomain = function (domain,secure,callback) {
  var based = this;
  var returnObject  = {
    UUID: based.userCtx.UUID,
    success: false,
    Site: {
      uuid: '',
      siteFound: false
    },
    errorCode: 0,
    error: ""
  }
  based.Validator = validator;
  based.Validator.check(domain).notEmpty();
  based.Validator.check(secure).notEmpty();
  if (!based.Validator.hasErrors()) {
    if (based.isExisted != undefined && !based.isExisted) {
      var sql = "select s.uuid  \
                  from  sites_domains sd \
                  left join sites s on s.id = sd.site_id \
                  where sd.domain = ? and sd.isSecure = ?  and s.isSupported = 1 and s.isBlocked = 0 and s.isArcived = 0 limit 1";
      secure = sanitize(secure).toBoolean();
      based.dbLink.query(sql,[domain,secure],function (err,resultes, fields) { 
        if (err) {
          railway.logger.write(err);
          callback(returnObject);
        }
        if (resultes.length != 0) {
          returnObject.Site.uuid = resultes[0].uuid;
          returnObject.Site.siteFound = true;
          callback(returnObject);
        }
      });
    }
    else {
      returnObject.errorCode = 500;
      returnObject.error = "Its Uses when uuid or site id is unkowns";
      callback(returnObject);
    }
  }
  else {
    returnObject.errorCode = 500;
    returnObject.error = "Incorrect Params";
    callback(returnObject);
  }
}


PMP.Models.Site.prototype.IsExistSiteDomain = function (domain,secure,callback) {
  var based = this;
  var returnObject  = {
    UUID: based.userCtx.UUID,
    success: false,
    errorCode: 0,
    error: ""
  }

  based.Validator = validator;
  based.Validator.check(domain).notEmpty();
  based.Validator.check(secure).notEmpty();
  if (!based.Validator.hasErrors()) {
    if (based.isExisted != undefined && based.isExisted) {
      secure = sanitize(secure).toBoolean();
      var sql = "select id from sites_domains where site_id = ? and domain = ? and isSecure = ? limit 1";

        based.dbLink.query(sql,[based.SiteID,domain,secure],function (err,resultes, fields) {    
          if (err) {
            railway.logger.write(err);
            callback(returnObject);
          }
          if(resultes.length != 0) returnObject.success = true;
          callback(returnObject);
          
        });
    }
    else {
      returnObject.errorCode = 501;
      returnObject.error = "site not existed";
      callback(returnObject);
    }
  }
  else {
    returnObject.errorCode = 500;
    returnObject.error = "Incorrect Params";
    callback(returnObject);
  }
}


PMP.Models.Site.prototype.SiteAddDomain = function (domain,secure,callback) {
  var based = this;
  var returnObject  = {
    UUID: based.userCtx.UUID,
    success: false,
    errorCode: 0,
    error: ""
  }

  based.Validator = validator;
  based.Validator.check(domain).notEmpty();
  based.Validator.check(secure).notEmpty();
  if (!based.Validator.hasErrors()) {
    if (based.isExisted != undefined && based.isExisted) {
      secure = sanitize(secure).toBoolean();
      var sqlSite = "INSERT INTO sites_domains( site_id, domain, isSecure ) VALUES ( ? ,  ? , ?)";
      secure = sanitize(secure).toBoolean();
      based.dbLink.query(sqlSite,[based.SiteID,domain,secure],function (err,resultes, fields) { 
        if (err) {
          railway.logger.write(err);
          callback(returnObject);
        }
        if (resultes.length != 0) returnObject.success = true;
        callback(returnObject);
      });
    }  
    else {
      returnObject.errorCode = 501;
      returnObject.error = "site not existed";
      callback(returnObject);
    }
  }
  else {
    returnObject.errorCode = 500;
    returnObject.error = "Incorrect Params";
    callback(returnObject);
  }
}

PMP.Models.Site.prototype.SiteRemoveDomain = function (domain,secure,callback) {
  var based = this;
  var returnObject  = {
    UUID: based.userCtx.UUID,
    success: false,
    errorCode: 0,
    error: ""
  }

  based.Validator = validator;
  based.Validator.check(domain).notEmpty();
  based.Validator.check(secure).notEmpty();
  if (!based.Validator.hasErrors()) {
    if (based.isExisted != undefined && based.isExisted) {
      secure = sanitize(secure).toBoolean();
      var sqlSite = "delete from sites_domains where site_id = ? and domain = ? and isSecure = ? limit 1";
      var trans = based.dbLink.startTransaction();
      trans.query(sqlSite,[based.SiteID,item.lable,item.value],function (err,resultes, fields) {  
        if (err) {
          railway.logger.write(err);
          trans.rollback();
          callback(returnObject);
        }  
        if (arrInputValues.length <= key){
          returnObject.success =true;
          trans.commit();
          callback(returnObject);
        }
      });
      trans.execute();
    }  
    else {
      returnObject.errorCode = 501;
      returnObject.error = "site not existed";
      callback(returnObject);
    }
  }
  else {
    returnObject.errorCode = 500;
    returnObject.error = "Incorrect Params";
    callback(returnObject);
  }
}

PMP.Models.Site.prototype.SiteDomainList = function (callback) {
  var based = this;
  var returnObject  = {
    UUID: based.userCtx.UUID,
    success: false,
    site_id: based.SiteID,
    Domains: [],
    errorCode: 0,
    error: ""
  }

  if (based.isExisted != undefined && based.isExisted) {
    var sql = "select domain, isSecure  , register_date from  sites_domains where site_id = ?";
    based.dbLink.query(sql,[based.SiteID],function (err,resultes, fields) {    
      if (err) {
        railway.logger.write(err);
        callback(returnObject);
      }
      if(resultes.length != 0) {
        returnObject.success = true;
        _.each (resultes , function (item){
          var moment = require("moment");
          var obj = {
            domain:  item.domain,
            isSecure: Boolean(item.isSecure),
            register_date: moment(register_date)
          };
          returnObject.Domains.push(obj);
        });
        callback(returnObject);
      }
      else {
        returnObject.success = true;
        callback(returnObject);
      }
    });
  }
  else {
    returnObject.errorCode = 501;
    returnObject.error = "site not existed";
    callback(returnObject);
  }

}

PMP.Models.Site.prototype.SiteList = function (options,callback) {
  var based = this;
  var returnObject  = {
    UUID: based.userCtx.UUID,
    success: false,
    Sites: [],
    errorCode: 0,
    error: ""
  }

  if (based.isExisted != undefined && based.isExisted) {
  secure = sanitize(secure).toBoolean();
    var sql = "select id,security_level_id, uuid  , name, description ,isSupported, isBlocked , isArcived ,register_date,update_date,last_scan_date \
                from  sites \
                where isSupported = ? and isBlocked = ? and isArcived = ? \
                order by namedesc \
                limit ? , ?";
    based.dbLink.query(sql,[options.isShowSupported,options.isShowBlocked,options.isShowArcived,options.start,options.limit],function (err,resultes, fields) {    
      if (err) {
        railway.logger.write(err);
        callback(returnObject);
      }
      if(resultes.length != 0) {
        returnObject.success = true;
        _.each (resultes , function (item){
          var moment = require("moment");
          var obj = {
            id:  item.id,
            security_level_id:  item.security_level_id,
            uuid:  item.uuid,
            name:  item.name,
            description:  item.description,
            isSupported: Boolean(item.isSupported),
            isBlocked: Boolean(item.isBlocked),
            isArcived: Boolean(item.isArcived),
            register_date: moment(item.register_date),
            update_date: moment(item.update_date),
            last_scan_date: moment(item.last_scan_date)
          };
          returnObject.Sites.push(obj);
        });
        callback(returnObject);
      }
      else {
        returnObject.success = true;
        callback(returnObject);
      }
    });
  }
  else {
    returnObject.errorCode = 501;
    returnObject.error = "site not existed";
    callback(returnObject);
  }
}

PMP.Models.Site.prototype.ClearArciveSite = function (callback) {
  var based = this;
  var returnObject  = {
    UUID: based.userCtx.UUID,
    success: false,
    errorCode: 0,
    error: ""
  }
  //post data need be like this data=[{name:"", lable:"" , value:""}]
  if (based.isExisted != undefined && based.isExisted) {
    var sqlSite = "Delete from sites where uuid = ? and isArcived = 1";
    var trans = based.dbLink.startTransaction();
    trans.query(sqlSite,[based.siteUUID],function (err,resultes, fields) {  
      if (err) {
        railway.logger.write(err);
        trans.rollback();
        callback(returnObject);
      }  
      returnObject.success =true;
      trans.commit();
      callback(returnObject);
    });
    trans.execute();
  }
  else {
    returnObject.errorCode = 501;
    returnObject.error = "site not existed";
    callback(returnObject);
  }
}

PMP.Models.Site.prototype.IsArciveSite = function (callback) {
  var based = this;
  var returnObject  = {
    UUID: based.userCtx.UUID,
    success: false,
    errorCode: 0,
    error: ""
  }

  if (based.isExisted != undefined && based.isExisted) {
      returnObject.success = based.Context.isArcived;
      callback(returnObject);
  }
  else {
    returnObject.errorCode = 501;
    returnObject.error = "site not existed";
    callback(returnObject);
  }
}

PMP.Models.Site.prototype.ScanSites = function (callback) {
  var based = this;
  var returnObject  = {
    UUID: based.userCtx.UUID,
    Scans: [],
    success: false,
    errorCode: 0,
    error: ""
  }
  if (based.isExisted != undefined && based.isExisted) {
    var dateDiffSetting =  parseInt(PMP.Settings.expried_scan_key,10);
    var sqlSite = "select s.uuid,sd.`domain` , sd.isSecure, s.last_scan_date from sites_domains sd \
                    left join sites s on s.id = s.site_id \
                    where DATEDIFF(s.last_scan_date,NOW()) <= ? and s.isArcived = 0 and s.isBlocked = 0 and s.isSupported = 1";
    based.dbLink.query(sqlSite,[dateDiffSetting],function (err,resultes, fields) {    
      if (err) {
        railway.logger.write(err);
        callback(returnObject);
      }
      if(resultes.length != 0) {
        var moment = require("moment");
        returnObject.success = true;
        _.each(resultes,function (item) {
          var obj = {
            uuid: item.uuid,
            domain:  item.domain,
            isSecure: Boolean(item.isSecure),
            lastScan: moment(item.last_scan_date)
          };
          returnObject.Scans.push(obj);
        });
        callback(returnObject);
      }
    });
  }
  else {
    returnObject.errorCode = 501;
    returnObject.error = "site not existed";
    callback(returnObject);
  }
}

PMP.Models.Site.prototype.MarkScanSite = function (callback) {
  var based = this;
  var returnObject  = {
    UUID: based.userCtx.UUID,
    success: false,
    errorCode: 0,
    error: ""
  }
  if (based.isExisted != undefined && based.isExisted) {
    var sqlupdateSite = "update sites set last_scan_date = NOW() , update_date = NOW() where UUID = ?;";
    var trans = based.dbLink.startTransaction();
    trans.query(sqlupdateSite,[based.siteUUID],function (err,resultes, fields) {    
      if (err) {
        railway.logger.write(err);
        trans.rollback();
        callback(returnObject); 
      }
      returnObject.success =true;
      returnObject.SiteUUID =based.siteUUID; 
      trans.commit();
      callback(returnObject);
    });
    trans.execute();
  }  
  else {
    returnObject.errorCode = 501;
    returnObject.error = "site not existed";
    callback(returnObject);
  } 
}

PMP.Models.Site.prototype.IsExistSite = function (siteUUID,callback) {
  var based = this;
  var returnObject  = { 
    UUID: based.userCtx.UUID,
    Site: {
      UUID: '',
      Name: "",
      Desc: "", 
      IsBlocked: '',
      IsSupport: '',
      Register_date: '',
      Update_date: '',
      LastScan: ""
    },
    success: false,
    errorCode: 0,
    error: ""
  };
  if (based.GUID.isGuid(siteUUID)) {
    var sqlSite = "select id,`uuid`, `name`, `description`, `isSupported`, `isBlocked`, `isArcived`, `register_date`, `update_date`, `last_scan_date` from sites where uuid = ?  limit 1";
    based.dbLink.query(sqlSite,[siteUUID],function (err,resultes, fields) {    
      if (err) {
        railway.logger.write(err);
        callback(returnObject);
      }
      if(resultes.length != 0) {
        var moment = require("moment");
        returnObject.success = true;
        based.SiteID = resultes[0].id;
        returnObject.Site.UUID = siteUUID;
        based.Name = resultes[0].name;
        based.Description = resultes[0].description;
        based.IsBlocked = Boolean(resultes[0].isBlocked);
        based.IsSupported = Boolean(resultes[0].isSupported);
        based.isArcived = Boolean(resultes[0].isArcived);
        based.Register_date = moment(resultes[0].register_date);
        based.Update_date = moment(resultes[0].update_date);
        based.LastScan = moment(resultes[0].last_scan_date);
        callback(returnObject);
      }
      else {
        callback(returnObject);
      }
    });
  }
}
