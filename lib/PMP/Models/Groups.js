/**
 * Module object of the group entity
 * @param GUID   uuid           the GUID Id of the currect token of the user
 * @param USERObject   userCtx        user object module
 * @param function callback       call back to the controller
 * @param function  failercallback call back to the controller on failer
 * @param Boolean  isRegister     true - only when is is on register else false
 */
PMP.Models.Groups = function (uuid,userCtx,callback,failercallback,isRegister) { 
  	var based = this;
  	based.UUID = uuid;
  	based.GroupID = -1;
  	based.isRegister = isRegister;
  	based.Base58 = require('base58');
	based.dbLink = app.ApplicationSharedObjects.Connection;
	if (based.isRegister) {
		callback(based);
	}
	else {
		if (based.userCtx.IsLoginsuccess&&!based.isRegister) {
			based.UUID = based.userCtx.UUID;
			callback(based);
		}
		else {
			failercallback();
		}
	}
};
/**
 * Add Group
 * @param string   name        name of the group
 * @param string   description desc. of the group
 * @param string   email       email of the group 
 * @param function callback       call back to the controller
 */
PMP.Models.Groups.prototype.AddGroup =  function (name,description,email,callback) {
	var based = this;
	var returnObject  = {
		UUID: based.UUID,
		success: false,
		errorCode: 0,
		error: ""
	}
	based.Validator = validator;
	based.Validator.check(email,"Email Needed").notEmpty().len(6, 255).isEmail();
	based.Validator.check(name,"Group Name Needed").notEmpty().len(4, 45);
	var errors = based.Validator.getErrors();
	if (based.Validator.hasErrors()) {
		returnObject.errorCode =101;
		returnObject.error= "errors";
	  	callback(returnObject);
	}
	else {
		based.IsGroupExistedByName(groupid,function (res){
			if (!res.success) {
				var sqlInsertGroup = "insert into groups (gorup_profile_id ,name,  description, isActive) values (?,?,?,1);";
				var sqlInsertGroupProfile = "insert into group_profile (email , logo) values (?,'');";
				var trans = based.dbLink.startTransaction();
		  		trans.query(sqlInsertGroupProfile,[email],function (err,resultes, fields) {    
		  			if (err) {
		  				railway.logger.write(err);
		  				returnObject.error = "Data Not Found";
		  				returnObject.errorCode = 500;
		  				trans.rollback();
		  				callback(returnObject);
		  			}
		  			var gpid = resultes.insertId;
		  			trans.query(sqlInsertGroup,[gpid,name,description],function (err,resultes, fields) {   
			  			if (err) {
			  				railway.logger.write(err);
		  				returnObject.error = "Data Not Found";
		  				returnObject.errorCode = 500;
			  				trans.rollback();
			  				callback(returnObject);
			  			}
  						var based58 = require('base58');
			  			returnObject.GroupID = based58.encode(gpid);
						returnObject.success =true;
			  			trans.commit();
			  			callback(returnObject);
		  			});
		  		});
		  		trans.execute();
			}
			else {
				returnObject.errorCode = 500;
				returnObject.error= "Group Exist";
		  		callback(returnObject);
			}
		});
	}
}
/**
 * Delete the group
 * @param B58 int   groupid           group id that is under encode of base58
 * @param B58 int   moveUserToGroupid Not in used
 * @param function callback       call back to the controller
 */
PMP.Models.Groups.prototype.DeleteGroup =  function (groupid,moveUserToGroupid,callback) {
	var based = this;
	var returnObject  = {
		UUID: based.UUID,
		success: false,
		errorCode: 0,
		error: ""
	}
  	groupid= based.Base58.decode(groupid);
	based.IsGroupExisted(groupid,function (res){
		if (res.success) {
			based.MoveGroupUsers(groupid,moveUserToGroupid,-1,function (response) {
				var sql = "delete from groups g ,group_profile gp where g.id=? and g.group_profile_id = gp.id";
				var trans = based.dbLink.startTransaction();
		  		trans.query(sqlInsertGroupProfile,[email],function (err,resultes, fields) { 
		  			if (err) {
		  				railway.logger.write(err);
		  				returnObject.error = "Data Not Found";
		  				returnObject.errorCode = 500;
		  				trans.rollback();
		  				callback(returnObject);
		  			}
					returnObject.success =true;
		  			trans.commit();
		  			callback(returnObject);
		  		});
		  		trans.execute();
			});
		}
		else {
			returnObject.errorCode = 500;
			returnObject.error= "Group Not Exist";
	  		callback(returnObject);
		}
	})
}
/**
 * Get all the groups on the system
 * @param function callback       call back to the controller
 */
PMP.Models.Groups.prototype.GetGroups = function (callback) {

	var based = this;
	var returnObject  = {
		success: false,
		Groups: [],
		errorCode: 0,
		error: ""
	}
	var sql = "select id,name,  description, isActive from groups g\
				left join group_profile gp on gp.id = gp.group_profile_id";
	based.dbLink.query(sqlInsertGroupProfile,[email],function (err,resultes, fields) { 
		if (err) {
			railway.logger.write(err);
			returnObject.error = "Data Not Found";
			returnObject.errorCode = 500;
			callback(returnObject);
			return;
		}
		_.each(resultes,function (groupItem) {
			try {
				var obj = {
					ID: based.Base58.encode(groupItem.id),
					Name: groupItem.name,
					Description: groupItem.description,
					Email: "",
					isActive: Boolean(groupItem.isActive)
				};
				returnObject.Groups.push(obj);
			}
			catch(err) {
        		railway.logger.write(err);
			}
		});
		returnObject.success =true;
		callback(returnObject);
	});
}
/**
 * Get the profile of the group
 * @param B58 int   groupid           group id that is under encode of base58
 * @param function callback       call back to the controller
 */
PMP.Models.Groups.prototype.GetGroupProfile =  function (groupid,callback) {
	var based = this;
	var returnObject  = {
		UUID: based.UUID,
		success: false,
		Profile: {
			logo: "",
			email: ""
		},
		errorCode: 0,
		error: ""
	}
  	groupid= based.Base58.decode(groupid);
	based.IsGroupExisted(groupid,function (res){
		if (res.success) {
			var sql = "select gp.logo, gp.email from group_profile gp \
						left join groups g on g.group_profile_id = gp.id \
						where g.id = ? limit 1";
		  		based.dbLink.query(sqlInsertGroupProfile,[email],function (err,resultes, fields) { 
		  			if (err) {
		  				railway.logger.write(err);
		  				returnObject.error = "Data Not Found";
		  				returnObject.errorCode = 500;
		  				callback(returnObject);
		  				return;
		  			}
					returnObject.success =true;
					returnObject.Profile.logo = resultes[0].logo;
					returnObject.Profile.email = resultes[0].email;
		  			callback(returnObject);
		  		});
		}
		else {
			returnObject.errorCode = 500;
			returnObject.error= "Group Not Exist";
	  		callback(returnObject);
		}
	})
}
/**
 * Enable of disable the active flag of the group
 * @param B58 int   groupid           group id that is under encode of base58
 * @param function callback       call back to the controller
 */
PMP.Models.Groups.prototype.ToggelsuccessGroup =  function (groupid,callback) {
	var based = this;
	var returnObject  = {
		UUID: based.UUID,
		success: false,
		errorCode: 0,
		error: ""
	}
  	groupid= based.Base58.decode(groupid);
	based.IsGroupExisted(groupid,function (res){
		if (res.success) {
			var sqlGroup = "update groups set isActive = ? where id = ?";
			var success = (based.IsActive)?false:true;
			var trans = based.dbLink.startTransaction();
	  		trans.query(sqlGroup,[success,groupid],function (err,resultes, fields) { 
	  			if (err) {
	  				railway.logger.write(err);
	  				returnObject.error = "Data Not Found";
	  				returnObject.errorCode = 500;
	  				trans.rollback();
	  				callback(returnObject);
	  				return;
	  			}
				returnObject.success =true;
	  			trans.commit();
	  			callback(returnObject);
	  		});
	  		trans.execute();
		}
		else {
			returnObject.errorCode = 500;
			returnObject.error= "Group Not Exist";
	  		callback(returnObject);
		}
	})
}
/**
 * check if group is exist or by id or by id
 * @param int/string   groupIdOrName  by id or name
 * @param function callback       call back to the controller
 */
PMP.Models.Groups.prototype.IsGroupExisted =  function (groupIdOrName,callback) {
	var based = this;
	var returnObject  = {
		UUID: based.UUID,
		success: false,
		isExisted: false,
		errorCode: 0,
		error: ""
	}
	var groupId = -1;
	var groupName = "";
	if (groupIdOrName == null|| groupIdOrName == "")  {
		returnObject.errorCode = 101;
		returnObject.error = "Error in Arguments";
		callback(returnObject);
	}
	else {
		var isName = false;
		try { groupId = parseInt(groupIdOrName,10); } 
		catch(err) {
			isName = true;
			groupName = groupIdOrName;
		}
		var sqlExistedGroup = (groupId!= -1)? "select id,name,description,isActive from groups where id = ?": "select id,name,description,isActive from groups where name = ?";
		if (!isName) {
  			groupid= based.Base58.decode(groupid);
	  		based.dbLink.query(sqlExistedGroup,[groupid],function (err,resultes, fields) {    
	  			if (err) {
	  				railway.logger.write(err);
	  				returnObject.error = "Data Not Found";
	  				returnObject.errorCode = 500;
	  				callback(returnObject);
	  			}

	  			if(resultes.length != 0) {
	  				returnObject.success = (based.isRegister)?false:true;
	  				returnObject.isExisted =true;
	  				based.GroupID = resultes[0].id;
	  				based.Name = resultes[0].name;
	  				based.Description = resultes[0].description;
	  				based.IsActive = Boolean(resultes[0].isActive);
	  				callback(returnObject);
	  			}
	  			else {
					returnObject.errorCode = 404;
					returnObject.error = "Group Not Found";
					callback(returnObject);
	  			}
	  		});
		}
		else {
	  		based.dbLink.query(sqlExistedGroup,[groupName],function (err,resultes, fields) {    
	  			if (err) {
	  				railway.logger.write(err);
	  				callback(returnObject);
	  			}
	  			if(resultes.length != 0) {
	  				returnObject.success = (based.isRegister)?false:true;
	  				returnObject.isExisted =true;
	  				based.GroupID = resultes[0].id;
	  				callback(returnObject);
	  			}
	  			else {
					returnObject.errorCode = 404;
					returnObject.error = "Group Not Found";
					callback(returnObject);
	  			}
	  		});
		}
	}
}
/**
 * Move Users From Group A to Group B
 * @param B58 int   groupid           group id that is under encode of base58
 * @param B58 int   togroupid          to group id that is under encode of base58
 * @param array   usersArr  array of users id under encode of base58
 * @param function callback       call back to the controller
 */
PMP.Models.Groups.prototype.MoveGroupUsers =  function (groupid,togroupid,usersArr,callback) {
	var based = this;
	var returnObject  = {
		UUID: based.UUID,
		success: false,
		errorCode: 0,
		error: ""
	}
	based.Validator = Validator;
  	groupid= based.Base58.decode(groupid);
	based.IsGroupExisted(groupid,function (res){
		if (res.success) {
  			togroupid= based.Base58.decode(togroupid);
			based.IsGroupExisted(togroupid,function (res){
				if (res.success) {
					if (usersArr == -1) {//move all users
						var sqlMoveUsers = "update premmisionmap2user set groupid=? where groupid=?";
						var trans = based.dbLink.startTransaction();
						trans.query(sqlMoveUsers,[togroupid,groupid],function (err,resultes, fields) { 
							if (err) {
								railway.logger.write(err);
				  				returnObject.error = "Data Not Found";
				  				returnObject.errorCode = 500;
								trans.rollback();
								callback(returnObject);
								return;
							}
							returnObject.success =true;
				  			trans.commit();
				  			callback(returnObject);
				  		});
				  		trans.execute();
					}
					else {//move user based array
						if (_.isArray(usersArr)) {
							var userIds = "";
							_.each(usersArr,function (userId){
								based.Validator = validator;
								based.Validator.check(userId).isInt();
								if (!based.Validator.hasErrors()) {
  									var based58 = require('base58');
									var userId = sanitize(based58.decode(userId)).toInt();
									if (userIds == "") {
										userIds += userId;
									}
									else {
										userIds += ","+userId;
									}
								}
							});
							var sqlMoveUsers = "update premmisionmap2user set groupid=? where groupid=? and userid in ("+userIds+")";
							var trans1 = based.dbLink.startTransaction();
							trans1.query(sqlMoveUsers,[togroupid,groupid],function (err,resultes, fields) { 
								if (err) {
									railway.logger.write(err);
					  				returnObject.error = "Data Not Found";
					  				returnObject.errorCode = 500;
					  				trans1.rollback();
					  				trans.rollback();
					  				callback(returnObject);
					  				return;
					  			}
								returnObject.success =true;
					  			trans1.commit();
					  			trans.commit();
					  			callback(returnObject);
					  		});
					  		trans1.execute();
						}
					}
				}
				else {
					returnObject.errorCode = 500;
					returnObject.error= "Group Not Exist";
			  		callback(returnObject);
				}
			})
		}
		else {
			returnObject.errorCode = 500;
			returnObject.error= "Group Not Exist";
	  		callback(returnObject);
		}
	})
}
/**
 * [IsGroupPartOfRole description
 * @param B58 int   groupid           group id that is under encode of base58
 * @param B58 int   roleid           role id that is under encode of base58
 * @param function callback       call back to the controller
 */
PMP.Models.Groups.prototype.IsGroupPartOfRole =  function (groupid,roleid,callback) {
	var based = this;
	var returnObject  = {
		UUID: based.UUID,
		success: false,
		errorCode: 0,
		error: ""
	}
  	groupid= based.Base58.decode(groupid);
	based.IsGroupExisted(groupid,function (res){
		if (res.success) {
			var sqlGroup = "select id from roles2groups where group_id = ? and role_id = ?";
  			roleid= based.Base58.decode(roleid);
			based.dbLink.query(sqlGroup,[groupid,roleid],function (err,resultes, fields) {    
	  			if (err) {
	  				railway.logger.write(err);
	  				returnObject.error = "Data Not Found";
	  				returnObject.errorCode = 500;
	  				callback(returnObject);
	  			}

	  			if(resultes.length != 0) {
	  				returnObject.success = true;
	  				callback(returnObject);
	  			}
	  		});
		}
		else {
			returnObject.errorCode = 500;
			returnObject.error= "Group Not Exist";
	  		callback(returnObject);
		}
	})
}
/**
 * will bind user to a group
 * @param int userid  user id
 * @param int groupid group id
 */
PMP.Models.Groups.prototype.BindUserToGroup = function (userid,groupid) {
	var based = this;
	var returnObject  = {
		UUID: based.UUID,
		success: false,
		errorCode: 0,
		error: ""
	}
	based.IsGroupExisted(groupid,function (res){
		if (res.success) {

			var sqlUpdatePrem = "update  premmisionmap2user  set groupid = ? where userid = ?";
			var sqlchackGroupPremmision = "SELECT pu.userid,pu.groupid \
			                        from premmisionmap2user pu \
			                        left join user u on u.id = pu.userid \
			                        where pu.groupid = ? and u.id = ? limit 1"
			based.dbLink.query(sqlGroup,[groupid,userid],function (err,resultes, fields) {  
				if (err) {
					railway.logger.write(err);
					returnObject.error = "Data Not Found";
					returnObject.errorCode = 500;
					callback(returnObject);
				}
				else {
					if (resultes.length == 0) {
						var trans = based.dbLink.startTransaction();
						trans.query(sqlUpdatePrem,[groupid,userid],function (err,resultes, fields) { 
							if (err) {
								railway.logger.write(err);
								returnObject.error = "Data Not Found";
								returnObject.errorCode = 500;
								callback(returnObject);
							}
							else {
								returnObject.success =true;
					  			trans.commit();
					  			callback(returnObject);
					  		}
				  		});
				  		trans.execute();
				  	}
				}
			});
		}
		else {
			returnObject.errorCode = 500;
			returnObject.error= "Group Not Exist";
	  		callback(returnObject);
		}
	})
}
/**
 * will unbind user to a group
 * @param int userid  user id
 * @param int groupid group id
 */
PMP.Models.Groups.prototype.UnBindUserToGroup = function (userid,groupid) {
	var based = this;
	var returnObject  = {
		UUID: based.UUID,
		success: false,
		errorCode: 0,
		error: ""
	}
	based.IsGroupExisted(groupid,function (res){
		if (res.success) {

			var sqlUpdatePrem = "update  premmisionmap2user  set groupid = null where userid = ?";
			var sqlchackGroupPremmision = "SELECT pu.userid,pu.groupid \
			                        from premmisionmap2user pu \
			                        left join user u on u.id = pu.userid \
			                        where pu.groupid = ? and u.id = ? limit 1"
			based.dbLink.query(sqlGroup,[groupid,userid],function (err,resultes, fields) {  
				if (err||resultes.length == 0) {
					railway.logger.write(err);
					returnObject.error = "Data Not Found";
					returnObject.errorCode = 500;
					callback(returnObject);
				}
				else {
					var trans = based.dbLink.startTransaction();
					trans.query(sqlUpdatePrem,[groupid,userid],function (err,resultes, fields) { 
						if (err) {
							railway.logger.write(err);
							returnObject.error = "Data Not Found";
							returnObject.errorCode = 500;
							callback(returnObject);
						}
						else {
							returnObject.success =true;
				  			trans.commit();
				  			callback(returnObject);
				  		}
			  		});
			  		trans.execute();
		
				}
			});
		}
		else {
			returnObject.errorCode = 500;
			returnObject.error= "Group Not Exist";
	  		callback(returnObject);
		}
	})
}