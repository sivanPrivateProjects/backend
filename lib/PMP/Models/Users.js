PMP.Models.Users = function (isForcedLogin,uuid,callback,failercallback) {
  var based = this;
	based.GUID = require('guid');	
	based.dbLink = app.ApplicationSharedObjects.Connection;
	based.Context = null;
  based.IsLoginsuccess = false;
  based.Hashing = require('jshashes');
  based.Base58 = require('base58');
  if (based.GUID.isGuid(uuid)) {
      based.UUID = uuid;
      based.IsLogin(uuid,function (response) {
        if (response.success) {
          based._getContext(response.id,function (response) {
            based.Context = response;
            callback(based);
          });
        }
        else {
          if (isForcedLogin) {
            failercallback();
          }
          else {
            callback(based);
          }
        }
    });
	}
  else {
    if (uuid == null) {
      callback(based);
    }
    else 
      failercallback();
  }
}


/****
***** Public METHODS
*****/
/**
 * Get user profile
 * @param int   user_id  user id
 * @param function callback       call back to the controller
 */
PMP.Models.Users.prototype.GetProfile = function (user_id,callback) {
	var based = this;
  var returnObject  = {
    UUID: based.UUID,
		id: -1,
		country: "",
		fname: "",
		lname: "",
		gender: "",
		address: "",
		city: "",
		birth_date: "",
		title: "",
    success: false,
    errorCode: 0,
    error: ""
	}
	var sqluserprofile = "SELECT u.`id`, c.`short_name` country ,  up.`fname`,  up.`lname`,  up.`gender`,  up.`address`, up. `city`,  up.`birth_date`,  up.`title` \
              						FROM `users_profile` up \
              						left join users u on u.user_profile_id= up.id \
              						left join country c on c.id = up.country_id \
              						where u.id = ? limit 1"; 
	based.dbLink.query(sqlSession,[user_id],function (err,resultes, fields) {
      if (err||resultes.length == 0) {
        railway.logger.write(err);
        returnObject.error  = "User Not Found";
        returnObject.errorCode = 404;
        callback(returnObject);
      }
      else {
      	returnObject.id = based.Base58.encode(intval(resultes[0].id));
      	returnObject.country = resultes[0].country;
      	returnObject.fname = resultes[0].fname;
      	returnObject.lname = resultes[0].lname;
      	returnObject.gender = resultes[0].gender;
      	returnObject.address = resultes[0].address;
      	returnObject.city = resultes[0].city;
      	returnObject.birth_date = new Date(resultes[0].birth_date);
      	returnObject.title = resultes[0].title;
        returnObject.success = true;
        callback(returnObject);
      }
    });
}
/**
 * Chack if session exist and login
 * @param GUID   uuid     UUID of the user
 * @param function callback       call back to the controller
 */
PMP.Models.Users.prototype.IsLogin = function (uuid,callback) {
	var based = this;
  var returnObject  = {
    success: false,
    errorCode: 0,
    error: ""
  }
  var sqlSession = "select ul.user_id,ul.expire_date from userlogin ul left join users u on u.id= ul.user_id where ul.uuid = ? and u.isActive = 1 limit 1"; 
  var sqlUpdateSession = "update userlogin set uuid = ?, expire_date = current_timestamp() where user_id = ?"; 
	based.dbLink.query(sqlSession,[uuid],function (err,resultes, fields) {
      if (err||resultes.length == 0) {
        railway.logger.write(err);
        returnObject.error  = "User Not Found";
        returnObject.errorCode = 404;
        callback(returnObject);
      }
      else {
        var moment = require("moment");
        var currentDate = moment();
        var lastKeyDate =moment(resultes[0].expire_date);
        if (currentDate.diff(lastKeyDate,'minutes') >= parseInt(PMP.Settings.expried_session_key,10))  {
          var uuidLoginHash = based.GUID.create(1);
          returnObject.id = resultes[0].user_id;
          returnObject.success = true;
          based.IsLoginsuccess = true;
          based.ID = resultes[0].user_id;
          based.dbLink.query(sqlUpdateSession,[uuidLoginHash,resultes[0].user_id],function (err,resultes, fields) {
            if (err) {
              railway.logger.write(err);
            }
          });
          based.UUID = uuidLoginHash;
          returnObject.uuid = uuidLoginHash;
          callback(returnObject);
        }
        else {
        	returnObject.success = true;
          returnObject.uuid = uuid;
          returnObject.id = resultes[0].user_id;
          callback(returnObject);
        }
      }
  });
}
/**
 * Logout session user
 * @param GUID   uuid     UUID of the user
 * @param function callback       call back to the controller
 */
PMP.Models.Users.prototype.Logout = function (uuid,callback) {
	var based = this;
	var returnObject  = {
    success: false,
    errorCode: 0,
    error: ""
	}
	var sqlSession = "select ul.user_id from userlogin ul left join users u on u.id= ul.user_id where ul.uuid = ? and u.isActive = 1 limit 1"; 
	based.dbLink.query(sqlSession,[uuid],function (err,resultes, fields) {
        if (err||resultes.length == 0) {
          railway.logger.write(err);
          returnObject.error  = "Data currapted";
          returnObject.errorCode = 500;
          callback(returnObject);
        }
        else {
          var sqlSessionDelete = "Delete from userlogin where uuid = ? limit 1"; 
          based.dbLink.query(sqlSessionDelete,[uuid],function (err,resultes, fields) {
            returnObject.success =true;
            callback(returnObject);
          });
        }
  });
}

/**
 * Regist session and create the login pros.
 * @param string   user            user name
 * @param string   pass            user pass
 * @param string   ip              user ip
 * @param string   localPrivateKey current frontend local key
 * @param function callback       call back to the controller
 */
PMP.Models.Users.prototype.RegisterSession = function (user,pass,ip,localPrivateKey,callback) { 
    var based = this;
    var uuidLoginHash = based.GUID.create(1);
    var returnObject = {
        success: true,
        UUID: '',
        errorCode: 4303,
        error: "User or Password are incorrect!"
    };
    var sql = "Select id from users where email = ? and password = ? and isActive = 1 limit 1";
    var sqlSession = "select user_id from userlogin where user_id = ? limit 1"; 
    var sqlInsertSession = "insert into userlogin (user_id,uuid) value (? , ?);update users set last_login_date = now() ,  last_ip = ? where id = ?;";
    var SHA512 = new based.Hashing.SHA512;
    var b64 = new based.Hashing.Base64;
    var encodePass = b64.encode(SHA512.hex_hmac(pass,localPrivateKey));
    based.dbLink.query(sql,[user,encodePass] , function (err,resultes, fields) {
        if (err) {
          railway.logger.write(err);
          returnObject.error  = "Data currapted";
          returnObject.errorCode = 500;
          callback(returnObject);
        }
        else {
          if (resultes.length == 0) 
              callback(showError());
          else {
              var row = resultes[0];
              based.dbLink.query(sqlSession,[row.id],function (err,resultes, fields) {
                  if (err) {
                      callback(showError(err));
                      return;
                  }
                  if (resultes.length != 0) {
                      callback(showError("User Logged Can't relogin"));
                      return;
                  }
                  else {
                      based.dbLink.query(sqlInsertSession,[row.id,uuidLoginHash,ip,row.id]);
                      returnObject.error =  "";
                      returnObject.errorCode = 0
                      based.Id = based.Base58.encode(row.id);
                      returnObject.UUID = uuidLoginHash;
                      returnObject.success = true;
                      callback(returnObject);
                  }
              });
          }
        }
    });
    function showError(errorMessage) {
        if (errorMessage != undefined ||errorMessage != "") {
            returnObject.error = errorMessage;
        }
        returnObject.success = false;
        return returnObject;
    }
}

PMP.Models.Users.prototype.GetUserGroups = function (user_id,groupid,roleid,callback) {
  var based = this;
  var returnObject  = {
    UUID: based.UUID,
    Groups: [],
    success: false,
    errorCode: 0,
    error: ""
  };
  var sqlchackGroupPremmision = "SELECT pu.userid,pu.groupid \
                            from premmisionmap2user pu \
                            left join userlogin ul on ul.user_id = pu.userid \
                            where pu.groupid = ? and ul.user_id = ? ";

  based.dbLink.query(sqlchackGroupPremmision,[groupid,uuid],function (err,resultes, fields) {
    if (err) {
      railway.logger.write(err);
      returnObject.error  = "Data currapted";
      returnObject.errorCode = 500;
      callback(returnObject);
    }
    if (resultes.length == 0) callback(returnObject);
    else {
      _.each(resultes,function (item) {
        returnObject.Groups.push(based.Base58.encode(item.groupid));
      });
      returnObject.success = true;
      callback(returnObject)
    }
  }); 
};
/**
 * Get the user premmision table and what he have access to and what roles
 * @param int   user_id  user id
 * @param string   groupid  base 58 group id
 * @param string   roleid   base 58 role id
 * @param function callback       call back to the controller
 */
PMP.Models.Users.prototype.HasUserHavePremmision = function (groupid,roleid,callback) {
  var based = this;
  var returnObject  = {
    UUID: based.UUID,
    success: false,
    matchedGroup: false,
    matchedRole: false,
    errorCode: 0,
    error: ""
  };
  groupid= based.Base58.decode(groupid);
  roleid= based.Base58.decode(roleid);
  var sqlchackGroupPremmision = "SELECT pu.userid,pu.groupid \
                            from premmisionmap2user pu \
                            left join userlogin ul on ul.user_id = pu.userid and pu.isActive = 1 \
                            where pu.groupid = ? and ul.uuid = ? limit 1";
  var sqlchackRolePremmision = "SELECT pu.userid,pu.roleid \
                            from premmisionmap2user pu \
                            left join userlogin ul on ul.user_id = pu.userid and pu.isActive = 1 \
                            where pu.roleid = ? and ul.uuid = ? limit 1";
  based.dbLink.query(sqlchackGroupPremmision,[groupid,based.UUID],function (err,resultes, fields) {
      if (err) {
        railway.logger.write(err);
        returnObject.error  = "Data currapted";
        returnObject.errorCode = 500;
        callback(returnObject);
      }
      if (resultes.length == 1) {
        returnObject.matchedGroup = true;
      }
      based.dbLink.query(sqlchackRolePremmision,[roleid,based.UUID],function (err,resultes, fields) {
        if (err) {
          railway.logger.write(err);
          returnObject.error  = "Data currapted";
          returnObject.errorCode = 500;
          callback(returnObject);
        }
        if (resultes.length == 1) {
          returnObject.matchedRole = true;
        }
        returnObject.success = true;
        callback(returnObject);
      });
  })
}
/**
 * Get User Data
 * @param int   user_id  user id
 * @param function callback       call back to the controller
 */
PMP.Models.Users.prototype.GetUser = function (user_id,callback) {
  var based = this;
  var returnObject  = {
    id: -1,
    email: "",
    register_date: "",
    update_date: "",
    last_login_date: "",
    last_ip: "",
    isActive: "",
    success: false,
    errorCode: 0,
    error: ""
  }
  var sqlSession = "SELECT u.`id`,  u.`email`,  u.`register_date`, u.`update_date`, u.`last_login_date`, u.`last_ip`, u.`isActive` FROM `users` u  WHERE u.id = ? limit 1"; 
  based.dbLink.query(sqlSession,[id],function (err,resultes, fields) {
    if (err) {
      railway.logger.write(err);
      returnObject.error  = "Data currapted";
      returnObject.errorCode = 500;
      callback(returnObject);
    }
    if (resultes.length == 0) callback(returnObject);
    else {
      returnObject.success = true;
      returnObject.id = resultes[0].id;
      returnObject.email = resultes[0].email;
      returnObject.register_date = new Date(resultes[0].register_date);
      returnObject.update_date = new Date(resultes[0].update_date);
      returnObject.last_login_date =  new Date(resultes[0].id);
      returnObject.last_ip = resultes[0].last_ip;
      returnObject.isActive = Boolean(resultes[0].isActive);
      callback(returnObject);
    }
  });
}
/**
 * Get User data by his user name
 * @param string   username user name
 * @param function callback       call back to the controller
 */
PMP.Models.Users.prototype.GetUserByUsername = function (username,callback) {
  var based = this;
  var returnObject  = {
    id: -1,
    email: "",
    register_date: "",
    update_date: "",
    last_login_date: "",
    last_ip: "",
    isActive: "",
    success: false,
    errorCode: 0,
    error: ""
  }
  var sqlSession = "SELECT u.`id`,  u.`email`,  u.`register_date`, u.`update_date`, u.`last_login_date`, u.`last_ip`, u.`isActive` FROM `users` u  WHERE u.email = ? limit 1"; 
  based.dbLink.query(sqlSession,[username],function (err,resultes, fields) {
    if (err) {
      railway.logger.write(err);
      returnObject.error  = "Data currapted";
      returnObject.errorCode = 500;
      callback(returnObject);
    }
    if (resultes.length == 0) callback(returnObject);
    else {
      returnObject.success = true;
      returnObject.id = resultes[0].id;
      returnObject.email = resultes[0].email;
      returnObject.register_date = new Date(resultes[0].register_date);
      returnObject.update_date = new Date(resultes[0].update_date);
      returnObject.last_login_date =  new Date(resultes[0].id);
      returnObject.last_ip = resultes[0].last_ip;
      returnObject.isActive = Boolean(resultes[0].isActive);
      callback(returnObject);
    }
  });
}

PMP.Models.Users.prototype.GetUserList = function (groupid,limit, page,callback) {
  var based = this;
  var returnObject  = {
    users:[],
    success: false,
    errorCode: 0,
    error: ""
  };
  var offset = (page-1)*limit
  var sql = "SELECT u.`id`,  u.`email`,  u.`register_date`, u.`update_date`, u.`last_login_date`, u.`last_ip`, u.`isActive` FROM `users` u   limit ?,? "; 
  var sqlWithGroup = "SELECT u.`id`,  u.`email`,  u.`register_date`, u.`update_date`, u.`last_login_date`, u.`last_ip`, u.`isActive` \
                              FROM  premmisionmap2user pu\
                              Left join users u on u.id = pu.userid`users`\
                              Where pu.groupid = ?\
                              Limit ? , ?"; 
  if (groupid != -1) {
    based.dbLink.query(sql,[offset,limit],function (err,resultes, fields) {
      if (err) {
        railway.logger.write(err);
        returnObject.error  = "Data currapted";
        returnObject.errorCode = 500;
        callback(returnObject);
      }
      else 
        buildResults(resultes);
    });
  }
  else {
    based.dbLink.query(sqlWithGroup,[groupid,offset,limit],function (err,resultes, fields) {
      if (err) {
        railway.logger.write(err);
        returnObject.error  = "Data currapted";
        returnObject.errorCode = 500;
        callback(returnObject);
      }
      else 
        buildResults(resultes);
    });
  }
  function buildResults(resultes) {
      _.each(resultes,function (item) {
        var userObj = {
          id: -1,
          email: "",
          register_date: "",
          update_date: "",
          last_login_date: "",
          last_ip: "",
          isActive: ""
        };
        try {
          userObj.id = resultes[0].id;
          userObj.email = resultes[0].email;
          userObj.register_date = new Date(resultes[0].register_date);
          userObj.update_date = new Date(resultes[0].update_date);
          userObj.last_login_date =  new Date(resultes[0].id);
          userObj.last_ip = resultes[0].last_ip;
          userObj.isActive = Boolean(resultes[0].isActive);
          returnObject.users.push(userObj);
        }
        catch(err) {
          railway.logger.write(err);
        }
      });
      returnObject.success = true;
      callback(returnObject);
  }
}
/**
 * Register the user
 * @param Object   fields          User Data Object
 * @param string   ip              user ip
 * @param string   localPrivateKey local front end key
 * @param Object   groupContext    Group object
 * @param Object   roleContext     Role object
 * @param function callback       call back to the controller
 */
PMP.Models.Users.prototype.Register = function (fields ,ip,localPrivateKey,groupContext,roleContext,callback) {
  var based = this;
  var returnObject  = {
    UUID: based.UUID,
    success: false,
    isAutoLogin:false,
    errorCode: 0,
    error: ""
  }

  railway.logger.write(_matchObject(fields));
  if (_matchObject(fields)) {
    railway.logger.write("matched");
  	based.GetUserByUsername(fields.email,function(userObj) {
      if (userObj.id != -1) {
        returnObject.errorCode = 504;
        returnObject.error = "User Existed Cant register the user";
        callback(returnObject);
      }
      else {
        var roleid = -1 , groupid =-1;
        if (fields.gid == -1 && fields.rid == -1) {
          _registerUser(fields,groupid,roleid);
        }
        else {
          if (fields.gid != -1 && fields.rid != -1){
            groupContext.IsGroupExisted(based.Base58.decode(fields.gid),function (responseGroup) {
              roleContext.IsExistRole(based.Base58.decode(fields.rid),function (responseRole) {
                if (responseGroup.isExisted&&responseRole.isExisted) {
                  roleid= roleContext.RoleID;
                  groupid = groupContext.GroupID;
                  _registerUser(fields,groupid,roleid);
                }
              });
            });
          }
          else {
            if (fields.gid != -1) {
              groupContext.IsGroupExisted(based.Base58.decode(fields.gid),function (responseGroup) {
                if (responseGroup.isExisted) {
                  groupid = groupContext.GroupID;
                  _registerUser(fields,groupid,roleid);
                }
              });
            }
            else if (fields.rid != -1) {
              roleContext.IsExistRole(based.Base58.decode(fields.rid),function (responseRole) {
                if (responseRole.isExisted) {
                  roleid= roleContext.RoleID;
                  _registerUser(fields,groupid,roleid);
                }
              });
            }
          }
        }
      }
    })
  }
  else {
    returnObject.errorCode = 404;
    returnObject.error = "arguments not Meet";
    railway.logger.write(returnObject);
    callback(returnObject);
  }
  /**
   * Here The register pros. real happend will be register the user 
   * @param  object fields  user data field for register data
   * @param  string groupid based58 id / -1
   * @param  string roleid  based58 id / -1
   */
  function _registerUser(fields,groupid,roleid) {
    railway.logger.write("passed fillters");
    var sqluserprofile = "INSERT INTO users_profile (country_id , fname,lname,address,city) values ( ? , ? , ? , ? , ? );";
    var sqluser = "insert into users (user_profile_id , email,password,last_ip) values ( ? , ? , ? , ?);";
    var sqlPromissions = "insert into premmisionmap2user (userid , roleid,groupid) values ( ? , ? , ?);";
    var trans = based.dbLink.startTransaction();
    var SHA512 = new based.Hashing.SHA512;
    var b64 = new based.Hashing.Base64;
    var encodePass = b64.encode(SHA512.hex_hmac(fields.pass,localPrivateKey));
    var trans = based.dbLink.startTransaction();
    trans.query(sqluserprofile,[based.Base58.decode(fields.country), fields.fname, fields.lname, fields.address, fields.city],function (err,resultes, flds) {    
      if (err) {
        railway.logger.write(err);
        trans.rollback();
        callback(returnObject);
        return;
      }
      var profile_id = resultes.insertId;
      var trans1 = based.dbLink.startTransaction();
      trans1.query(sqluser,[profile_id,fields.email,encodePass,ip],function (err,resultes, flds) {    
        if (err) {
          railway.logger.write(err);
          trans1.rollback();
         // trans.rollback();
          callback(returnObject);
          return;
        }
        var uid = resultes.insertId;
        if (roleid != -1 && groupid!= -1) {
          var trans2 = based.dbLink.startTransaction();
          if (roleid == -1) roleid = null;
          if (groupid == -1) groupid = null;
          trans2.query(sqlPromissions,[uid,roleid,groupid],function (err,resultes, flds) {    
            if (err) {
              railway.logger.write(err);
              trans2.rollback();
             // trans1.rollback();
            //  trans.rollback();
              callback(returnObject);
              return;
            }

            returnObject.success =true;
            trans2.commit();
           // trans1.commit();
           // trans.commit();
            callback(returnObject);
          });
          trans2.execute();
        }
        else {
          returnObject.success =true;
          trans1.commit();
         // trans.commit();
          callback(returnObject);
        }
      });
      trans1.execute();
    });
    trans.execute();
  }
  function _matchObject(obj) {
    if (_.has(obj, "email") &&
        _.has(obj, "fname") &&
        _.has(obj, "lname") &&
        _.has(obj, "pass") &&
        _.has(obj, "address") &&
        _.has(obj, "city") &&
        _.has(obj, "country") &&
        _.has(obj, "gid") &&
        _.has(obj, "rid")) 
      return true;
    return false;
  }
}

/****
***** PRIVATE METHODS
*****/
/**
 * Get User data Context
 * @param  int   id       user id
 * @param function callback       call back to the controller
 */
PMP.Models.Users.prototype._getContext = function (id,callback) {
	var based = this;
  var returnObject  = {
		id: -1,
		email: "",
		register_date: "",
		update_date: "",
		last_login_date: "",
		last_ip: "",
		isActive: ""
	}
	var sqlSession = "SELECT u.`id`,  u.`email`,  u.`register_date`, u.`update_date`, u.`last_login_date`, u.`last_ip`, u.`isActive` FROM `users` u  WHERE u.id = ? and isActive = 1 limit 1"; 
  based.dbLink.query(sqlSession,[id],function (err,resultes, fields) {
    if (err) callback(returnObject);
    if (resultes.length == 0) callback(returnObject);
    else {
      returnObject.id = resultes[0].id;
    	returnObject.email = resultes[0].email;
    	returnObject.register_date = new Date(resultes[0].register_date);
    	returnObject.update_date = new Date(resultes[0].update_date);
    	returnObject.last_login_date =  new Date(resultes[0].id);
    	returnObject.last_ip = resultes[0].last_ip;
    	returnObject.isActive = Boolean(resultes[0].isActive);
      callback(returnObject);
    }
  });
}