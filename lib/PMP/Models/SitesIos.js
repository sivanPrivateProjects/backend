PMP.Models.SiteIOs = function (uuid,userCtx,callback,failercallback) {
  	var based = this;
	based.GUID = require('guid');	
	based.dbLink = app.ApplicationSharedObjects.Connection;
	if (this.GUID.isGuid(uuid)) {
		based.UUID = uuid;
		if (based.userCtx.IsLoginsuccess) {
		    based.userCtx = userCtx;
		    callback(based);
		}
		else {
			failercallback();
		}
	}
	else {
		failercallback();
	}
}

PMP.Models.SiteIOs.prototype.AddSiteIOs = function(site_id , user_id, title , icon_path ,  alt ,isReadonly,secuty_id,callback) {
  var based= this;
  var returnObject  = {
    UUID: based.userCtx.UUID,
    success: false,
    errorCode: 0,
    error: ""
  }
  based.Validator = validator;

  based.Validator.check(site_id).notEmpty().IsInt();
  based.Validator.check(user_id).notEmpty().IsInt();
  based.Validator.check(title).notEmpty().len(5,35);
  based.Validator.check(icon_path).notEmpty();
  if (!based.Validator.hasErrors()) {
    var isReadonly = Boolean(isReadonly);
    var sql = "insert into sites_ios (site_id , user_id , title , icon_path , alt, isArcived,isReadonly,securty_level_id) values ( ? , ? , ? , ? , ? , ? , ?);";
    var trans = based.dbLink.startTransaction();
    trans.query(sql,[site_id , user_id, title , icon_path ,  alt ,isReadonly,secuty_id],function (err,resultes, fields) {    
      if (err) {
        railway.logger.write(err);
        trans.rollback();
        callback(returnObject);
      }

      returnObject.success =true;
      trans.commit();
      callback(returnObject);
    });
  }
  else {
    returnObject.errorCode = 500;
    returnObject.error = "Incorrect Params";
    callback(returnObject);
  }
};

PMP.Models.SiteIOs.prototype.DeleteSiteIOs = function(site_id , user_id , ios_id,callback) {
  var based= this;
  var returnObject  = {
    UUID: based.userCtx.UUID,
    success: false,
    errorCode: 0,
    error: ""
  }
  based.Validator = validator;
  based.IsExistSiteIOs(site_id , user_id , ios_id,function (response) {
    if (response.success) {
      var sql = "delete from sites_ios where  site_id = ? and user_id = ? and id = ?";
      var trans = based.dbLink.startTransaction();

      trans.query(sql,[site_id , user_id , ios_id],function (err,resultes, fields) {    
        if (err) {
          railway.logger.write(err);
          trans.rollback();
          callback(returnObject);
        }
        returnObject.success =true;
        trans.commit();
        callback(returnObject);
      });
      trans.execute();
    }
    else {
      returnObject.errorCode = 501;
      returnObject.error = "site Ios Icon Not Existed";
      callback(returnObject);
    }
  });
}

PMP.Models.SiteIOs.prototype.ArciveSiteIOs = function (site_id , user_id , ios_id,callback) {
  var based= this;
  var returnObject  = {
    UUID: based.userCtx.UUID,
    success: false,
    errorCode: 0,
    error: ""
  }
  based.Validator = validator;
  based.IsExistSiteIOs(site_id , user_id , ios_id,function (response) {
    if (response.success) {
      var sql = "update sites_ios set  where isArcived= 1 site_id = ? and user_id = ? and id = ?";
      var trans = based.dbLink.startTransaction();

      trans.query(sql,[site_id , user_id , ios_id],function (err,resultes, fields) {    
        if (err) {
          railway.logger.write(err);
          trans.rollback();
          callback(returnObject);
        }

        returnObject.success =true;
        trans.commit();
        callback(returnObject);
      });
      trans.execute();
    }
    else {
      returnObject.errorCode = 501;
      returnObject.error = "site Ios Icon Not Existed";
      callback(returnObject);
    }
  });
};

PMP.Models.SiteIOs.prototype.IsArciveSiteIOs = function (site_id , user_id , ios_id,callback) {
  var based= this;
  var returnObject  = {
    UUID: based.userCtx.UUID,
    success: false,
    errorCode: 0,
    error: ""
  }
  based.Validator = validator;
  based.IsExistSiteIOs(site_id , user_id , ios_id,function (response) {
    if (response.success) {
      returnObject.success = true;
      returnObject.Icon.IsArcive = based.isArcived;
      callback(returnObject);
    }
    else {
      returnObject.errorCode = 501;
      returnObject.error = "site Ios Icon Not Existed";
      callback(returnObject);
    }
  });
};

PMP.Models.SiteIOs.prototype.IsReadonlySiteIOs = function (site_id , user_id , ios_id,callback) {
  var based= this;
  var returnObject  = {
    UUID: based.userCtx.UUID,
    success: false,
    errorCode: 0,
    error: ""
  }
  based.Validator = validator;
  based.IsExistSiteIOs(site_id , user_id , ios_id,function (response) {
    if (response.success) {
      returnObject.success = true;
      returnObject.Icon.IsArcive = based.isReadonly;
      callback(returnObject);
    }
    else {
      returnObject.errorCode = 501;
      returnObject.error = "site Ios Icon Not Existed";
      callback(returnObject);
    }
  });
};

PMP.Models.SiteIOs.prototype.IsExistSiteIOs = function (site_id , user_id , ios_id,callback) {	
  var based= this;
  var returnObject  = {
    UUID: based.userCtx.UUID,
    success: false,
    errorCode: 0,
    error: ""
  }

  based.Validator = validator;
  based.Validator.check(site_id).notEmpty().IsInt();
  based.Validator.check(user_id).notEmpty().IsInt();
  based.Validator.check(ios_id).notEmpty().IsInt();

  if (!based.Validator.hasErrors()) {
  		var sql = "SELECT `icon_path`, `title`, `alt`, `securty_level_id`, `isArcived`, `isReadonly`, `last_access` FROM `sites_ios` WHERE site_id = ? and user_id = ? and id = ? limit 1";
    	based.dbLink.query(sql,[site_id,user_id,ios_id],function (err,resultes, fields) {
    		if (err) {
				railway.logger.write(err);
				callback(returnUsersObject);
    		}
    		if (resultes.length != 0) {
      		var moment = require("moment");
      		based.Icon= {};
    			based.Icon.icon_path =  resultes[0].icon_path;
    			based.Icon.title =  resultes[0].title;
    			based.Icon.alt =  resultes[0].alt;
    			based.Icon.securty_level_id =  resultes[0].securty_level_id;
    			based.Icon.isArcived =  Boolean(resultes[0].isArcived);
    			based.Icon.isReadonly =  Boolean(resultes[0].isReadonly);
    			based.Icon.last_access =  moment(resultes[0].last_access);
    			returnObject.success = true;
   				callback(returnObject);
    		}
    		else {
   				callback(returnObject);
    		}
    	}); 
  }
  else {
    returnObject.errorCode = 500;
    returnObject.error = "Incorrect Params";
    callback(returnObject);
  }
};

PMP.Models.SiteIOs.prototype.BindSiteIOs2SiteIOsFolder = function (site_id , user_id , ios_id,fod_id,callback) {
  var based= this;
  var returnObject  = {
    UUID: based.userCtx.UUID,
    success: false,
    errorCode: 0,
    error: ""
  }
  based.Validator = validator;
  based.IsExistSiteIOs(site_id , user_id , ios_id,function (responseIcon) {
  	if (response.success) {
      if (!responseIcon.Icon.isReadonly&&!responseIcon.Icon.isArcived) {
        based.IsExistSiteIOsFolder( user_id , ios_id,function (responseFolder) {
          if (response.success) {
            if (!responseFolder.Folder.isReadonly&&!responseFolder.Folder.isArcived){
              var sql = "insert into sites_ios2folders (ios_id , folder_id) values ( ? , ? );";
              var trans = based.dbLink.startTransaction();

              trans.query(sql,[fod_id, fod_id],function (err,resultes, fields) {    
                if (err) {
                  railway.logger.write(err);
                  trans.rollback();
                  callback(returnObject);
                }

                returnObject.success =true;
                trans.commit();
                callback(returnObject);
              });
              trans.execute();
            }
            else {
              returnObject.errorCode = 501;
              returnObject.error = "site Ios Folder Locked or Acived";
              callback(returnObject);
            }
          }
          else {
            returnObject.errorCode = 501;
            returnObject.error = "site Ios Folder Not Existed";
            callback(returnObject);
          }
        });
      }
      else {
        returnObject.errorCode = 501;
        returnObject.error = "site Ios Icon Locked or Acived";
        callback(returnObject);
      }
  	}
  	else {
	    returnObject.errorCode = 501;
	    returnObject.error = "site Ios Icon Not Existed";
	    callback(returnObject);
  	}
  });
};

PMP.Models.SiteIOs.prototype.UnBindSiteIOs2SiteIOsFolder = function (site_id , user_id , ios_id,fod_id,callback) {

  var based= this;
  var returnObject  = {
    UUID: based.userCtx.UUID,
    success: false,
    errorCode: 0,
    error: ""
  }
  based.Validator = validator;
  based.IsExistSiteIOs(site_id , user_id , ios_id,function (responseIcon) {
    if (response.success) {
      if (!responseIcon.Icon.isReadonly&&!responseIcon.Icon.isArcived) {
        based.IsExistSiteIOsFolder( user_id , ios_id,function (responseFolder) {
          if (response.success) {
            if (!responseFolder.Folder.isReadonly&&!responseFolder.Folder.isArcived){
              var sql = "delete from sites_ios2folders where ios_id =? and folder_id = ?";
              var trans = based.dbLink.startTransaction();

              trans.query(sql,[fod_id, fod_id],function (err,resultes, fields) {    
                if (err) {
                  railway.logger.write(err);
                  trans.rollback();
                  callback(returnObject);
                }

                returnObject.success =true;
                trans.commit();
                callback(returnObject);
              });
              trans.execute();
            }
            else {
              returnObject.errorCode = 501;
              returnObject.error = "site Ios Folder Locked or Acived";
              callback(returnObject);
            }
          }
          else {
            returnObject.errorCode = 501;
            returnObject.error = "site Ios Folder Not Existed";
            callback(returnObject);
          }
        });
      }
      else {
        returnObject.errorCode = 501;
        returnObject.error = "site Ios Icon Locked or Acived";
        callback(returnObject);
      }
    }
    else {
      returnObject.errorCode = 501;
      returnObject.error = "site Ios Icon Not Existed";
      callback(returnObject);
    }
  });
};

PMP.Models.SiteIOs.prototype.AddSiteIOsFolder = function( user_id, title ,   alt ,isReadonly,secuty_id,callback) {
  var based= this;
  var returnObject  = {
    UUID: based.userCtx.UUID,
    success: false,
    errorCode: 0,
    error: ""
  }
  based.Validator = validator;
  
  based.Validator.check(user_id).notEmpty().IsInt();
  based.Validator.check(title).notEmpty().len(5,35);
  based.Validator.check(title).notEmpty().len(5,35);
  based.Validator.check(icon_path).notEmpty();
  if (!based.Validator.hasErrors()) {
    var isReadonly = Boolean(isReadonly);
    var sql = "insert into sites_ios_folder (user_id , title , alt, isArcived,isReadonly) values ( ? , ? , ? , ? , ? );";
    var trans = based.dbLink.startTransaction();

    trans.query(sql,[site_id , user_id, title , icon_path ,  alt ,isReadonly],function (err,resultes, fields) {    
      if (err) {
        railway.logger.write(err);
        trans.rollback();
        callback(returnObject);
      }

      returnObject.success =true;
      trans.commit();
      callback(returnObject);
    });
    trans.execute();
  }
  else {
    returnObject.errorCode = 500;
    returnObject.error = "Incorrect Params";
    callback(returnObject);
  }
};

PMP.Models.SiteIOs.prototype.DeleteSiteIOsFolder = function(user_id ,folder_id , callback) {
  var based= this;
  var returnObject  = {
    UUID: based.userCtx.UUID,
    success: false,
    errorCode: 0,
    error: ""
  }
  based.Validator = validator;
  based.Validator.check(folder_id).notEmpty().IsInt();
  based.Validator.check(user_id).notEmpty().IsInt();
  based.IsExistSiteIOsFolder(user_id ,folder_id , function (response) {
  	if (response.success) {
      var sql = "delete from sites_ios_folder where user_id = ? and id = ?";
      var trans = based.dbLink.startTransaction();

      trans.query(sql,[ user_id , folder_id],function (err,resultes, fields) {    
        if (err) {
          railway.logger.write(err);
          trans.rollback();
          callback(returnObject);
        }

        returnObject.success =true;
        trans.commit();
        callback(returnObject);
      });
      trans.execute();
  	}
  	else {
	    returnObject.errorCode = 501;
	    returnObject.error = "site Ios Folder Not Existed";
	    callback(returnObject);
  	}
  });
};

PMP.Models.SiteIOs.prototype.ArciveSiteIOsFolder = function (user_id ,folder_id , callback) {
    var based= this;
    var returnObject  = {
        UUID: based.userCtx.UUID,
        success: false,
        errorCode: 0,
        error: ""
    }
    based.Validator = validator;
    based.Validator.check(folder_id).notEmpty().IsInt();
    based.Validator.check(user_id).notEmpty().IsInt();
    based.IsExistSiteIOsFolder(user_id ,folder_id , function (response) {
        if (response.success) {
            var sql =  "update sites_ios_folder set isArcived = 1 WHERE where user_id = ? and id = ? limit 1";
            var trans = based.dbLink.startTransaction();
            trans.query(sql,[ user_id , folder_id],function (err,resultes, fields) {    
                if (err) {
                  railway.logger.write(err);
                  trans.rollback();
                  callback(returnObject);
                }
                
                returnObject.success =true;
                trans.commit();
                callback(returnObject);
            });
            trans.execute();
        }
        else {
            returnObject.errorCode = 501;
            returnObject.error = "site Ios Folder Not Existed";
            callback(returnObject);
        }
    });

};

PMP.Models.SiteIOs.prototype.IsArciveSiteIOsFolder = function (user_id , ios_id,callback) {
  var based= this;
  var returnObject  = {
    UUID: based.userCtx.UUID,
    success: false,
    errorCode: 0,
    error: ""
  }
  based.Validator = validator;
  based.IsExistSiteIOsFolder( user_id , ios_id,function (response) {
    if (response.success) {
      returnObject.success = true;
      returnObject.Folder.IsArcive = based.isArcived;
      callback(returnObject);
    }
    else {
      returnObject.errorCode = 501;
      returnObject.error = "site Ios Folder Not Existed";
      callback(returnObject);
    }
  });
};

PMP.Models.SiteIOs.prototype.IsReadonlySiteIOsFolder = function (user_id , ios_id,callback) {
  var based= this;
  var returnObject  = {
    UUID: based.userCtx.UUID,
    success: false,
    errorCode: 0,
    error: ""
  }
  based.Validator = validator;
  based.IsExistSiteIOsFolder( user_id , ios_id,function (response) {
    if (response.success) {
      returnObject.success = true;
      returnObject.Folder.IsArcive = based.isReadonly;
      callback(returnObject);
    }
    else {
      returnObject.errorCode = 501;
      returnObject.error = "site Ios Folder Not Existed";
      callback(returnObject);
    }
  });
};

PMP.Models.SiteIOs.prototype.IsExistSiteIOsFolder = function (user_id ,folder_id , callback) {
  var based= this;
  var returnObject  = {
    UUID: based.userCtx.UUID,
    success: false,
    errorCode: 0,
    error: ""
  }

  based.Validator = validator;
  based.Validator.check(folder_id).notEmpty().IsInt();
  based.Validator.check(user_id).notEmpty().IsInt();

  if (!based.Validator.hasErrors()) {
  		var sql = "SELECT `title`, `alt`, `securty_level_id`, `isReadonly`, `isArcived` FROM `sites_ios_folder` WHERE where user_id = ? and id = ? limit 1";
    	based.dbLink.query(sql,[site_id,user_id,ios_id],function (err,resultes, fields) {
    		if (err) {
				railway.logger.write(err);
				callback(returnUsersObject);
    		}
    		if (resultes.length != 0) {
        		based.Folder= {};
    			based.Folder.title =  resultes[0].title;
    			based.Folder.alt =  resultes[0].alt;
    			based.Folder.securty_level_id =  resultes[0].securty_level_id;
    			based.Folder.isArcived =  Boolean(resultes[0].isArcived);
    			based.Folder.isReadonly =  Boolean(resultes[0].isReadonly);
    			returnObject.success = true;
   				callback(returnObject);
    		}
    		else {
   				callback(returnObject);
    		}
    	}); 
  }
  else {
    returnObject.errorCode = 500;
    returnObject.error = "Incorrect Params";
    callback(returnObject);
  }

};