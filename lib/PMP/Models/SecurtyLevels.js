PMP.Models.SecurtyLevels = function (uuid,userCtx,callback,failercallback) {
  var based = this;
	based.GUID = require('guid');	
	based.dbLink = app.ApplicationSharedObjects.Connection;
    based.Base58 = require('base58');
	if ( based.GUID.isGuid(uuid)) {
		based.UUID = uuid;
		if (based.userCtx.IsLoginStatus) {
		    based.userCtx = userCtx;
		    callback(based);
		}
		else {
			failercallback();
		}
	}
	else {
		failercallback();
	}
}

PMP.Models.SecurtyLevels.prototype.AddSecurtyLevel = function(name , description , isFullFromReq,isPasswordReq,callback) {
	var based = this;
	var returnObject  = {
		UUID: based.userCtx.UUID,
		status: false,
		errorCode: 0,
		error: ""
	}
    
    based.Validator = validator;
    based.Validator.check(name).notEmpty().len(4,35);
	if (!based.Validator.hasErrors()) {
        isFullFromReq = sanitize(isFullFromReq).toBoolean();
        isPasswordReq = sanitize(isPasswordReq).toBoolean();
    	var sql = "select id from security_levels where name = ?";
        based.dbLink.query(sql,[name],function (err,resultes, fields) {    
    		if (err) {
    			railway.logger.write(err);
    			callback(returnObject);
    		}  
            if (resultes.length == 0) {
        		var sqlInsert = "insert into security_levels (name , description,isFullFromReq,isPasswordReq) values( ? , ? , ? , ? )";
        		var trans = based.dbLink.startTransaction();
        		trans.query(sqlInsert,[name , description , isFullFromReq,isPasswordReq,based.Security.id],function (err,resultes, fields) {  
        			if (err) {
        				railway.logger.write(err);
        				trans.rollback();
        				callback(returnObject);
        			}  
        			returnObject.status =true;
                    returnObject.sec_id = based.Base58.encode(resultes.insertId);
        			trans.commit();
        			callback(returnObject);
        		});
                trans.execute();
            }
    	});
	}
};

PMP.Models.SecurtyLevels.prototype.EditSecurtyLevel = function(securty_id,name , description , isFullFromReq,isPasswordReq,callback) {
  var based = this;
  var returnObject  = {
    UUID: based.userCtx.UUID,
    status: false,
    errorCode: 0,
    error: ""
  }

  based.IsExistSecurtyLevel(securty_id,function (response) {
  	if (response.status) {
	    var sql = "update security_levels set name=? , description =? ,isFullFromReq = ? ,isPasswordReq = ?  where id = ?";
	    var trans = based.dbLink.startTransaction();
	    trans.query(sql,[name , description , isFullFromReq,isPasswordReq,based.Security.id],function (err,resultes, fields) {  
			if (err) {
				railway.logger.write(err);
				trans.rollback();
				callback(returnObject);
			}  
			returnObject.status =true;
			trans.commit();
			callback(returnObject);
	    });
	}
	else {
		returnObject.errorCode= 500;
		returnObject.error = "securty Level Not Existed By this id";
	    callback(returnObject);
	}
  });
};

PMP.Models.SecurtyLevels.prototype.DeleteSecurtyLevel = function (securty_id,callback) {
  var based = this;
  var returnObject  = {
    UUID: based.userCtx.UUID,
    status: false,
    errorCode: 0,
    error: ""
  }
  based.IsExistSecurtyLevel(securty_id,function (response) {
  	if (response.status) {
	    var sql = "delete from security_levels where id = ?";
	    var trans = based.dbLink.startTransaction();
		trans.query(sql,[based.Security.id],function (err,resultes, fields) {  
			if (err) {
				railway.logger.write(err);
				trans.rollback();
				callback(returnObject);
			}  
			returnObject.status =true;
			trans.commit();
			callback(returnObject);
		});
        trans.execute();
	}
	else {
		returnObject.errorCode= 500;
		returnObject.error = "securty Level Not Existed By this id";
	    callback(returnObject);
	}
  });
};

PMP.Models.SecurtyLevels.prototype.GetSecurtyLevelList= function (callback) {
    var based = this;
	var returnObject  = {
		UUID: based.userCtx.UUID,
		SecurityLevels:[],
		status: false,
		errorCode: 0,
		error: ""
	};
  	if (based.GUID.isGuid(siteUUID)) {
    var sql = "select `name`, `description`, `isFullFromReq`,isPasswordReq from security_levels";
    based.dbLink.query(sql,[],function (err,resultes, fields) {    
		if (err) {
			railway.logger.write(err);
			callback(returnObject);
		}
		returnObject.status = true;
		_.each(resultes, function (item ) {
			var obj = {
				id: item.id,
				name:  item.name,
				description: item.description,
				isFullFromReq: Boolean(item.isFullFromReq),
				isPasswordReq: Boolean(item.isPasswordReq)
			};
			returnObject.SecurityLevels.push(obj);
		});
		callback(returnObject);
		
    });
  }
};

PMP.Models.SecurtyLevels.prototype.GetSecurtyLevel = function (securty_id,callback) {
    var based = this;
	var returnObject  = {
		UUID: based.userCtx.UUID,
		SecurityLevels:[],
		status: false,
		errorCode: 0,
		error: ""
	};
  	if (based.GUID.isGuid(siteUUID)) {
    var sql = "select id,`name`, `description`, `isFullFromReq`,isPasswordReq from security_levels where id = ?";
    based.dbLink.query(sql,[securty_id],function (err,resultes, fields) {    
		if (err) {
			railway.logger.write(err);
			callback(returnObject);
		}
		returnObject.status = true;
		_.each(resultes, function (item ) {
			var obj = {
				id: based.Base58.encode(item.id),
				name:  item.name,
				description: item.description,
				isFullFromReq: Boolean(item.isFullFromReq),
				isPasswordReq: Boolean(item.isPasswordReq)
			};
			returnObject.SecurityLevels.push(obj);
		});
		callback(returnObject);
		
    });
  }
};

PMP.Models.SecurtyLevels.prototype.IsExistSecurtyLevel = function (securty_id,callback) {
    var based = this;
	var returnObject  = {
		UUID: based.userCtx.UUID,
		Security: {
			id: "",
			name: '',
			description: "",
			isFullFromReq: "",
			isPasswordReq: ''
		},
		status: false,
		errorCode: 0,
		error: ""
	};
  	if (based.GUID.isGuid(siteUUID)) {
    var sql = "select `name`, `description`, `isFullFromReq`,isPasswordReq from security_levels where id = ?  limit 1";
    based.dbLink.query(sql,[securty_id],function (err,resultes, fields) {    
		if (err) {
			railway.logger.write(err);
			callback(returnObject);
		}
		if(resultes.length != 0) {
			returnObject.status = true;
			returnObject.Security.id = based.Base58.encode(resultes[0].id);
			returnObject.Security.name = resultes[0].name;
			returnObject.Security.description = resultes[0].description;
			returnObject.Security.isFullFromReq = Boolean(resultes[0].isFullFromReq);
			returnObject.Security.isPasswordReq = Boolean(resultes[0].isPasswordReq);
			based.Security = returnObject.Security;
			callback(returnObject);
		}
        else{ 
    		callback(returnObject);
        }
    });
  }
};