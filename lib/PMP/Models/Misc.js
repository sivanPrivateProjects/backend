PMP.Models.Misc = function () { 
  	var based = this;
      based.Base58 = require('base58');
	based.dbLink = app.ApplicationSharedObjects.Connection;
};

PMP.Models.Misc.prototype.GetCountrys =  function (callback) {
	var based = this;
	var sql = "SELECT `id`, `short_name`, `long_name`, `iso2`, `iso3`, `numcode` FROM `country`"; 
	var returnObject = {
            success: false,
            errorCode: 0,
            error: "",
		Countrys: []
	}
	based.dbLink.query(sql,[],function (err,resultes, fields) {
            if (err) {
            	railway.logger.write(err);
                  callback(returnObject);
            }
            else {
            	_.each(resultes ,function (item){
                        var object = {
                              id: based.Base58.encode(item.id),
                              short_name: item.short_name ,
                              long_name: item.long_name,
                              iso2: item.iso2,
                              iso3: item.iso3,
                              numcode: item.numcode
                        }
            		returnObject.Countrys.push(object);
            	});
                  returnObject.success = true;
            	callback(returnObject)
            }
	});
}

PMP.Models.Misc.prototype.IsCountryExist =  function (id,callback) {
      var based = this;
      var sql = "SELECT `id`, `short_name`, `long_name`, `iso2`, `iso3`, `numcode` FROM `country` where id = ?"; 
      var returnObject = {
            success: false,
            isFound: false,
            errorCode: 0,
            error: "",
            Countrys: []
      }
      var cid = based.Base58.decode(id);

      based.dbLink.query(sql,[cid],function (err,resultes, fields) {
            if (err) {
                  railway.logger.write(err);
                  callback(returnObject);
            }
            else {
                  if (resultes.length != 0) {
                        returnObject.isFound = true;
                  }
                  returnObject.success = true;
                  callback(returnObject);
            }
      });
}