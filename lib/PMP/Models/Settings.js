PMP.Models.Settings = function () { 
  	var based = this;
	based.dbLink = app.ApplicationSharedObjects.Connection;
};
/**
 * Load the settings data only that effect BO
 * @param function callback       call back to the controller
 */
PMP.Models.Settings.prototype.LoadSettings =  function (callback) {
	var based = this;
	var sqlsettings = "SELECT `key`,`value` from settings where exposeLevel in(0,1)"; 
	var returnObject = {
		isBlocker: true,
		Settings: []
	}
	based.dbLink.query(sqlsettings,[],function (err,resultes, fields) {
      if (err) {
      	railway.logger.write(err);
        callback(returnObject);
      }
      else { 
      	_.each(resultes ,function (item){
      		returnObject.Settings[item.key] = item.value;
      	});
      	returnObject.isBlocker = false;
      	callback(returnObject)
      }
	});
}
/**
 * Load the settings data only effect Front end
 * @param function callback       call back to the controller
 */
PMP.Models.Settings.prototype.GetSettingsFrontEnd =  function (callback) {
      var based = this;
      var sqlsettings = "SELECT `key`,`value` from settings where exposeLevel <> '0'"; 
      var returnObject = {
            Settings: [],
            success: false,
            errorCode: 0,
            error: ""
      }
      based.dbLink.query(sqlsettings,[],function (err,resultes, fields) {
            if (err) {
                  railway.logger.write(err);
                  callback(returnObject);
            }
            else {
                  _.each(resultes ,function (item){
                        var obj = {};
                        obj[item.key] = item.value;
                        returnObject.Settings.push(obj);
                  });
                  returnObject.success = true;
                  callback(returnObject);
            }
      });
}


