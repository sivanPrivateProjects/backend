PMP.Models.Roles = function (uuid,userCtx,callback,failercallback,isRegister) { 
	var based = this;
  	based.isRegister = isRegister;
	based.GUID = require('guid');	
  	based.Base58 = require('base58');
	based.dbLink = app.ApplicationSharedObjects.Connection;
	if (based.isRegister) {
		callback(based);
	}
	else {
		if (based.GUID.isGuid(uuid)) {
		  if (based.userCtx.IsLoginsuccess&&!based.isRegister) {
			based.UUID = based.userCtx.UUID;
			callback(based);
		  }
		  else {
		    failercallback();
		  }
		}
		else {
			failercallback();
		}
	}
};

/**
 * add role to the ACL
 * @param string   name        role name
 * @param string   description role description
 * @param function callback       call back to the controller
 */
PMP.Models.Roles.prototype.AddRole = function (name ,description,callback) {
	var based = this;
	var returnUsersObject  = {
		UUID: based.UUID,
		success: false,
		errorCode: 0,
		error: ""
	}
	based.Validator = validator;
	based.Validator.check(name,"role name needed").notEmpty();
	var errors = based.Validator.getErrors();
	if (!based.Validator.hasErrors()) {
		based.GetRoleByName(name,function (response) {
			if (!response.success) {
				var sqlGroup = "insert into roles (name, `desc`,isActive) values(?,?,1)";
				var trans = based.dbLink.startTransaction();
		  		trans.query(sqlGroup,[name ,description],function (err,resultes, fields) { 
		  			if (err) {
		  				railway.logger.write(err);
		  				trans.rollback();
		  				callback(returnUsersObject);
		  				return;
		  			}
					returnUsersObject.success =true;
		  			trans.commit();
		  			callback(returnUsersObject);
		  		});
		  		trans.execute();
			}
			else {
				returnUsersObject.errorCode = 500;
				returnUsersObject.error= "Role Not Exist";
		  		callback(returnUsersObject);
			}
		});
	}
	else  {
		returnObject.errorCode =101;
		returnObject.error= errors;
	  	callback(returnObject);
	}
} 
/**
 * delete the role
 * @param string   roleid   role id encode on base 58
 * @param function callback       call back to the controller
 */
PMP.Models.Roles.prototype.DeleteRole = function (roleid,callback) {
	var based = this;
	var returnUsersObject  = {
		UUID: based.UUID,
		success: false,
		errorCode: 0,
		error: ""
	}
  	roleid= based.Base58.decode(roleid);
	based.IsExistRole(roleid,function (response) {
		if (response.success) {
			var sqlGroup = "delete from role where id = ?";
			var trans = based.dbLink.startTransaction();
	  		trans.query(sqlGroup,[roleid],function (err,resultes, fields) { 
	  			if (err) {
	  				railway.logger.write(err);
	  				trans.rollback();
	  				callback(returnUsersObject);
	  				return;
	  			}
				returnUsersObject.success =true;
	  			trans.commit();
	  			callback(returnUsersObject);
	  		});
	  		trans.execute();
		}
		else {
			returnUsersObject.errorCode = 500;
			returnUsersObject.error= "Role Not Exist";
	  		callback(returnUsersObject);
		}
	});
} 
/**
 * find the role under N name
 * @param string   name     role name
 * @param function callback       call back to the controller
 */
PMP.Models.Roles.prototype.GetRoleByName = function (name,callback) {
	var based = this;
	var returnUsersObject  = {
		UUID: based.UUID,
		success: false,
		errorCode: 0,
		error: ""
	}
	based.Validator = validator;
	based.Validator.check(name,"Role name needed").notEmpty();
	var errors = based.Validator.getErrors();
	if (!based.Validator.hasErrors()) {
		var sqlRole = "select id,isActive from roles where name = ?";
		based.dbLink.query(sqlRole,[name],function (err,resultes, fields) {    
  			if (err) {
  				railway.logger.write(err);
  				callback(returnUsersObject);
  			}
  			if(resultes.length != 0) {
  				returnUsersObject.success = true;
  				returnUsersObject.roleId = resultes[0].id;
  				based.RoleID = resultes[0].id;
  				callback(returnUsersObject);
  			}
  		});
	}
	else  {
		returnObject.errorCode =101;
		returnObject.error= errors;
	  	callback(returnObject);
	}
} 


/**
 * Get all the roles on the system
 * @param function callback       call back to the controller
 */
PMP.Models.Roles.prototype.GetRoles = function (callback) {

	var based = this;
	var returnObject  = {
		success: false,
		Roles: [],
		errorCode: 0,
		error: ""
	}
	var sql = "select id,name,  `desc`, isActive from roles";
	based.dbLink.query(sqlInsertGroupProfile,[email],function (err,resultes, fields) { 
		if (err) {
			railway.logger.write(err);
			callback(returnObject);
			return;
		}
		_.each(resultes,function (groupItem) {
			try {
				var obj = {
					ID: based.Base58.encode(groupItem.id),
					Name: groupItem.name,
					Description: groupItem.desc,
					isActive: Boolean(groupItem.isActive)
				};
				returnObject.Roles.push(obj);
			}
			catch(err) {
        		railway.logger.write(err);
			}
		});
		returnObject.success =true;
		callback(returnObject);
	});
}
/**
 * will active/deactive role based on his current success
 * @param string   roleid   role id encode on base 58
 * @param function callback       call back to the controller
 */
PMP.Models.Roles.prototype.ToggleRole = function (roleid,callback) {
	var based = this;
	var returnUsersObject  = {
		UUID: based.UUID,
		success: false,
		errorCode: 0,
		error: ""
	}
  	roleid= based.Base58.decode(roleid);
	based.IsExistRole(roleid,function (response) {
		if (response.success) {

			var sqlGroup = "update set role isActive = ?  where id = ?";
			var success = (based.IsActive)?false:true;
			var trans = based.dbLink.startTransaction();
	  		trans.query(sqlGroup,[success,roleid],function (err,resultes, fields) { 
	  			if (err) {
	  				railway.logger.write(err);
	  				trans.rollback();
	  				callback(returnUsersObject);
	  				return;
	  			}
				returnUsersObject.success =true;
	  			trans.commit();
	  			callback(returnUsersObject);
	  		});
	  		trans.execute();
		}
		else {
			returnUsersObject.errorCode = 500;
			returnUsersObject.error= "Role Not Exist";
	  		callback(returnUsersObject);
		}
	});
} 
/**
 * will chack if role exised (by his id)
 * @param string   roleid   role id encode on base 58
 * @param function callback       call back to the controller
 */
PMP.Models.Roles.prototype.IsExistRole = function (roleid,callback) {
	var based = this;
	var returnUsersObject  = {
		UUID: based.UUID,
		success: false,
		isExisted: false,
		errorCode: 0,
		error: ""
	}

  	roleid= based.Base58.decode(roleid);
	based.Validator = validator;
	based.Validator.check(roleid,"role id is needed").isInt();
	var errors = based.Validator.getErrors();
	if (!based.Validator.hasErrors()) {
		var roleid = sanitize(roleid).toInt();
		var sqlRole = "select id,name,description,isActive from roles where id = ?";
		based.dbLink.query(sqlRole,[roleid],function (err,resultes, fields) {    
  			if (err) {
  				railway.logger.write(err);
  				callback(returnUsersObject);
  			}
  			if(resultes.length != 0) {
  				returnObject.success = (based.isRegister)?false:true;
  				returnObject.isExisted =true;
  				based.RoleID = resultes[0].id;
  				based.Name = resultes[0].name;
  				based.Description = resultes[0].description;
  				based.IsActive = Boolean(resultes[0].isActive);
  				callback(returnUsersObject);
  			}
  		});
	}
	else  {
		returnObject.errorCode =101;
		returnObject.error= errors;
	  	callback(returnObject);
	}
} 
/**
 * will bind user to a role
 * @param int userid  user id
 * @param int roleid role id
 */
PMP.Models.Roles.prototype.BindUserToRole = function (userid,roleid) {
	var based = this;
	var returnObject  = {
		UUID: based.UUID,
		success: false,
		errorCode: 0,
		error: ""
	}
	based.IsExistRole(roleid,function (res){
		if (res.success) {

			var sqlUpdatePrem = "update  premmisionmap2user  set roleid = ? where userid = ?";
			var sqlchackGroupPremmision = "SELECT pu.userid,pu.roleid \
			                        from premmisionmap2user pu \
			                        left join user u on u.id = pu.userid \
			                        where pu.roleid = ? and u.id = ? limit 1"
			based.dbLink.query(sqlchackGroupPremmision,[roleid,userid],function (err,resultes, fields) {  
				if (err||resultes.length == 0) {
					railway.logger.write(err);
					returnObject.error = "Data Not Found";
					returnObject.errorCode = 500;
					callback(returnObject);
				}
				else {
					if (resultes.length == 0) {
						var trans = based.dbLink.startTransaction();
						trans.query(sqlUpdatePrem,[roleid,userid],function (err,resultes, fields) { 
							if (err) {
								railway.logger.write(err);
								returnObject.error = "Data Not Found";
								returnObject.errorCode = 500;
								callback(returnObject);
							}
							else {
								returnObject.success =true;
					  			trans.commit();
					  			callback(returnObject);
					  		}
				  		});
				  		trans.execute();
				  	}
				}
			});
		}
		else {
			returnObject.errorCode = 500;
			returnObject.error= "Group Not Exist";
	  		callback(returnObject);
		}
	})
}
/**
 * will unbind user to a role
 * @param int userid  user id
 * @param int roleid role id
 */
PMP.Models.Roles.prototype.UnBindUserToRole = function (userid,roleid) {
	var based = this;
	var returnObject  = {
		UUID: based.UUID,
		success: false,
		errorCode: 0,
		error: ""
	}
	based.IsExistRole(roleid,function (res){
		if (res.success) {
			var sqlUpdatePrem = "update  premmisionmap2user  set roleid = null where userid = ?";
			var sqlchackGroupPremmision = "SELECT pu.userid,pu.roleid \
			                        from premmisionmap2user pu \
			                        left join user u on u.id = pu.userid \
			                        where pu.roleid = ? and u.id = ? limit 1"
			based.dbLink.query(sqlchackGroupPremmision,[roleid,userid],function (err,resultes, fields) {  
				if (err||resultes.length == 0) {
					railway.logger.write(err);
					returnObject.error = "Data Not Found";
					returnObject.errorCode = 500;
					callback(returnObject);
				}
				else {
					var trans = based.dbLink.startTransaction();
					trans.query(sqlUpdatePrem,[roleid,userid],function (err,resultes, fields) { 
						if (err) {
							railway.logger.write(err);
							returnObject.error = "Data Not Found";
							returnObject.errorCode = 500;
							callback(returnObject);
						}
						else {
							returnObject.success =true;
				  			trans.commit();
				  			callback(returnObject);
				  		}
			  		});
			  		trans.execute();
		
				}
			});
		}
		else {
			returnObject.errorCode = 500;
			returnObject.error= "Group Not Exist";
	  		callback(returnObject);
		}
	})
}

/**
 * will bind group to a role
 * @param int groupid  group id
 * @param int roleid role id
 */
PMP.Models.Roles.prototype.BindGroupToRole = function (groupid,roleid) {
	var based = this;
	var returnObject  = {
		UUID: based.UUID,
		success: false,
		errorCode: 0,
		error: ""
	}
	based.IsExistRole(roleid,function (res){
		if (res.success) {
			exports.GroupControl.context.IsGroupExisted(groupid,function (resGroup){
				if (res.success) {
					var sqlInsert = "insert  roles2groups  (role_id,group_id) values (? ,?)";
					exports.GroupControl.context.IsGroupPartOfRole(groupid,roleid,function (resRoleMatchGroup){
						if (!resRoleMatchGroup.success) {
							var trans = based.dbLink.startTransaction();
							trans.query(sqlInsert,[roleid,groupid],function (err,resultes, fields) { 
								if (err) {
									railway.logger.write(err);
									returnObject.error = "Data Not Found";
									returnObject.errorCode = 500;
									callback(returnObject);
								}
								else {
									returnObject.success =true;
						  			trans.commit();
						  			callback(returnObject);
						  		}
					  		});
					  		trans.execute();
						}
						else {
							returnObject.success =true;
							callback(returnObject);
						}
					});
				}
				else {
					returnObject.errorCode = 500;
					returnObject.error= "Group Not Exist";
			  		callback(returnObject);
				}
			})
		}
		else {
			returnObject.errorCode = 500;
			returnObject.error= "Role Not Exist";
	  		callback(returnObject);
		}
	})
}

/**
 * will unbind group to a role
 * @param int groupid  group id
 * @param int roleid role id
 */
PMP.Models.Roles.prototype.UnBindGroupToRole = function (groupid,roleid) {
	var based = this;
	var returnObject  = {
		UUID: based.UUID,
		success: false,
		errorCode: 0,
		error: ""
	}
	based.IsExistRole(roleid,function (res){
		if (res.success) {
			exports.GroupControl.context.IsGroupExisted(groupid,function (resGroup){
				if (res.success) {
					var sqlDelete = "delete  roles2groups where role_id=?,group_id=?";
					exports.GroupControl.context.IsGroupPartOfRole(groupid,roleid,function (resRoleMatchGroup){
						if (resRoleMatchGroup.success) {
							var trans = based.dbLink.startTransaction();
							trans.query(sqlDelete,[roleid,groupid],function (err,resultes, fields) { 
								if (err) {
									railway.logger.write(err);
									returnObject.error = "Data Not Found";
									returnObject.errorCode = 500;
									callback(returnObject);
								}
								else {
									returnObject.success =true;
						  			trans.commit();
						  			callback(returnObject);
						  		}
					  		});
					  		trans.execute();
						}
						else {
							returnObject.success =true;
							callback(returnObject);
						}
					});
				}
				else {
					returnObject.errorCode = 500;
					returnObject.error= "Group Not Exist";
			  		callback(returnObject);
				}
			})
		}
		else {
			returnObject.errorCode = 500;
			returnObject.error= "Role Not Exist";
	  		callback(returnObject);
		}
	})
}