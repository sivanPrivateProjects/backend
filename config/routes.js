exports.routes = function (map) {

    // Generic routes. Add all your routes below this line
    // feel free to remove generic routes
  	//map.all(':controller/:action');
    //map.all(':controller/:action/:id');
	map.namespace('services', function (ns) {
		ns.resources('users',function (action) {
		    action.post(/^\/(services)\/(api)\/(users)\/(login)\.(serv)?/i,"users#login");
		    action.post(/^\/(services)\/(api)\/(users)\/(register)\.(serv)?/i,"users#register");
		    action.get(/^\/(services)\/(api)\/(users)\/(\{){0,1}[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}(\}){0,1}\/(logout)\.(serv)/i,"users#logout");
		    //services/user/abcs1234-1234-1234-1234-123456789abc/logout.serv
		    action.get(/^\/(services)\/(api)\/(users)\/(\{){0,1}[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}(\}){0,1}\/(HasUserHavePremmision)\.(serv)?/i,"users#HasPremmision");
		    action.get(/^\/(services)\/(api)\/(users)\/(\{){0,1}[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}(\}){0,1}\/(getProfile)\.(serv)?/i,"users#getProfile");
		    action.get(/^\/(services)\/(api)\/(users)\/(\{){0,1}[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}(\}){0,1}\/(userGroups)\.(serv)?/i,"users#userGroups");
		    action.get(/^\/(services)\/(api)\/(users)\/(\{){0,1}[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}(\}){0,1}\/(getUser)\.(serv)?/i,"users#getUser");
		});
		ns.resources('groups',function (action) {
		    action.post(/^\/(services)\/(api)\/(groups)\/(\{){0,1}[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}(\}){0,1}\/(add)\.(serv)/i,"groups#AddGroup");
		    action.put(/^\/(services)\/(api)\/(groups)\/(\{){0,1}[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}(\}){0,1}\/(moveGroupUsers)\.(serv)?/i,"groups#MoveGroupUsers");
		    action.delete(/^\/(services)\/(api)\/(groups)\/(\{){0,1}[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}(\}){0,1}\/(deleteGroup)\.(serv)?/i,"groups#DeleteGroup");
		    action.get(/^\/(services)\/(api)\/(groups)\/(\{){0,1}[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}(\}){0,1}\/(getGroupProfile)\.(serv)?/i,"groups#GetGroupProfile");
		    //services/groups/abcs1234-1234-1234-1234-123456789abc/logout.serv
		    action.get(/^\/(services)\/(api)\/(groups)\/(\{){0,1}[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}(\}){0,1}\/(isGroupPartOfRole)\.(serv)?/i,"groups#IsGroupPartOfRole");
		    action.put(/^\/(services)\/(api)\/(groups)\/(\{){0,1}[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}(\}){0,1}\/(UnBindUser2Group)\.(serv)?/i,"groups#UnBindUserToGroup");
		    action.put(/^\/(services)\/(api)\/(groups)\/(\{){0,1}[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}(\}){0,1}\/(BindUser2Group)\.(serv)?/i,"groups#BindUserToGroup");
		    action.put(/^\/(services)\/(api)\/(groups)\/(\{){0,1}[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}(\}){0,1}\/(toggelStatus)\.(serv)?/i,"groups#ToggelStatusGroup");
		});
		ns.resources('roles',function (action) {
		    action.post(/^\/(services)\/(api)\/(roles)\/(\{){0,1}[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}(\}){0,1}\/(add)\.(serv)/i,"roles#AddRole");
		    action.put(/^\/(services)\/(api)\/(roles)\/(\{){0,1}[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}(\}){0,1}\/(UnBindRole2Group)\.(serv)?/i,"roles#UnBindRoleToGroup");
		    action.put(/^\/(services)\/(api)\/(roles)\/(\{){0,1}[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}(\}){0,1}\/(BindRole2Group)\.(serv)?/i,"roles#BindRoleToGroup");
		    action.put(/^\/(services)\/(api)\/(roles)\/(\{){0,1}[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}(\}){0,1}\/(UnBindRole2User)\.(serv)?/i,"roles#UnBindRoleToUser");
		    action.put(/^\/(services)\/(api)\/(roles)\/(\{){0,1}[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}(\}){0,1}\/(BindRole2User)\.(serv)?/i,"roles#BindRoleToUser");
		    action.put(/^\/(services)\/(api)\/(roles)\/(\{){0,1}[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}(\}){0,1}\/(toggleRole)\.(serv)?/i,"roles#ToggleRole");
		    action.delete(/^\/(services)\/(api)\/(roles)\/(\{){0,1}[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}(\}){0,1}\/(deleteGroup)\.(serv)?/i,"roles#DeleteRole");
		});
		ns.resources('misc',function (action) {
		    action.get(/^\/(services)\/(api)\/(misc)\/(getcountrys)\.(serv)?/i,"misc#getCountrys");
		    action.get(/^\/(services)\/(api)\/(misc)\/(isCountryExist)\.(serv)?/i,"misc#IsCountryExist");
		});
		ns.resources('controls',function (action) {
			action.get(/^\/(services)\/(api)\/(controls)\/(getRoles)\.(serv)?/i,"roles#GetRoles");
			action.get(/^\/(services)\/(api)\/(controls)\/(getGroups)\.(serv)?/i,"groups#GetGroups");
		    action.get(/^\/(services)\/(api)\/(controls)\/(getFrontendSettings)\.(serv)?/i,"misc#getFrontEndSettings");
		});
		ns.resources('caching',function (action) {
			action.get(/^\/(services)\/(api)\/(caching)\/(fetchGroups)\.(serv)?/i,"groups#ScanCachedGroups");
			action.get(/^\/(services)\/(api)\/(caching)\/(fetchRoles)\.(serv)?/i,"roles#ScanCachedRoles");
		});
	});

	map.namespace('test', function (ns) {
		ns.resources('usertest',function (action) {
		    action.get(/^\/(form)\/(login)\.(htm)/,"usertest#login");
		})
	});
};
 