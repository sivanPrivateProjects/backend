var express    = require('express'),
    mysql = require('../startApplication').LoadDb(),
    MySQLStore = require('connect-mysql')(express);
var extras = require('express-extras');

var cookiePrivateKey = "c227ecd5-fb75-422b-90ec-60af037fae48";
var sessionPrivateKey = "d6457651-1c61-47d9-9971-687ead47756a";

app.configure(function(){
    var cwd = process.cwd();
    
    app.use(express.static(cwd + '/public', {maxAge: 86400000}));
    app.set('views', cwd + '/app/views');
    app.set('view engine', 'ejs');
    app.set('view options', {complexNames: true});
    app.set('jsDirectory', '/javascripts/');
    app.set('cssDirectory', '/stylesheets/');
    app.enable('jsonp callback');
    app.use(express.bodyParser());
    app.use(express.cookieParser(cookiePrivateKey));
    app.use(extras.fixIP([
        'x-forwarded-for',
        'forwarded-for',
        'x-cluster-ip'
    ]));
    app.use(express.session({ secret: sessionPrivateKey,cookie: { secure: true }, store: new MySQLStore({ client: mysql })}));

    app.use(function (req, res, next) {
        app.ApplicationSharedObjects = require('../startApplication').LoadApplciation();
        app.ApplicationSharedObjects.OnBeginRequest(req, res, next);
        //console.log(__dirname);
        app.ApplicationSharedObjects.OnEndRequest(req, res);
    });
    app.use(express.methodOverride());
    app.use(app.router);
});
